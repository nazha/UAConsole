# API列表
除了link API以外，api统一以`/agent/admin/`开头，下面的URL路径会省略此前缀。URL中最后一个`/`(slash)可有可无，文档里面会统一写成有。

AUTH: YES  需要鉴权，http header带上X-Auth-User和X-Auth-Token
AUTH: NO   不需要鉴权

分页: 所有需要分页的接口，统一参数如下

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|$page|false|int|页码，默认1|
|$size|false|int|每页显示数目，默认15|

HTTP返回格式:
```py
{
    "status": 0,
    "msg": "",   # status不为0时，错误消息会以中文的形式打印出来，可以直接显示给用户
    "data": {...}
}
```

HTTP返回码包括：
```python
    OK = 200
    CREATED = 201
    ACCEPTED = 202
    NO_CONTENT = 204
    PARTIAL_CONTENT = 206
    NOT_MODIFIED = 304
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405
    EXPECTATION_FAILED = 417
    SERVER_ERROR = 500
    NOT_IMPLEMENTED = 501
```
为了简化设计，2xx系列的只使用了200.

status的可能值包括：
```python
{
    0: "OK",
    1: "未知错误",
    2: "参数错误",
    3: "需要使用HTTPS",
    4: "数据错误",
    5: "数据库错误",
    6: "缓存错误",

    101: "用户不存在",
    102: "密码错误",
    103: "验证码错误",
    104: "账户已存在",
    105: "TOKEN失效",
    106: "手机号已存在",

    201: "资源不足",
    202: "余额不足",

    301: "达到限制",
}
```





