# API列表
除了link API以外，api统一以`/agent/admin/`开头，下面的URL路径会省略此前缀。URL中最后一个`/`(slash)可有可无，文档里面会统一写成有。

AUTH: YES  需要鉴权，http header带上X-Auth-User和X-Auth-Token
AUTH: NO   不需要鉴权

分页: 所有需要分页的接口，统一参数如下

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|$page|false|int|页码，默认1|
|$size|false|int|每页显示数目，默认15|

HTTP返回格式:
```py
{
    "status": 0,
    "msg": "",   # status不为0时，错误消息会以中文的形式打印出来，可以直接显示给用户
    "data": {...}
}
```

HTTP返回码包括：
```python
    OK = 200
    CREATED = 201
    ACCEPTED = 202
    NO_CONTENT = 204
    PARTIAL_CONTENT = 206
    NOT_MODIFIED = 304
    BAD_REQUEST = 400
    UNAUTHORIZED = 401
    FORBIDDEN = 403
    NOT_FOUND = 404
    METHOD_NOT_ALLOWED = 405
    EXPECTATION_FAILED = 417
    SERVER_ERROR = 500
    NOT_IMPLEMENTED = 501
```
为了简化设计，2xx系列的只使用了200.

status的可能值包括：
```python
{
    0: "OK",
    1: "未知错误",
    2: "参数错误",
    3: "需要使用HTTPS",
    4: "数据错误",
    5: "数据库错误",
    6: "缓存错误",
    101: "用户不存在",
    102: "密码错误",
    103: "验证码错误",
    104: "账户已存在",
    105: "TOKEN失效",
    106: "手机号已存在",
    201: "资源不足",
    202: "余额不足",
    301: "达到限制",
}
```


### 创建子代理 
- URL: `user/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON字符串

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|
|password|true|str|密码|
|nickname|true|str|昵称|

- RETURN:  一个字典，格式为：

```py
{
    "msg": "",
    "status": 0,
    "data": {
        "username": "daili889",
        "agent_level": 2,
        "parent_id": 2,
        "role": 1,
        "nickname": "daili889",
        "id": 20
    }
}
```

### 登录
- URL: `user/login/`
- METHOD: `POST`
- AUTH: NO
- PARAMS: JSON字符串

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|
|password|true|str|密码|
|code|true|str|图形验证码|

- RETURN:  一个字典，格式为：

```py
{
    "msg": "",
    "status": 0,
    "data": {
        "username": "admin",
        "balance": 0,
        "last_login": "2018-06-12 14:09:27",
        "extend": null,
        "bind_phone": null,
        "created_at": "2018-06-11 16:33:45",
        "updated_at": "2018-06-12 14:09:27",
        "parent_id": 0,
        "token": "72692201-55b6-4a47-87cd-4efb8c8764dc",
        "agent_level": 1,
        "total_reward": 0,
        "bind_ip": null,
        "role": 1,
        "nickname": "admin001",
        "id": 2
    }
}
```


### 登出
- URL: `user/logout/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: NULL

- RETURN:
```py
{}
```

### 获取图形验证码
- URL: `imagecode/`
- METHOD: `GET`
- AUTH: NO
- PARAMS: QUERYSTRING

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|

- RETURN:  一个image/png格式的图片


### 判断用户名是否存在 
- URL: `user/exists/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "exists": true/false
    }
}
``` 


### 忘记密码
- URL: `forget_passwd/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "phone": "*******5529", // 前端需要展示手机号码的后四位，提示用户已发送
        "code": '345233' // 测试环境会直接返回code用于调试
    }
}
``` 


### 重新设置密码
- URL: `change_password/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|username|true|str|用户名|
|password|true|str|密码|
|code|true|str|验证码|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
    }
}
``` 


### 获取手机短信验证码
- URL: `vcode/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|phone|true|str|手机号|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "code": '345233' // 测试环境会直接返回code用于调试
    }
}
``` 


### 绑定手机
- URL: `bind_phone/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|phone|true|str|手机号|
|code|true|str|短信验证码|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
    }
}
``` 


### 获取单个代理信息 
- URL: `user/{userid}/`
- METHOD: `GET`
- AUTH: YES 

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "username": "admin",
        "balance": 0,
        "last_login": "2018-06-13 16:52:43",
        "extend": null,
        "bind_phone": null,
        "created_at": "2018-06-12 00:33:45",
        "updated_at": "2018-06-13 16:52:43",
        "parent_id": 0,
        "agent_level": 1,
        "total_reward": 0,
        "bind_ip": null,
        "role": 1,
        "password": "9687cbc8a16232d862191009eacddec1",
        "nickname": "admin001",
        "id": 2
    }
}
``` 


### 修改单个代理信息(目前只能修改自己的)
- URL: `user/{userid}/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON字符串

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|password|false|str|密码|
|nickname|false|str|昵称|
|bind_alipay|false|json|支付宝信息 {"name": "小张", "alipay_no": "xiaozhang@ali.com"}|
|bind_union|false|json|银行卡信息 {"name": "小张", "bank_name": "中国银行", "bank_no": "622000111000"}|
|bind_ip|false|str|绑定ip '8.8.8.8'|
|code|false|str|手机短信验证码，修改bind_alipay/bind_union/bind_ip时必填|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
    }
}
``` 


### 获取子代理信息列表 
- URL: `user/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|id|false|int|代理编号|
|role|false|int|状态，0: 封号， 1:正常|
|bind_iphone|false|str|绑定手机号|
|username|false|str|用户名|
|nickname|false|str|昵称|
|balance_min|false|int|最小余额|
|balance_max|false|int|最大余额|
|reward_min|false|int|历史总额最小|
|reward_max|false|int|历史总额最大|
|register_start|false|string|注册起始时间 格式2018-06-12|
|register_end|false|string|注册截止时间 格式2018-06-12|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "list": [{
            "username": "admin4",
            "bind_phone": null,
            "bind_ip": null,
            "nickname": "admin004",
            "id": 6,
            "level": 2,
            "created_at": "2018-06-12 15:51:07",
            "last_login": "2018-06-12 11:51:06",
            "role": 1,
            "total_reward": 0,
            "balance": 0
        },
        {
            "username": "daili001",
            "bind_phone": null,
            "bind_ip": null,
            "nickname": "daili001",
            "id": 7,
            "level": 2,
            "created_at": "2018-06-12 20:47:18",
            "last_login": "2018-06-12 16:47:17",
            "role": 1,
            "total_reward": 0,
            "balance": 0
        }
        ],
    }
}
``` 


### 获取整体数据(首页信息) 
- URL: `overview/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: null


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "total": {
            "total_recharge": 0,  # 总充值 
            "total_tax": 0,       # 总税收
            "total_reward": 0,    # 总返利 
            "register_count": 0,  ＃注册用户数(绑定)
            "active_count": 0     # 激活用户数(新增)
        },
        "yesterday": {
            "total_recharge": 0,
            "total_tax": 0,
            "total_reward": 0,
            "register_count": 0,
            "active_count": 0
        },
        "today": {
            "total_recharge": 0,
            "total_tax": 7800,
            "total_reward": 390,
            "register_count": 1,
            "active_count": 1
        }
    }
}
```


### 获取本级代理用户统计数据(推广用户数据统计界面)
- URL: `user_stats/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING 

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|start_date|false|string|日期起始值|
|end_date|false|string|日期截止值|


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "total_count": 3,
        "list": [
        {
            "total_tax": 2600,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 1300,
            "agent_id": 6,
            "day": "2018-06-12"
        },
        {
            "total_tax": 0,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 0,
            "agent_id": 7,
            "day": "2018-06-11"
        },
        {
            "total_tax": 5200,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 1
            },
            "month": null,
            "total_reward": 260,
            "agent_id": 7,
            "day": "2018-06-12"
        }
        ],
    }
}
```


### 获取本级代理汇总统计数据(推广数据汇总界面)
- URL: `overview_stats/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING 

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|start_date|false|string|日期起始值|
|end_date|false|string|日期截止值|


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "overview": {
            "total_tax": 2575.1400000000003,
            "total_reward": 128.75700000000003,
            "register_count": 24
        },
        "total_count": 3,
        "list": [
        {
            "total_tax": 2600,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 1300,
            "agent_id": 6,
            "day": "2018-06-12"
        },
        {
            "total_tax": 0,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 0,
            "agent_id": 7,
            "day": "2018-06-11"
        },
        {
            "total_tax": 5200,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 1
            },
            "month": null,
            "total_reward": 260,
            "agent_id": 7,
            "day": "2018-06-12"
        }
        ],
    }
}
```

### 获取下级代理统计数据(推广代理数据统计界面)
- URL: `agent_stats/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING 

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|agent_id|false|int|代理编号|
|max_reward|false|int|代理收入起始值|
|min_reward|false|int|代理收入截止值|
|max_active|false|int|新增用户起始值|
|min_active|false|int|新增用户截止值|
|start_date|false|string|日期起始值|
|end_date|false|string|日期截止值|


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "total_count": 3,
        "list": [
        {
            "total_tax": 2600,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 1300,
            "return_reward": 130,
            "agent_id": 6,
            "day": "2018-06-12"
        },
        {
            "total_tax": 0,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 0
            },
            "month": null,
            "total_reward": 0,
            "return_reward": 0,
            "agent_id": 7,
            "day": "2018-06-11"
        },
        {
            "total_tax": 5200,
            "extend": {
                "total_withdraw": 0,
                "total_recharge": 0,
                "register_count": 0,
                "active_count": 1
            },
            "month": null,
            "total_reward": 260,
            "return_reward": 26,
            "agent_id": 7,
            "day": "2018-06-12"
        }
        ],
    }
}
```


### 获取推广配置列表 
- URL: `chns/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: NULL


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "list": [{
            "id": 1111,
            "chn": "daili001",
            "qr_link": "http://xxxx.html", # 二维码url 
            "post_link": ["http://xxxxx.jpg", "http://cxxxx.jpg"], # 海报图片下载地址
            "bind_info": {}, # 绑定的相关扩展信息
        }] 
    }
}
```


### 申请新的推广连接
- URL: `bind_chn/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: NULL


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "id": 1111,
        "chn": "daili001",
        "qr_link": "http://xxxx.html", # 二维码url 
        "post_link": "http://xxxxx.jpg", # 海报图片下载地址
        "bind_info": {}, # 绑定的相关扩展信息
    }
}
```


### 获取下载地址，供落地页调用
- URL: `/agent/api/link/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS: QUERYSTRING 

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|uid|true|string|传入落地页的参数uid|
|platform|true|string|ios或android|


- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        "link": "http://xxxxxxx.com/daili0009.ipa",  # 落地页获取地址后主动跳转
        "chn": "ios_daili0009" # 落地页获取chn后写入cookie
    }
}
```


### 提交提现申请
- URL: `withdraw/submit/`
- METHOD: `POST`
- AUTH: YES 
- PARAMS: JSON字符串

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|target_type|true|int|提现方式，0:银行卡 1:支付宝|
|amount|true|int|提现金额|
|code|true|str|手机短信验证码|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
    }
}
``` 


### 获取提现记录 
- URL: `withdraw/record/`
- METHOD: `GET`
- AUTH: YES 
- PARAMS:  QUERYSTRING

|name|必选|类型|说明|
|:--:|:----|:----|:-----|
|page|false|int|页数|
|size|true|int|每页显示数目|

- RETURN:
```py
{
    "msg": "",
    "status": 0,
    "data": {
        'list': [{
            'id': 101, # 订单编号
            'trans_id': 12312313,
            'target_type': 1, # 0：银行卡  1: 支付宝
            'user_id': 81,
            'status': 1, # 状态，1:未提现 2:已提现 4:提现失败 8:自动提现中 16:禁止提现 32:返款 64:已提交第三方
            'price': 100, ＃ 提现申请金额
            'real_price': 100, ＃ 实际提现金额
            'after_cash': 0,  # 提现后剩余余额
            'info': {
                'name': '小涨',
                'alipay_no': 'd3f'
            }, # 提现账号信息，json
            'created_at': '2018-06-05 12:00:00' # 提现申请时间
            'updated_at': '2018-06-05 12:00:00' # 提现更新时间
        }],
        'total_count': 100,
        'page': 1,
        'size': 10
    }
}
``` 
