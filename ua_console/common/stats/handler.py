# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta
from common import agent_mg
from common.utils.export import redirect_to_file, gen_filename
from common.chn.db import get_platform_chn
from common.agent import db as agent_db
from common.transaction import db as transaction_db
from common.system_recharge import db as system_recharge_db
from common.utils.currency import convert_fen_to_yuan
from common.utils.mg import paginator, get_order_by

_LOGGER = logging.getLogger('agent')


def _export_financial_stats_list(items):
    filename = gen_filename('platform_stats')
    header = ('day', 'platform_id', 'agent_id', 'agent_level', 'commission_rate', 'total_active', 'total_register',
              'total_recharge', 'total_withdraw', 'total_profit', 'total_tax', 'total_cost', 'agent_withdraw')
    cn_header = (u'日期', u'游戏中文名称', u'代理ID', u'代理层级', u'提成', u'激活', u'注册', u'充值',
                 u'提现', u'营收', u'税收', u'渠道成本', u'代理提现金额')
    platform_chn = get_platform_chn()
    export_items = []
    for item in items:
        export_data = []
        for field in header:
            if field == 'platform_id':
                export_data.append(platform_chn.get(item.get('platform_id'), {}).get('label', '-'))
            else:
                export_data.append(item.get(field, '-'))
        export_items.append(export_data)
    return redirect_to_file(export_items, cn_header, filename)


def list_financial_stats(agent_id, agent_level, platform_id, chn_type, start_time, end_time, order_by,
                         to_export_file, page, size):
    offset, size = paginator(dict(page=page, size=size))
    condition = dict()
    for field in ('agent_id', 'agent_level', 'chn_type', 'platform_id'):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    if start_time:
        condition['day'] = {'$gte': start_time}
    if end_time:
        if 'day' in condition:
            condition['day']['$lt'] = end_time
        else:
            condition['day'] = {'$lt': end_time}
    (sort_key, direction) = (order_by[1:], -1) if order_by.startswith('-') else (order_by, 1)
    if to_export_file:
        financial_stats = agent_mg.financial_stats.find(condition).sort('day', -1)
        return _export_financial_stats_list(items=financial_stats)
    financial_stats = agent_mg.financial_stats.find(condition).sort(sort_key, direction).skip(offset).limit(size)
    total_count = agent_mg.financial_stats.count(condition)
    resp_items = [each_stats for each_stats in financial_stats]
    return dict(list=resp_items, total_count=total_count)


def _export_platform_stats_list(items):
    filename = gen_filename('financial_stats')
    header = ('day', 'platform_name', 'chn_type_name', 'total_active', 'total_register', 'total_recharge',
              'total_withdraw', 'total_profit', 'total_tax', 'total_cost')
    cn_header = (u'日期', u'平台', u'游戏', u'激活', u'注册', u'充值',
                 u'提现', u'营收', u'税收', u'渠道成本')
    export_items = []
    for item in items:
        export_data = []
        for field in header:
            export_data.append(item.get(field, '-'))
        export_items.append(export_data)
    return redirect_to_file(export_items, cn_header, filename)


def list_platform_stats(platform_id, chn_type, start_time, end_time, order_by, to_export_file, page, size):
    offset, size = paginator(dict(page=page, size=size))
    condition = dict(chn_type={"$exists": 0}) if not chn_type else dict(chn_type={"$in": chn_type})
    if platform_id:
        condition['platform_id'] = platform_id
    if start_time:
        condition['day'] = {'$gte': start_time}
    if end_time:
        condition.setdefault('day', {})
        condition['day']['$lt'] = end_time
    (sort_key, direction) = (order_by[1:], -1) if order_by.startswith('-') else (order_by, 1)
    if to_export_file:
        financial_stats = agent_mg.platform_stats.find(condition).sort('day', -1)
        return _export_platform_stats_list(items=financial_stats)
    platform_stats = agent_mg.platform_stats.find(condition).sort(sort_key, direction).skip(offset).limit(size)
    total_count = agent_mg.platform_stats.count(condition)
    resp_items = []
    for each_stats in platform_stats:
        resp_items.append(each_stats)
    return dict(list=resp_items, total_count=total_count)


def _format_agent_stats_item(item):
    return dict(
        id=item['_id'],
        day=item.get('day'),
        month=item.get('month'),
        agent_id=item['agent_id'],
        parent_id=item.get('parent_id'),
        agent_level=item.get('agent_level'),
        bind_phone=item.get('bind_phone'),
        username=item.get('username'),
        nickname=item.get('nickname'),
        total_tax=round(item.get('total_tax', 0), 2),
        total_reward=round(item.get('total_reward', 0), 2),
        user_tax=round(item.get('user_tax', 0), 2),
        user_reward=round(item.get('user_reward', 0), 2),
        sub_tax=round(item.get('sub_tax', 0), 2),
        sub_reward=round(item.get('sub_reward', 0), 2),
        extend_total=item.get('extend', {}).get('total', {}),
        extend_user=item.get('extend', {}).get('user', {}),
        extend_sub=item.get('extend', {}).get('sub', {}),
        system_recharge=item.get('system_recharge', 0)
    )


def _export_agent_stats_list(items):
    filename = gen_filename('agent_stats')
    header = ['day', 'agent_id', 'parent_id', 'agent_level',
              'total_tax', 'total_reward', 'user_tax', 'user_reward',
              'sub_tax', 'sub_reward', 'system_recharge',
              'extend.total.total_recharge', 'extend.total.total_withdraw',
              'extend.total.active_count', 'extend.total.register_count',
              'extend.user.total_recharge', 'extend.user.total_withdraw',
              'extend.user.active_count', 'extend.user.register_count',
              'extend.sub.total_recharge', 'extend.sub.total_withdraw',
              'extend.sub.active_count', 'extend.sub.register_count']
    cn_header = [u'日期', u'代理ID', u'上级代理ID', u'代理层级', u'总税收', u'总收益',
                 u'本级税收', u'本级收益', u'下级税收', u'下级收益', u'运营活动收入', u'总充值', u'总提现',
                 u'总激活', u'总注册', u'本级充值', u'本级提现', u'本级激活', u'本级注册',
                 u'下级充值', u'下级提现', u'下级激活', u'下级注册']
    export_items = []
    for item in items:
        item = _format_agent_stats_item(item)
        data = []
        for x in header:
            if 'extend' in x:
                k1, k2, k3 = x.split('.')
                data.append(item.get('{}_{}'.format(k1, k2), {}).get(k3, 0))
            else:
                data.append(item.get(x, '-'))
        export_items.append(data)
    return redirect_to_file(export_items, cn_header, filename)


def list_agent_stats(agent_id, parent_id, agent_level, chn, register_start_time, register_end_time,
                     page, size, order_by, to_export_file=False):
    mg, date_key = agent_mg.daily_agent_stats, 'day'
    condition = dict()
    if chn and not agent_id:
        chn_item = agent_mg.chn_pool.find_one({'chn': chn})
        if not chn_item or not chn_item['agent_id']:
            return [], 0
        agent_id = chn_item['agent_id']
    for field in ('agent_id', 'parent_id', 'agent_level'):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    if register_start_time:
        condition[date_key] = {'$gte': register_start_time}
    if register_end_time:
        if date_key in condition:
            condition[date_key]['$lt'] = register_end_time
        else:
            condition[date_key] = {'$lt': register_end_time}
    offset, size = paginator(dict(page=page, size=size))
    sort_key, direction = get_order_by(dict(order_by=order_by))
    if to_export_file:
        items = mg.find(condition).sort(sort_key, direction).limit(10000)
        return _export_agent_stats_list(items)
    items = mg.find(condition).sort(sort_key, direction).skip(offset).limit(size)
    total_count = mg.count(condition)
    resp_items = [_format_agent_stats_item(item) for item in items]
    return dict(list=resp_items, total_count=total_count)


def _export_custom_service_reward_list(items):
    filename = gen_filename('root_stats')
    header = ['day', 'agent_id', 'agent_level', 'platform_id', 'chn_type_name',
              'daily_reward', 'total_reward', 'balance', 'system_recharge']
    cn_header = [u'日期', u'代理ID', u'代理层级', u'平台', u'游戏中文名称', u'收益',
                 u'累计收益', u'余额	', u'结算']
    export_items = []
    for item in items:
        data = []
        for x in header:
            data.append(item.get(x, '-'))
        export_items.append(data)
    return redirect_to_file(export_items, cn_header, filename)


def list_custom_service_reward_stats(agent_id, parent_id, platform_id, chn_type, start_time,
                                     end_time, to_export_file, page, size):
    offset, size = paginator(dict(page=page, size=size))
    platform_conf = get_platform_chn()
    condition = dict(agent_level=0)
    if parent_id:
        condition.pop('agent_level', None)
        condition['parent_id'] = parent_id
    for field in ('agent_id', 'platform_id', 'chn_type'):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    if start_time:
        condition['day'] = {'$gte': start_time}
    if end_time:
        if 'day' in condition:
            condition['day']['$lt'] = end_time
        else:
            condition['day'] = {'$lt': end_time}
    if to_export_file:
        items = agent_mg.daily_agent_stats.find(condition).sort('day', -1).limit(10000)
        return _export_custom_service_reward_list(items)
    custom_service_reward_stats = agent_mg.daily_agent_stats.find(condition).sort('day', -1).skip(offset).limit(size)
    total_count = agent_mg.daily_agent_stats.count(condition)
    resp_items = []
    for each_stats in custom_service_reward_stats:
        agent = agent_db.get_agent(each_stats['agent_id'], to_dict=True)
        agent_stats = agent_db.get_agent_stats(each_stats['agent_id'])
        utc_date_end = datetime.strptime(each_stats['day'], '%Y-%m-%d') + timedelta(hours=16)
        item = dict(
            day=each_stats['day'],
            agent_id=each_stats['agent_id'],
            agent_level=each_stats['agent_level'],
            platform_id=platform_conf.get(agent_stats.get('platform_id'), {}).get('label', ''),
            chn_type_name=platform_conf.get(agent_stats.get('platform_id'), {}).get('chn_types', {}).get(
                agent_stats.get('chn_type'), {}).get('chn_type_name', ''),
            daily_reward=each_stats.get('total_reward', 0),
            total_reward=convert_fen_to_yuan(
                transaction_db.get_user_accumulate_reward(each_stats['agent_id'], utc_date_end) or 0),
            balance=convert_fen_to_yuan(transaction_db.get_user_date_balance(each_stats['agent_id'], utc_date_end) or 0),
            system_recharge=convert_fen_to_yuan(
                system_recharge_db.get_daily_system_recharge_amount(agent['id'], each_stats['day']))
        )
        resp_items.append(item)
    return dict(list=resp_items, total_count=total_count)
