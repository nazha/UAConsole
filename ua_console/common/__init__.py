# -*- coding: utf-8 -*-
"""
DB Model, used for ORM
"""
from pymongo import MongoClient
from common.utils.orm import ArmoryOrm
from django.conf import settings


orm = ArmoryOrm()
orm.init_conf(settings.MYSQL_CONF)

admin_orm = ArmoryOrm()
admin_orm.init_conf(settings.ADMIN_CONF)

game_mg = MongoClient(settings.MONGO_GAME_ADDR).game
agent_mg = MongoClient(settings.MONGO_AGENT_ADDR).agent
