# -*- coding: utf-8 -*-
import logging
import json
from common.withdraw import db
from common.withdraw.model import WITHDRAW_STATUS, WITHDRAW_TYPE, CHECK_STATUS
from common import agent_mg
from common.utils.export import redirect_to_file, gen_filename
from common.utils.currency import convert_fen_to_yuan
from common.utils import exceptions as err
from common.utils import tz

_LOGGER = logging.getLogger('agent')


def _export_withdraw_list(items):
    filename = gen_filename('withdraw_records')
    cn_header = (u'提现单号', u'用户ID', u'總代ID', u'提现金额', u'实际金额', u'提现方式',
                 u'提现通道', u'提现时间', u'提现状态', u'更新时间', u'姓名',
                 u'账户信息1', u'账户信息2', u'第三方流水号', u'交易时间',)
    export_items = []
    withdraw_user_id_list = [item.user_id for item in items]
    user_stats = agent_mg.agent_stats.find({'_id': {'$in': withdraw_user_id_list}})
    root_id_mapper = {user['_id']: user['path'][0] for user in user_stats}
    for item in items:
        data = item.as_dict()
        withdraw_at = tz.utc_to_local_str(data['created_at'])
        updated_at = tz.utc_to_local_str(data['updated_at'])
        withdraw_status = WITHDRAW_STATUS.get_label(data['status'])
        withdraw_info = json.loads(data['extend'] or '{}')
        withdraw_detail = withdraw_info.get('info')
        name = withdraw_detail.get('name')
        third_id, pay_date, withdraw_chn = '-', '-', '-'
        if (data['target_type'] == WITHDRAW_TYPE.ALIPAY and data['status'] == WITHDRAW_STATUS.DONE and
                'auto_trans_info' in withdraw_info):
            third_id = withdraw_info['auto_trans_info'].get('order_id') or '-'
            pay_date = withdraw_info['auto_trans_info'].get('pay_date')
            payer_no = withdraw_info['auto_trans_info'].get('payer_no')
            withdraw_chn = payer_no
        amount = convert_fen_to_yuan(data['price'])
        real_amount = convert_fen_to_yuan(data['real_price'] or data['price'])
        root_id = root_id_mapper[data['user_id']]
        data_row = [str(data['id'])[4:], data['user_id'], root_id, amount, real_amount,
                    WITHDRAW_TYPE.get_label(data['target_type']), withdraw_chn,
                    withdraw_at, withdraw_status, updated_at, name]
        if data['target_type'] == WITHDRAW_TYPE.ALIPAY:
            data_row += [withdraw_detail.get('alipay_no'), '-']
        elif data['target_type'] == WITHDRAW_TYPE.BANKCARD:
            data_row += [withdraw_detail.get('bank_no'), withdraw_detail.get('bank_name')]
        else:
            data_row += ['-', '-']
        data_row += [third_id, pay_date]
        export_items.append(data_row)
    return redirect_to_file(export_items, cn_header, filename)


def list_withdraw(withdraw_id, user_id, target_type, status, check_status, order_by,
                  created_at_start_time, created_at_end_time, updated_at_start_time, updated_at_end_time,
                  to_export_file, page, size):
    disable_paginate = True if to_export_file else False
    items, total_count = db.list_withdraw(id=withdraw_id,
                                          user_id=user_id,
                                          target_type=target_type,
                                          status=status,
                                          check_status=check_status,
                                          created_at_start_time=created_at_start_time,
                                          created_at_end_time=created_at_end_time,
                                          updated_at_start_time=updated_at_start_time,
                                          updated_at_end_time=updated_at_end_time,
                                          order_by=order_by,
                                          page=page,
                                          size=size,
                                          disable_paginate=disable_paginate)
    if to_export_file:
        return _export_withdraw_list(items)
    resp_items = []
    for item in items:
        data = item.as_dict()
        data['id'] = str(data['id'])
        data['price'] = convert_fen_to_yuan(data['price'])
        if 'real_price' in data and data['real_price'] is not None:
            data['real_price'] = convert_fen_to_yuan(data['real_price'])
        data['real_cash_price'] = data['real_price'] or data['price']
        data['after_cash'] = convert_fen_to_yuan(data['after_cash'])
        data['created_at'] = tz.utc_to_local_str(data['created_at'])
        data['updated_at'] = tz.utc_to_local_str(data['updated_at'])
        data['withdraw_info'] = json.loads(data['extend'] or '{}')
        resp_items.append(data)
    return dict(list=resp_items, page=page or 1, size=len(resp_items), total_count=total_count)


def get_withdraw(withdraw_id):
    withdraw = db.get_withdraw(withdraw_id=withdraw_id)
    withdraw = withdraw.as_dict()
    withdraw['price'] = convert_fen_to_yuan(withdraw['price'])
    if 'real_price' in withdraw:
        withdraw['real_price'] = convert_fen_to_yuan(withdraw['real_price'])
    withdraw['real_cash_price'] = withdraw['price']
    withdraw['created_at'] = tz.utc_to_local_str(withdraw['created_at'])
    withdraw['updated_at'] = tz.utc_to_local_str(withdraw['updated_at'])
    withdraw['content'] = json.loads(withdraw['extend'] or '{}')
    return withdraw


def check_withdraw(admin_id, withdraw_id, check_status, reason=None):
    if check_status not in (CHECK_STATUS.SUCC, CHECK_STATUS.FAIL):
        raise err.ParamError('invalid check_status')
    if check_status not in (CHECK_STATUS.SUCC, CHECK_STATUS.FAIL):
        raise err.ParamError('invalid check_status')
    db.check_withdraw(admin_id, withdraw_id, check_status, reason)
