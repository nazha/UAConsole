# -*- coding: utf-8 -*-
import logging
from common.cache import redis_cache
from common.agent import db as account_db
from common.withdraw.model import WITHDRAW_TYPE
from common.withdraw.db import get_today_withdraw_amount
from common.utils.currency import convert_yuan_to_fen, convert_fen_to_yuan

_LOGGER = logging.getLogger('alipay')


_LIMIT = {
    WITHDRAW_TYPE.ALIPAY: {
        'single_amount_limit': convert_yuan_to_fen(1500),
        'today_amount_limit': convert_yuan_to_fen(5000)
    },
    WITHDRAW_TYPE.BANKCARD: {
        'single_amount_limit': convert_yuan_to_fen(5000),
        'today_amount_limit': convert_yuan_to_fen(50000)
    }
}


def check_user_risk(uid, item):
    reason_list = []
    account = account_db.get_agent(uid)
    if account.role == 0:
        reason_list.append(u'黑名单用户')
    if account.agent_level <= 2:
        reason_list.append(u'内部用户')
    single_amount_limit = _LIMIT[item.target_type]['single_amount_limit']
    today_amount_limit = _LIMIT[item.target_type]['today_amount_limit']
    if item.price >= single_amount_limit:
        reason_list.append(u'当前提现金额{}超限'.format(convert_fen_to_yuan(item.price)))
    today_amount = get_today_withdraw_amount(uid)
    if today_amount >= today_amount_limit:
        reason_list.append(u'收款账号今日提现{}超过5w'.format(convert_fen_to_yuan(today_amount)))
    if len(reason_list) > 0:
        return False, reason_list
    return True, 'success'


def check_daily_risk(amount):
    interval_price = 100000
    before_price = redis_cache.get_withdraw_daily_amount()
    after_price = redis_cache.add_withdraw_daily_amount(amount)
    if after_price / interval_price > before_price / interval_price:
        # notify
        try:
            _LOGGER.warn('check_daily_risk, sms notified, current amount %s', after_price)
        except Exception as e:
            _LOGGER.exception('check_daily_risk, sms notify error, %s', e)


def get_available_withdraw_type():
    return [WITHDRAW_TYPE.BANKCARD, WITHDRAW_TYPE.ALIPAY]
"""
    channels = redis_cache.get_available_withdraw_channels()
    available_withdraw_types = []
    for channel in channels:
        if channel in [WITHDRAW_CHANNELS.JUSTPAY_ALIPAY, WITHDRAW_CHANNELS.GROUP_ALIPAY]:
            available_withdraw_types.append(WITHDRAW_TYPE.ALIPAY)
        if channel in [WITHDRAW_CHANNELS.UNIONAGENCY_BANKCARD]:
            available_withdraw_types.append(WITHDRAW_TYPE.BANKCARD)
    return available_withdraw_types
"""
