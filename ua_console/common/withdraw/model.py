# -*- coding: utf-8 -*-
from datetime import datetime
from common import orm
from common.utils.types import Enum


WITHDRAW_CHANNELS = Enum({
    'JUSTPAY_ALIPAY': ('justpay_alipay', u'justpay 支付宝'),
    'GROUP_ALIPAY': ('group_alipay', u'集团支付宝'),
    'UNIONAGENCY_BANKCARD': ('unionagency_bankcard', u'unionagency 银行卡')
})


WITHDRAW_STATUS = Enum({
    "WAIT": (1L, u"未提现"),
    "DONE": (2L, u"已提现"),
    "FAIL": (4L, u"提现失败"),
    "AUTO": (8L, u"自动提现中"),
    "FORBIDDEN": (16L, u'禁止提现'),
    "BACK": (32L, u"提现失败，返回余额"),
    "SUBMIT_TO_THIRD": (64L, u"已提交第三方提现中")
})


WITHDRAW_TYPE = Enum({
    "BANKCARD": (0L, u"银行卡"),
    "ALIPAY": (1L, u"支付宝"),
})


WITHDRAW_BASE = 10


CHECK_STATUS = Enum({
    "NULL": (0L, u"无需审核"),
    "WAITING": (1L, u"待审核"),
    "SUCC": (2L, u"审核通过"),
    "FAIL": (4L, u"审核未通过"),
})


class Withdraw(orm.Model):
    __tablename__ = 'withdraw'
    id = orm.Column(orm.BigInteger, primary_key=True, autoincrement=True)
    trans_id = orm.Column(orm.BigInteger)  # 每笔提现对应一笔交易记录
    target_type = orm.Column(orm.SmallInteger)  # 提现方式
    user_id = orm.Column(orm.BigInteger)  # 用戶id
    status = orm.Column(orm.Integer)  # 提现状态
    check_status = orm.Column(orm.Integer, default=0)  # 提现状态
    price = orm.Column(orm.Integer)  # 用户提现金额，單位為分
    real_price = orm.Column(orm.FLOAT)  # 用户实际提现金额
    after_cash = orm.Column(orm.FLOAT)  # 用户提现后剩余可提现金额
    extend = orm.Column(orm.TEXT)  # 用户使用具体信息
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


