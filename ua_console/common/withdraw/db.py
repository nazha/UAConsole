# -*- coding: utf-8 -*-
import json
import logging
from datetime import timedelta, datetime
from common import orm
from common.transaction.model import TRANSACTION_TYPE
from common.transaction.db import create_transaction
from common.withdraw.model import Withdraw, WITHDRAW_STATUS, CHECK_STATUS
from common.utils import tz
from common.utils.currency import convert_yuan_to_fen, convert_fen_to_yuan
from common.utils import exceptions as err
from common.utils.db import get_count, get_orderby, paginate

_LOGGER = logging.getLogger('trans')


def get_withdraw(withdraw_id):
    return Withdraw.query.filter(Withdraw.id == withdraw_id).first()


def list_withdraw(id, user_id, target_type, status, check_status, created_at_start_time, created_at_end_time,
                  updated_at_start_time, updated_at_end_time, order_by, page, size, disable_paginate=False):
    _filter = list()
    for field in ('id', 'user_id', 'target_type', 'status', 'check_status'):
        if locals()[field] == 0 or locals()[field]:
            _filter.append(getattr(Withdraw, field) == locals()[field])
    for time_field in ('created_at', 'updated_at',):
        start_key, end_key = '_'.join([time_field, 'start_time']), '_'.join([time_field, 'end_time'])
        if locals()[start_key]:
            _filter.append(getattr(Withdraw, time_field) >= locals()[start_key])
        if locals()[end_key]:
            _filter.append(getattr(Withdraw, time_field) < locals()[end_key])
    query = Withdraw.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby(order_by, Withdraw)
    if order_by is not None:
        query = query.order_by(order_by)
    if not disable_paginate:
        query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


_ROAD_MAP = {
    WITHDRAW_STATUS.WAIT: [WITHDRAW_STATUS.BACK, WITHDRAW_STATUS.FORBIDDEN, WITHDRAW_STATUS.DONE, WITHDRAW_STATUS.AUTO],
    WITHDRAW_STATUS.FAIL: [WITHDRAW_STATUS.BACK, WITHDRAW_STATUS.FORBIDDEN, WITHDRAW_STATUS.DONE],
    WITHDRAW_STATUS.AUTO: [WITHDRAW_STATUS.BACK, WITHDRAW_STATUS.FORBIDDEN, WITHDRAW_STATUS.DONE],
    WITHDRAW_STATUS.FORBIDDEN: [WITHDRAW_STATUS.BACK, WITHDRAW_STATUS.DONE],
    WITHDRAW_STATUS.BACK: [],
    WITHDRAW_STATUS.DONE: [],
}

CAN_BE_BACK_STATUS = (
    WITHDRAW_STATUS.WAIT,
    WITHDRAW_STATUS.FAIL,
    WITHDRAW_STATUS.FORBIDDEN,
    WITHDRAW_STATUS.AUTO
)


def check_withdraw(admin_id, withdraw_id, check_status, reason=None):
    update_info = {
        'manual_check_info': {
            'admin_id': admin_id,
            'reason': reason,
            'check_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S')
        }
    }
    withdraw = Withdraw.query.with_for_update().filter(
        Withdraw.id == withdraw_id).first()
    withdraw.check_status = check_status
    if withdraw and withdraw.status in (WITHDRAW_STATUS.WAIT,):
        updated_info = json.loads(withdraw.extend)
        updated_info.update(update_info)
        if check_status == CHECK_STATUS.SUCC:
            withdraw.status = WITHDRAW_STATUS.AUTO
        withdraw.extend = json.dumps(updated_info, ensure_ascii=False)
    withdraw.save()


def batch_check_withdraws(admin_id, withdraw_ids, check_status):
    if not withdraw_ids:
        return
    update_info = {'manual_check_info': {
        'admin_id': admin_id,
        'check_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S')
    }}
    for withdraw_id in withdraw_ids:
        withdraw = Withdraw.query.with_for_update().filter(Withdraw.id == withdraw_id).first()
        withdraw.check_status = check_status
        if withdraw and withdraw.status in (WITHDRAW_STATUS.WAIT,):
            updated_info = json.loads(withdraw.extend)
            updated_info.update(update_info)
            if check_status == CHECK_STATUS.SUCC:
                withdraw.status = WITHDRAW_STATUS.AUTO
            withdraw.extend = json.dumps(updated_info, ensure_ascii=False)
        withdraw.save(auto_commit=False)
    orm.session.commit()


def withdraw_back(admin_id, withdraw_id, reason):
    withdraw = Withdraw.query.with_for_update().filter(
        Withdraw.status.in_(CAN_BE_BACK_STATUS), Withdraw.id == withdraw_id).one()
    withdraw.status = WITHDRAW_STATUS.BACK
    extend = json.loads(withdraw.extend)
    extend['back_reason'] = reason
    extend['back_trans_info'] = dict(
        admin_id=admin_id,
        back_date=tz.local_now().strftime('%Y-%m-%d %H:%M:%S')
    )
    withdraw.extend = json.dumps(extend, ensure_ascii=False)
    withdraw.save(auto_commit=False)
    create_transaction(withdraw.user_id, TRANSACTION_TYPE.WITHDRAW_BACK,
                       u'提现失败返款', convert_yuan_to_fen(convert_fen_to_yuan(withdraw.price)))
    orm.session.commit()


def update_withdraw_status(admin_id, withdraw_id, info):
    new_status = info['status']
    old_status = get_withdraw(withdraw_id).status
    if new_status not in _ROAD_MAP[old_status]:
        raise err.ParamError(u'无效操作，请检查订单状态!')
    if new_status == WITHDRAW_STATUS.BACK:
        withdraw_back(admin_id, withdraw_id, info['reason'])
        return
    item = Withdraw.query.filter(Withdraw.id == withdraw_id).with_lockmode('update').first()
    extend = json.loads(item.extend)
    extend_to_update = dict()
    if new_status == WITHDRAW_STATUS.AUTO:
        extend_to_update = {'manual_auto_trans_info': {
            'admin_id': admin_id,
            'pay_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S'),
        }}
    if new_status == WITHDRAW_STATUS.DONE:
        extend_to_update = {'manual_done_trans_info': {
            'admin_id': admin_id,
            'pay_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S'),
        }}
    if new_status == WITHDRAW_STATUS.FORBIDDEN:
        extend_to_update = {'manual_forbidden_trans_info': {
            'admin_id': admin_id,
            'pay_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S'),
        }}
    extend.update(extend_to_update)
    item.status = new_status
    item.extend = json.dumps(extend, ensure_ascii=False)
    item.save()


def get_today_withdraw_amount(user_id, date_str=None):
    if not date_str:
        start_date = tz.get_utc_date()
    else:
        start_date = datetime.strptime(date_str, '%Y-%m-%d') - timedelta(hours=8)
    end_date = start_date + timedelta(days=1)
    amount = orm.session.query(orm.func.sum(Withdraw.real_price)).filter(
        Withdraw.user_id == user_id,
        Withdraw.status == WITHDRAW_STATUS.DONE,
        Withdraw.created_at >= start_date,
        Withdraw.created_at <= end_date).first()
    return amount[0] or 0


def withdraw_success_action(pay_id, amount, trade_no, payer_no='group_new_withdraw'):
    item = Withdraw.query.with_for_update().filter(Withdraw.id == pay_id).first()
    if item.status == WITHDRAW_STATUS.DONE:
        return True
    elif item.status != WITHDRAW_STATUS.SUBMIT_TO_THIRD:
        _LOGGER.info('Auto trans, withdraw_success_action is none %s' % str(pay_id))
        raise err.DataError('withdraw_success_action query fail')
    item.status = WITHDRAW_STATUS.DONE
    item.real_price = convert_yuan_to_fen(amount)
    trans_info = dict(
        payer_no=payer_no,
        code=10000,
        amount=amount,
        order_id=trade_no,
        out_biz_no=pay_id,
        complete_pay_date=tz.local_now().strftime('%Y-%m-%d %H:%M:%S')
    )
    updated_info = json.loads(item.extend)
    updated_info.update({'auto_trans_info': trans_info})
    item.extend = json.dumps(updated_info, ensure_ascii=False)
    item.save()
    return True


def withdraw_failed_action(pay_id, amount, code, sub_code, sub_msg, out_biz_no, payer_no='group_new_withdraw'):
    item = Withdraw.query.with_for_update().filter(Withdraw.id == pay_id).filter(
        Withdraw.status == WITHDRAW_STATUS.SUBMIT_TO_THIRD).first()
    if item is None:
        _LOGGER.info('Auto trans, withdraw_failed_action is none %s' % str(pay_id))
        raise err.DataError('withdraw_failed_action query fail')
    item.status = WITHDRAW_STATUS.FAIL
    trans_info = dict(
        payer_no=payer_no,
        amount=amount,
        code=code,
        sub_code=sub_code,
        sub_msg=sub_msg,
        out_biz_no=out_biz_no,
        complete_pay_date=tz.local_now().strftime('%Y-%m-%d %H:%M:%S')
    )
    updated_info = json.loads(item.extend)
    updated_info.update({'auto_trans_info': trans_info})
    item.extend = json.dumps(updated_info, ensure_ascii=False)
    item.save()
