# -*- coding: utf-8 -*-
"""
币种计算转换工具
"""
from decimal import Decimal


def convert_yuan_to_fen(amount):
    return int(Decimal(str(amount)) * 100)


def convert_fen_to_yuan(amount):
    return Decimal(str(int(amount) / 100.0))
