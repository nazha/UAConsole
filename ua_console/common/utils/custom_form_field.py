import json
import logging
from datetime import datetime, timedelta
from django import forms

_LOGGER = logging.getLogger(__name__)


class JsonArrayDateField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(JsonArrayDateField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return []
            json_array_date = json.loads(value)
            assert isinstance(json_array_date, list)
            assert len(json_array_date) in [0, 2]
            cleaned_data = []
            for date_str in json_array_date:
                try:
                    datetime_obj = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
                except:
                    datetime_obj = datetime.strptime(date_str, '%Y-%m-%d')
                cleaned_data.append(datetime_obj)
        except:
            raise forms.ValidationError("Invalid data in JsonArrayDateField")
        return cleaned_data


class JsonArrayField(forms.Field):
    def  __init__(self, required=True, element_is_integer=False, *args, **kwargs):
        super(JsonArrayField, self).__init__(*args, **kwargs)
        self.required = required
        self.element_is_integer = element_is_integer

    def clean(self, value):
        try:
            if not self.required and not value:
                return []
            try:
                json_array = json.loads(value)
            except:
                json_array = value
            assert isinstance(json_array, list)
            if self.element_is_integer:
                json_array = [int(element) for element in json_array]
        except:
            raise forms.ValidationError("Invalid data in JsonArrayField")
        return json_array


class JsonField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(JsonField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            try:
                value = json.loads(value)
            except:
                pass
            assert isinstance(value, dict)
        except:
            raise forms.ValidationError("Invalid data in JsonField")
        return value


class AgentAlipayJsonField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(AgentAlipayJsonField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            alipay_json = json.loads(value)
            assert isinstance(alipay_json, dict)
            assert 'name' in alipay_json and 'alipay_no' in alipay_json
        except:
            raise forms.ValidationError("Invalid data in AgentAlipayJsonField")
        return alipay_json


class DateTimeStringField(forms.Field):
    def __init__(self, required=True, to_utc=False, to_date=False, *args, **kwargs):
        super(DateTimeStringField, self).__init__(*args, **kwargs)
        self.required = required
        self.to_utc = to_utc
        self.to_date = to_date

    def clean(self, value):
        try:
            if not self.required and not value:
                return None
            try:
                datetime_obj = datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
            except:
                datetime_obj = datetime.strptime(value, '%Y-%m-%d')
            if self.to_utc:
                datetime_obj = datetime_obj - timedelta(hours=8)
            date_str = str(datetime_obj)
            if self.to_date:
                date_str = date_str.split(' ')[0]
        except:
            raise forms.ValidationError("Invalid data in AgentAlipayJsonField")
        return date_str
