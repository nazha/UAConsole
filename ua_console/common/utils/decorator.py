# -*- coding: utf-8 -*-
import logging
from functools import partial
from django.conf import settings
from django.http import HttpRequest
from future.utils import raise_with_traceback
from redis import RedisError
from functools import wraps
from common.utils import JsonResponse
from common.utils.exceptions import Error, ServerError, CacheError
from common import orm, admin_orm
from common.utils.respcode import StatusCodeDict

_LOGGER = logging.getLogger(__name__)


def _wrap2json(data):
    if data is None:
        data = {}
    elif isinstance(data, dict) or isinstance(data, list):
        return JsonResponse(dict(status=0, msg='', data=data), status=200)
    else:
        return data


def response_wrapper(func):
    def _wrapper(request, *args, **kwargs):

        if settings.DEBUG:
            return _wrap2json(func(request, *args, **kwargs))

        try:
            return _wrap2json(func(request, *args, **kwargs))
        except ServerError as e:
            _LOGGER.exception('server error!')
            return JsonResponse(
                dict(status=e.STATUS,
                     msg=unicode(e) or StatusCodeDict.get(e.STATUS)),
                status=e.HTTPCODE)
        except Error as e:
            if 'Error' == e.__class__.__name__:
                _LOGGER.exception('catched error %s in %s, uid:%s',
                                  e.__class__.__name__, request.path,
                                  request.user_id)
            else:
                _LOGGER.warn('catched error %s in %s, uid:%s',
                             e.__class__.__name__, request.path,
                             request.user_id)
            return JsonResponse(
                dict(status=e.STATUS,
                     msg=unicode(e) or StatusCodeDict.get(e.STATUS)),
                status=e.HTTPCODE)
        except Exception as e:
            _LOGGER.exception('unexcepted error!!')
            return JsonResponse(
                dict(status=Error.STATUS, msg=unicode(e) or u'未知错误'),
                status=Error.HTTPCODE)

    return _wrapper


def validate_form(form_class):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            request = args[0]
            if not isinstance(request, HttpRequest):
                raise Exception("view的第一个参数必须是request,如果用于class-based view, "
                                "请使用@method_decorator(require_login)")
            _LOGGER.info(request.DATA)
            form = form_class(request.DATA)
            if not form.is_valid():
                return JsonResponse(dict(status=Error.STATUS, msg='传参错误'), status=Error.HTTPCODE)
            kwargs['cleaned_data'] = form.cleaned_data
            return func(*args, **kwargs)
        return wrapper
    return decorator


def cache_wrapper(func):
    def _wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except RedisError as e:
            raise_with_traceback(CacheError(e))
        except Error:
            raise
        except Exception as e:
            raise_with_traceback(Error(e))

    return _wrapper
