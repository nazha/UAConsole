# -*- coding: utf-8 -*-
from functools import wraps
from future.utils import raise_with_traceback
from django.views.decorators.csrf import csrf_exempt
from django.utils.encoding import smart_unicode
from common.utils.exceptions import AuthenticateError, ParamError
from common.utils.respcode import StatusCode

DEFAULT_COUNTRY = "OTHER"
_DEFAULT_PAGE_SIZE = 10
_MAX_PAGE_SIZE = 20


def check_auth(req):
    if not req.user_id:
        raise AuthenticateError(status=StatusCode.INVALID_TOKEN)


# for class based view, use `method_decorator`
def token_required(func):
    @csrf_exempt
    @wraps(func)
    def _wrapped(req, *args, **kwargs):
        check_auth(req)
        return func(req, *args, **kwargs)

    return _wrapped


def get_client_ip(request):
    ip = None
    real_ip = request.META.get('HTTP_X_REAL_IP')
    if real_ip:
        ip = real_ip.split(',')[0]
    if not ip or ip == "unknown":
        ip = request.META.get('REMOTE_ADDR')
    return ip
