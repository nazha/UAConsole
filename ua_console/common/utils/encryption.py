import hmac
from hashlib import md5

_SALT = u"%*eQg#GH"


def encode_password(password):
    return md5(password.encode('utf-8') + _SALT).hexdigest()


def generate_sign(params, secret):
    s = ''
    for k in sorted(params.keys()):
        s += '%s=%s&' % (k, params[k])
    s = s[:-1]
    m = hmac.new(secret.encode('utf8'), s.encode('utf8'))
    sign = m.hexdigest().upper()
    return sign