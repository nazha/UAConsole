# -*- coding: utf-8 -*-


def paginator(query_dct):
    page = int(query_dct.get('page') or 1)
    size = int(query_dct.get('size') or 50)
    offset = (page - 1) * size
    return offset, size


def get_order_by(query_dct):
    if 'order_by' in query_dct:
        sort_key = query_dct['order_by']
        if sort_key.startswith('-'):
            sort_key = sort_key[1:]
            direction = -1
        else:
            direction = 1
        return sort_key, direction
    return None, None


