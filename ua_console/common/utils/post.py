# -*- coding:utf-8 -*-
# sudo pip install qiniu
# sudo pip install python-magic
import os
import re
import sys
import time
import qrcode
import logging
import urllib2
import PIL.Image as Image
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont
from qiniu import Auth, BucketManager, put_file
from common.template_manage.db import get_platform_template
from StringIO import StringIO

from django.conf import settings

_LOGGER = logging.getLogger('agent')

LOCAL_DIR = "/www/agentweb/"
QINIU_DOMAIN = 'http://agent.toodan.com/'
EXPIRE_TIME = 365 * 24 * 3600   # 1 year
Q = Auth('MTJg_LUIOU-BFGd-9kWzcvfrMAcA4bZ8lIOKPvA2',
         'QeZQXkz4tk649BnOc4OHDK25uhpOFOph6HqhqJSU')


def get_token(bucket='agent'):
    token = Q.upload_token(bucket, expires=EXPIRE_TIME)
    return token


def upload_file(file_name, file_path, bucket='agent', key_prefix='', mime_type='text/plain'):
    token = get_token(bucket)
    if key_prefix and not key_prefix.endswith('/'):
        key_prefix += '/'
    key = '%s%s' % (key_prefix, file_name)

    b = BucketManager(Q)
    b.delete(bucket, key)

    ret, info = put_file(token, key, file_path, mime_type=mime_type, check_crc=False)
    if not ret or ret.get('key') != key:
        print 'fail to upload image, info: %s' % info

    ts = int(time.time())
    return QINIU_DOMAIN + key + '?ts={}'.format(ts)


class postMaker(object):
    def __init__(self, obj_id, qr_text, post_conf, platform):
        self.obj_id = obj_id
        self.qr_text = qr_text
        self.bg_list = post_conf['bg_list']
        self.qr_size = post_conf['qr_size']
        self.qr_pos = post_conf['qr_pos']
        self.post_list = []
        self.platform = platform

    def create(self, bg_img, qr_img, textColor={'R':0, 'G':0, 'B': 0}):
        """
        @bg_img: 背景图片
        @qr_img: 用户二维码图片
        @textColor: 文字颜色，{R，G，B}
        """
        try:
            _LOGGER.info('bg_image {}'.format(bg_img))
            if bg_img.startswith('http'):
                back_img = Image.open(urllib2.urlopen(bg_img, timeout=30))
            else:
                back_img = Image.open(bg_img)
            back_img = back_img.convert("RGBA")
            draw = ImageDraw.Draw(back_img)
            draw.ink = textColor.get('R', 0) + textColor.get('G', 0) * 256 + textColor.get('B', 0) * 256 * 256
            qr_img.thumbnail(self.qr_size)
            back_img.paste(qr_img, self.qr_pos)
            return back_img
        except Exception as e:
            _LOGGER.exception('create qr img fail. %s', bg_img)

    def make_qr(self):
        qr = qrcode.QRCode(
            version=2,
            error_correction=qrcode.constants.ERROR_CORRECT_Q,
            box_size=20,
            border=0
        )
        qr.add_data(self.qr_text)
        qr.make(fit=True)
        img = qr.make_image()
        img = img.convert('RGBA')
        return img

    def upload(self):
        for post in self.post_list:
            post_file = str(self.obj_id) + "_" + post['title']
            post_path = LOCAL_DIR + post_file
            post['img'].convert('RGB').save(post_path, "jpeg")
            if settings.TEST_ENV:
                prefix = 'test'
            else:
                prefix = self.platform
            post_url = upload_file(post_file, post_path, key_prefix=prefix)
            post.update({
                'post_url': post_url,
                'post_path': post_path
            })

    def run(self):
        qr_img = self.make_qr()
        for title, bg_conf in self.bg_list.iteritems():
            bg_img, weight = bg_conf['url'], bg_conf['weight']
            img = self.create(bg_img, qr_img)
            self.post_list.append({
                'title': title,
                'img': img,
                'weight': weight
            })
        _LOGGER.info('post_list: %s', self.post_list)
        self.upload()

    def fetch_result(self):
        return self.post_list


QR_LINK = 'http://m.h2resource.com/worldcup.html/?uid={}'
POST_RE = re.compile(r'.*(post\d)+.*')


def create_link(obj_id, qr_link, template_id=None, platform='dltgjm', chn_type='quanmdl'):
    templates = get_platform_template(platform=platform, template_id=template_id, chn_type=chn_type)
    post_links = []
    post_list = []
    for template in templates:
        post_conf = settings.POST_BG_GROUP[template.template_type]
        local_path = settings.POST_DIR + platform + '/' + template.template_type + '/' + template.title
        _LOGGER.info("local path: {}".format(local_path))
        if os.path.exists(local_path):
            path = local_path
        else:
            path = template.resource
        post_conf['bg_list'] = {
            template.title: {
                'url': path,
                'weight': template.weight
            }
        }
        pMaker = postMaker(obj_id, qr_link, post_conf, platform)
        pMaker.run()
        l = pMaker.fetch_result()
        post_list.extend(l)
    post_list.sort(key=lambda x:x['weight'], reverse=True)
    for post in post_list:
        post_links.append(post['post_url'])
    return post_links


if __name__ == "__main__":
    obj_id = sys.argv[1]
    qr_link = QR_LINK.format(obj_id)
    post_links = create_link(obj_id, qr_link, 'template_b')
    print obj_id, qr_link, post_links
