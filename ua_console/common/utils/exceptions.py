# -*- coding: utf-8 -*-
from common.utils.respcode import HttpCode, StatusCode


# Basic Exceptions
class Error(Exception):
    HTTPCODE = HttpCode.SERVER_ERROR
    STATUS = StatusCode.UNKNOWN_ERROR

    def __init__(self, msg='', httpcode=None, status=None):
        super(Error, self).__init__(msg)
        if httpcode:
            self.HTTPCODE = httpcode
        if status:
            self.STATUS = status


# General Exceptions
class ClientError(Error):
    HTTPCODE = HttpCode.BAD_REQUEST


class ServerError(Error):
    pass


class ParamError(ClientError):
    STATUS = StatusCode.PARAM_REQUIRED


class DataError(ClientError):
    STATUS = StatusCode.DATA_ERROR


class DbError(ServerError):
    STATUS = StatusCode.DB_ERROR


class CacheError(ServerError):
    STATUS = StatusCode.CACHE_ERROR


# Specific Exception
class ProtocolError(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.HTTPS_REQUIRED


class AuthenticateError(DataError):
    HTTPCODE = HttpCode.UNAUTHORIZED


class PermissionError(ClientError):
    STATUS = StatusCode.NOT_ALLOWED
    HTTPCODE = HttpCode.FORBIDDEN


class NotImplementedError(ServerError):
    HTTPCODE = HttpCode.NOT_IMPLEMENTED


class ResourceInsufficient(ClientError):
    HTTPCODE = HttpCode.FORBIDDEN
    STATUS = StatusCode.RESOURCE_INSUFFICIENT
