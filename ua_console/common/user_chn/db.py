# -*- coding:utf-8 -*-
from datetime import datetime
from common.user_chn.model import ManualBindHistory
from common.agent.db import get_agent
from common.utils import exceptions as err
from common.utils.db import get_count, get_orderby, paginate


def list_manual_bind_chn(platform_id=None, chn_type=None, bind_chn=None, user_id=None,
                         agent_id=None, agent_level=None):
    _filter = list()
    for field in ('platform_id', 'chn_type', 'user_id', 'agent_id', 'agent_level'):
        if locals()[field] == 0 or locals()[field]:
            _filter.append(getattr(ManualBindHistory, field) == locals()[field])
    if bind_chn:
        _filter.append(ManualBindHistory.bind_chn.contains('{}'.format(bind_chn)))
    query = ManualBindHistory.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby('-created_at', ManualBindHistory)
    if order_by is not None:
        query = query.order_by(order_by)
    return query.all(), total_count


def insert_manual_bind_chn(platform_id, chn_type, original_chn, bind_chn, agent_id, user_id, operator_id, note):
    agent = get_agent(agent_id, to_dict=True)
    if not agent:
        raise err.ParamError(u'代理不存在')
    manual_bind_chn = ManualBindHistory()
    manual_bind_chn.platform_id = platform_id
    manual_bind_chn.chn_type = chn_type
    manual_bind_chn.agent_id = agent_id
    manual_bind_chn.user_id = user_id
    manual_bind_chn.original_chn = original_chn
    manual_bind_chn.bind_chn = bind_chn
    manual_bind_chn.agent_level = agent['agent_level']
    manual_bind_chn.operator_id = operator_id
    manual_bind_chn.note = note
    manual_bind_chn.created_at = manual_bind_chn.updated_at = datetime.utcnow()
    manual_bind_chn.save()
