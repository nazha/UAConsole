# -*- coding: utf-8 -*-
from common import admin_orm


class ManualBindHistory(admin_orm.Model):
    __tablename__ = 'manual_bind_history'
    id = admin_orm.Column(admin_orm.BigInteger, primary_key=True)
    platform_id = admin_orm.Column(admin_orm.BigInteger)
    chn_type = admin_orm.Column(admin_orm.VARCHAR)
    original_chn = admin_orm.Column(admin_orm.VARCHAR)
    bind_chn = admin_orm.Column(admin_orm.VARCHAR)
    user_id = admin_orm.Column(admin_orm.BigInteger)
    agent_id = admin_orm.Column(admin_orm.BigInteger)
    agent_level = admin_orm.Column(admin_orm.Integer)
    operator_id = admin_orm.Column(admin_orm.BigInteger)
    note = admin_orm.Column(admin_orm.VARCHAR)
    created_at = admin_orm.Column(admin_orm.DATETIME)
    updated_at = admin_orm.Column(admin_orm.DATETIME)
