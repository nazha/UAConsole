# -*- coding: utf-8 -*-
import requests
import json
import logging
from django.conf import settings
from common import agent_mg
from common.utils import exceptions as err
from common.utils.encryption import generate_sign
from common.chn.db import get_platform_chn
from common.user_chn import db
from common.chn.model import CHN_STATUS
from django.http import HttpResponse


_LOGGER = logging.getLogger('agent')
logging.basicConfig(level=logging.INFO)
logging.getLogger('requests').setLevel(logging.ERROR)


def list_manual_assign_chn(platform_id, chn_type, bind_chn, user_id, agent_id, agent_level):
    platform_chn = get_platform_chn()
    if platform_id and platform_id not in platform_chn:
        raise err.ParamError(u'平台不存在')
    if chn_type and chn_type not in platform_chn[platform_id]['chn_types']:
        raise err.ParamError(u'渠道类型参数错误')
    bind_history, total_count = db.list_manual_bind_chn(platform_id=platform_id, chn_type=chn_type, bind_chn=bind_chn,
                                                        user_id=user_id, agent_id=agent_id, agent_level=agent_level)
    resp_items = list()
    for item in bind_history:
        data = item.as_dict()
        data['chn_type'] = platform_chn[data['platform_id']]['chn_types'][data['chn_type']]['chn_type_name']
        data['platform_id'] = platform_chn[data['platform_id']]['label']
        resp_items.append(data)
    return dict(items=resp_items, total_count=total_count, ts=platform_chn)


def manual_assign_chn(platform_id, chn_type, agent_id, user_id, bind_chn, operator_id, note=''):
    if 'ios_' not in bind_chn:
        raise err.ParamError(u'仅开放IOS手动绑定')
    target_chn = bind_chn.replace('ios_', '')
    _LOGGER.info({'agent_id': agent_id, 'chn_type': chn_type, 'project_id': platform_id, 'chn': target_chn})
    chn_resource = agent_mg.chn_pool.find_one({'agent_id': agent_id, 'chn_type': chn_type, 'project_id': platform_id,
                                               'chn': target_chn})
    if not chn_resource:
        raise err.ParamError(u'该渠道：{} 非归属代理：{}'.format(chn_type, agent_id))
    user_chn_info = get_user_chn_info(platform_id, user_id)
    original_chn = user_chn_info.get('bind_chn')
    maestro_api_key = settings.MAESTRO_API_KEY
    params = {
        'agent_chn': str(bind_chn),
        'user_id': user_id,

        "key": "agent"
    }
    sign = generate_sign(params, maestro_api_key)
    params['sign'] = "maestro_secret"
    assert platform_id in settings.MAESTRO_PLATFORM_MAPPER, u'修改失败'
    maestro_platform_id = settings.MAESTRO_PLATFORM_MAPPER.get(platform_id)
    api_url = settings.MAESTRO_MANUAL_UPDATE_AGENT_CHN.format(maestro_platform_id)
    res = requests.post(api_url, data=json.dumps(params), timeout=10,
                        headers={'Content-Type': 'application/json;charset=UTF-8'})
    try:
        res_json = json.loads(res.content)
    except:
        raise err.ServerError(u'修改失败')
    if res_json.get('status') != 0:
        raise err.ServerError(u'修改失败')
    db.insert_manual_bind_chn(platform_id, chn_type, original_chn, bind_chn, agent_id, user_id, operator_id, note=note)
    return dict()


def get_user_chn_info(platform_id, user_id):
    platform_conf = get_platform_chn()
    maestro_api_key = settings.MAESTRO_API_KEY
    params = {
        'user_id': user_id,
        "key": "agent"
    }
    sign = generate_sign(params, maestro_api_key)
    params['sign'] = "maestro_secret"
    assert platform_id in settings.MAESTRO_PLATFORM_MAPPER, u'修改失败'
    maestro_platform_id = settings.MAESTRO_PLATFORM_MAPPER.get(platform_id)
    api_url = settings.MAESTRO_GET_USER_AGENT_INFO.format(maestro_platform_id)
    res = requests.get(api_url, params=params, timeout=10)
    try:
        res_json = json.loads(res.content)
    except:
        raise err.ServerError(u'用户不存在')
    if res_json.get('status') != 0:
        raise err.ServerError(u'查询失败')
    data = res_json.get('data')
    data['platform'] = platform_conf[platform_id]['label']
    return res_json.get('data')


def get_ios_chn(platform_id=None, chn_type=None):
    condition = dict({"status": CHN_STATUS.USED})
    if platform_id:
        condition['platform_id'] = platform_id
    if chn_type:
        condition['chn_type'] = chn_type
    target_chn = agent_mg.chn_pool.find(condition)
    return ['ios_' + chn['chn'] for chn in target_chn]
