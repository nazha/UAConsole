# -*- coding:utf-8 -*-
import logging
from datetime import datetime
from sqlalchemy import and_
from common import orm
from common.utils.db import get_orderby, paginate
from common.announce.model import Announcement, ANNOUNCE_TYPE, SYSTEM_USER_ID

_LOGGER = logging.getLogger('agent')


def list_announcement(start_time, end_time, page, size):
    _filter = list()
    if start_time or end_time:
        if start_time:
            _filter.append(Announcement.start_time >= str(start_time))
        if end_time:
            _filter.append(Announcement.end_time < str(end_time))
    query = Announcement.query.filter(*_filter)
    current_time_str = str(datetime.now())
    query = query.filter(~and_(Announcement.announce_type == ANNOUNCE_TYPE.URGENT,
                               Announcement.end_time <= current_time_str))
    order_by = get_orderby('-created_at', Announcement)
    query = query.order_by(order_by)
    total_count = query.count()
    query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def create_announcement(title, content, start_time, end_time):
    announce = Announcement()
    announce.announce_agent_id = SYSTEM_USER_ID
    announce.announce_type = ANNOUNCE_TYPE.REGULAR
    announce.title = title
    announce.content = content
    announce.start_time = start_time
    announce.end_time = end_time
    announce.created_at = announce.updated_at = datetime.utcnow()
    announce.save()


def update_announcement(announcement_id, title, content, start_time, end_time):
    to_update_dict = dict()
    for field in ('title', 'content', 'start_time', 'end_time'):
        if locals()[field] == 0 or locals()[field]:
            to_update_dict[field] = locals()[field]
    Announcement.query.filter(Announcement.id == announcement_id).update(to_update_dict)
    orm.session.commit()


def delete_announcement(announcement_id):
    announce = Announcement.query.with_for_update().filter(Announcement.id == announcement_id).first()
    if announce:
        announce.delete()



