# -*- coding: utf-8 -*-=
from datetime import datetime
from common import orm
from common.utils.types import Enum

ANNOUNCE_TYPE = Enum({
    "REGULAR": (0, u"一般公告"),
    "URGENT": (1, u"紧急公告")
})

SYSTEM_USER_ID = 0


class Announcement(orm.Model):
    __tablename__ = 'announcement'
    id = orm.Column(orm.BigInteger, primary_key=True)
    announce_agent_id = orm.Column(orm.Integer)
    announce_type = orm.Column(orm.Integer)
    title = orm.Column(orm.VARCHAR)
    content = orm.Column(orm.VARCHAR)
    start_time = orm.Column(orm.DATETIME)
    end_time = orm.Column(orm.DATETIME)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
