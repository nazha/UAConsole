# -*- coding:utf-8 -*-
import logging
from datetime import timedelta
from common.announce import db

_LOGGER = logging.getLogger('agent')


def list_announcement(start_time, end_time, page, size):
    items, total_count = db.list_announcement(start_time, end_time, page, size)
    resp = []
    for item in items:
        announce = item.as_dict()
        announce['start_time'] = str(item.start_time + timedelta(hours=8))
        announce['end_time'] = str(item.end_time + timedelta(hours=8))
        resp.append(announce)
    return dict(list=resp, total_count=total_count)


def create_announcement(title, content, start_time, end_time):
    db.create_announcement(title=title,
                           content=content,
                           start_time=start_time,
                           end_time=end_time)


def update_announce(announcement_id, title, content, start_time, end_time):
    db.update_announcement(announcement_id, title, content, start_time, end_time)


def delete_announcement(announcement_id):
    db.delete_announcement(announcement_id)


