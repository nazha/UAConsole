# -*- coding: utf-8 -*-
from datetime import datetime
from common import orm
from common.utils.types import Enum


# out_trans_id + trans_type必须满足唯一
TRANSACTION_TYPE = Enum({
    "AWARD": (1, "award"),  # 分成收益
    "WITHDRAW": (2, "withdraw"),  # 提现
    "WITHDRAW_BACK": (3, "withdraw back"),  # 提现失败返款
    "SYSTEM_RECHARGE": (4, "system_recharge")
})


class Transaction(orm.Model):
    __tablename__ = 'transaction'
    id = orm.Column(orm.BigInteger, primary_key=True)
    type = orm.Column(orm.Integer)
    user_id = orm.Column(orm.Integer)
    title = orm.Column(orm.VARCHAR)
    amount = orm.Column(orm.Integer)
    balance = orm.Column(orm.Integer)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
