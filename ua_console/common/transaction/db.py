# -*- coding:utf-8 -*-
import logging
from datetime import datetime
from common import orm
from sqlalchemy import func
from common.transaction.model import Transaction
from common.agent.model import User
from common.utils import exceptions as err
from common.utils.currency import convert_fen_to_yuan

_LOGGER = logging.getLogger('agent')


def _increase_balance(user_id, amount):
    """
    增加帳戶餘額
    :param user_id: 用戶id
    :param amount: 增加的錢，單位是分
    :return: user: type dict
    """
    assert user_id > 0
    assert isinstance(amount, int)
    _LOGGER.info('user_id increase account amount: %s %s' % (user_id, amount))
    if amount <= 0:
        _LOGGER.info('user_id increase less zero amount transaction: %s %s' % (user_id, amount))
        return
    user = User.query.with_for_update().filter(User.id == user_id).first()
    user.balance += amount
    user.save(auto_commit=False)
    orm.session.flush()
    _LOGGER.info('the current account: %s' % user.balance)
    return user.as_dict()


def _decrease_balance(user_id, amount):
    """
    扣除帳戶餘額
    :param user_id: 用戶id
    :param amount: 扣除的錢，單位是分
    :return: user: type dict
    """
    assert user_id > 0
    assert isinstance(amount, int)
    _LOGGER.info('user_id decrease account amount: %s %s' % (user_id, amount))
    if amount <= 0:
        _LOGGER.info('user_id decrease less zero amount transaction: %s %s' % (user_id, amount))
        return
    user = User.query.with_for_update().filter(User.id == user_id).first()
    if user.balance < amount:
        raise err.ResourceInsufficient(u'当前余额不足')
    user.balance -= amount
    user.save(auto_commit=False)
    orm.session.flush()
    _LOGGER.info('the current account: %s' % user.balance)
    return user.as_dict()


def _create_transaction(user_id, trans_type, title, amount, balance):
    trans = Transaction()
    trans.type = trans_type
    trans.user_id = user_id
    trans.title = title
    trans.amount = amount
    trans.balance = balance
    trans.created_at = trans.updated_at = datetime.utcnow()
    trans.save(auto_commit=False)
    orm.session.flush()
    return trans.as_dict()


def get_out_trans_id(out_trans_id, trans_type):
    query = Transaction.query.filter(Transaction.out_trans_id == out_trans_id,
                                     Transaction.type == trans_type).first()
    if query:
        return False
    return True


def create_transaction(user_id, trans_type, title, amount):
    """
    :param user_id: 用戶id
    :param trans_type: 交易類型
    :param title: 交易標題
    :param amount: 交易金額，單位是分
    :return: trans: type dict
    """
    assert amount != 0
    # start transaction
    if amount > 0:
        user = _increase_balance(user_id, amount)
    elif amount < 0:
        user = _decrease_balance(user_id, -amount)
    amount, balance = convert_fen_to_yuan(amount), user['balance']
    # insert transaction
    trans_dct = _create_transaction(user_id, trans_type, title, amount, balance)
    return trans_dct


def get_user_date_balance(user_id, date):
    last_trans = Transaction.query.filter(
        Transaction.created_at < date,
        Transaction.user_id == user_id
    ).order_by(Transaction.created_at.desc()).first()
    return last_trans.balance if last_trans else 0


def get_user_accumulate_reward(user_id, date):
    query = orm.session.query(func.sum(Transaction.amount)).filter(Transaction.user_id == user_id,
                                                                   Transaction.created_at < date).all()
    return query[0][0] if query and query[0] else 0


