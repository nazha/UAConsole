# -*- coding: utf-8 -*-
import logging
from common.admin import db
from common.admin.model import ROLE
from common.utils import exceptions as err
from common.utils import tz

_LOGGER = logging.getLogger(__name__)


def login_user(email, password):
    user = db.get_user_by_email(email)
    if not user:
        raise err.AuthenticateError(u'用户不存在')
    if user['password'] != db.encode_password(password):
        raise err.AuthenticateError(u'密码错误')
    user_token = db.create_user_token(user['id'])
    user['token'] = user_token['token']
    user.pop('password', '')
    return user


def logout_user(user_id, user_token):
    db.logout_user(user_id, user_token)


def list_users(user_role, show_all_user, page=1, size=20):
    items, total_count = db.list_users(user_role, show_all_user)
    resp_items = []
    for item in items:
        data = item.as_dict()
        data.pop('password', None)
        data['updated_at'] = tz.utc_to_local_str(data['updated_at'])
        data['created_at'] = tz.utc_to_local_str(data['created_at'])
        resp_items.append(data)
    return dict(list=resp_items, page=page, size=size, total_count=total_count)


def update_user(editor_user_id, user_id, nickname, password, role, perm):
    editor_user = db.get_user(user_id=editor_user_id, to_dict=True)
    user = db.get_user(user_id=user_id, to_dict=True)
    if user_id != editor_user['id'] and user['role'] >= editor_user['role']:
        raise err.PermissionError(u'无权限修改上级用户')
    if user_id != editor_user['id'] and role and editor_user['role'] <= role:
        raise err.PermissionError(u'无权限赋予高于自身的身份')
    if editor_user['role'] != ROLE.ADMIN and perm:
        perm_to_update = perm.split('|')
        editor_perm = editor_user['perm'].split('|')
        if set(perm_to_update) - set(editor_perm):
            raise err.PermissionError(u'无权限赋予高于自身的权限')
    db.update_user(user_id, nickname, password, role, perm)
    if password:
        db.logout_user(user_id)


def get_user(user_id):
    user = db.get_user(user_id, to_dict=True)
    user['perm'] = user['perm'].split('|') if user['perm'] else []
    user['created_at'] = tz.utc_to_local_str(user['created_at'])
    user['updated_at'] = tz.utc_to_local_str(user['updated_at'])
    user.pop('password', None)
    return user


def create_user(email, password, nickname):
    db.create_user(email, password, nickname)


def delete_user(editor_user_id, user_id):
    editor_user = db.get_user(user_id=editor_user_id, to_dict=True)
    user = db.get_user(user_id=user_id, to_dict=True)
    if user and user['role'] >= editor_user['role']:
        raise err.PermissionError(u'不可删除上级用户')
    db.delete_user(user_id)


def list_perm(page, size):
    items, total_count = db.list_perm(page, size)
    resp_items = []
    for item in items:
        data = item.as_dict()
        data['updated_at'] = tz.utc_to_local_str(data['updated_at'])
        data['created_at'] = tz.utc_to_local_str(data['created_at'])
        resp_items.append(data)
    return dict(list=resp_items, page=page, size=len(resp_items), total_count=total_count)








