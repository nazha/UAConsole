# -*- coding: utf-8 -*-
from datetime import datetime
from common import admin_orm as orm
from common.utils.types import Enum


ROLE = Enum({
    "FORBIDDEN": (0, "forbidden"),
    "USER": (1, "user"),
    "SENIOR_USER": (2, "senior_user"),
    "MANAGER": (3, "manager"),
    "SENIOR_MANAGER": (4, "senior_manager"),
    "ADMIN": (5, "admin")
})

PERMISSION = Enum({
    "NONE": (0, "none"),
    "READ": (1, "read"),
    "WRITE": (2, "write"),
})

ACTION = {
    'POST': 1,
    'PUT': 2,
    'PATCH': 2,
    'DELETE': 3
}

RESOURCE = {
}


class User(orm.Model):
    __tablename__ = 'user'
    id = orm.Column(orm.BigInteger, primary_key=True)
    email = orm.Column(orm.VARCHAR)
    nickname = orm.Column(orm.VARCHAR)
    password = orm.Column(orm.VARCHAR)
    role = orm.Column(orm.SmallInteger)
    perm = orm.Column(orm.VARCHAR, default=0)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
    mch_ids = orm.Column(orm.VARCHAR)


class Permission(orm.Model):
    __tablename__ = 'permission'
    id = orm.Column(orm.BigInteger, primary_key=True)
    url = orm.Column(orm.VARCHAR)
    permission = orm.Column(orm.SmallInteger)
    min_role = orm.Column(orm.SmallInteger)
    user_perm = orm.Column(orm.VARCHAR) # 需要满足的用户权限
    desc = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class UserToken(orm.Model):
    __tablename__ = 'user_token'
    user_id = orm.Column(orm.BigInteger, primary_key=True)
    token = orm.Column(orm.VARCHAR, primary_key=True)
    deleted = orm.Column(orm.SmallInteger)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class Record(orm.Model):
    __tablename__ = 'record'
    id = orm.Column(orm.BigInteger, primary_key=True)
    resource = orm.Column(orm.VARCHAR)
    resource_id = orm.Column(orm.BigInteger)
    action = orm.Column(orm.SmallInteger)
    param = orm.Column(orm.Text)
    ontent = orm.Column(orm.Text)
    operator = orm.Column(orm.BigInteger)
    created_at = orm.Column(orm.DATETIME)
