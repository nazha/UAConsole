# -*- coding: utf-8 -*-
from hashlib import md5
from datetime import datetime
from common import admin_orm as orm
from common.admin.model import User, Permission, UserToken, ROLE
from common.utils.db import get_count
from common.utils import id_generator
from common.utils import exceptions as err
from common.utils.db import get_orderby, paginate

_SALT = u"%*egQ#HG"


def encode_password(passwd):
    return md5(passwd.encode('utf-8') + _SALT).hexdigest()


def create_user(email, password, nickname):
    user = User()
    user.email = email
    user.password = encode_password(password)
    user.nickname = nickname
    user.role = ROLE.FORBIDDEN
    user.created_at = user.updated_at = datetime.utcnow()
    user.save()


def update_user(user_id, nickname, password, role, perm):
    if password:
        password = encode_password(password)
    to_update_dict = dict()
    for field in ('nickname', 'password', 'role', 'perm',):
        if locals()[field] == 0 or locals()[field]:
            to_update_dict[field] = locals()[field]
    User.query.filter(User.id == user_id).update(to_update_dict)
    orm.session.commit()


def delete_user(user_id):
    user = User.query.with_for_update().filter(User.id == user_id).first()
    if user:
        user.delete()


def list_users(user_role, show_all_user=False):
    _filter = list()
    if not show_all_user:
        if user_role < 4:
            return [], 0
        _filter.append(User.role < user_role)
    query = User.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby('-created_at', User)
    if order_by is not None:
        query = query.order_by(order_by)
    return query.all(), total_count


def get_user(user_id, to_dict=False):
    user = User.query.filter(User.id == user_id).first()
    if not to_dict:
        return user
    return user.as_dict() if user else dict()


def get_user_by_email(email):
    user = User.query.filter(User.email == email).first()
    return user.as_dict() if user else {}


def active_user(user_id):
    user = User.query.filter(User.id == user_id).first()
    user.save()


def create_user_token(user_id):
    user_token = UserToken()
    user_token.token = id_generator.generate_uuid()
    user_token.deleted = 0
    user_token.user_id = user_id
    user_token.created_at = user_token.updated_at = datetime.utcnow()
    user_token.save()
    return user_token.as_dict()


def logout_user(user_id, specific_token=None):
    _filter = list(getattr(UserToken, 'user_id') == user_id)
    if specific_token:
        _filter.append(getattr(UserToken, 'token') == specific_token)
    UserToken.query.filter(*_filter).update({'deleted': 1, 'updated_at': datetime.utcnow()})
    orm.session.commit()


def get_online_info(user_id, token):
    return UserToken.query.filter(
        UserToken.user_id == user_id).filter(UserToken.token == token).first()


def list_perm(page, size):
    order_by = get_orderby('-created_at', Permission)
    query = Permission.query.order_by(order_by)
    total_count = get_count(query)
    query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def create_perm(url, permission, min_role):
    perm = Permission()
    perm.url = url
    perm. permission = permission
    perm.min_role = min_role
    perm.created_at = perm.updated_at = datetime.utcnow()
    perm.save()
    return perm


def get_perm(url=None, perm=None, id=None):
    if id:
        return Permission.query.filter(Permission.id == id).first()
    elif url and perm:
        return Permission.query.filter(Permission.url == url, Permission.permission == perm).first()


def update_perm(perm_id, min_role, user_perm, desc, editor_info):
    if editor_info.role != ROLE.ADMIN:
        raise err.PermissionError(u'权限不足')
    perm = Permission.query.filter(Permission.id == perm_id).with_for_update().first()
    if perm:
        perm.min_role = min_role
        perm.desc = desc
        perm.user_perm = user_perm
        perm.save()


def get_nickname_by_id(user_id):
    query = User.query.filter(User.id == user_id).first()
    return query.nickname
