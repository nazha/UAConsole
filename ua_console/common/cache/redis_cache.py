# -*- coding: utf-8 -*-
import logging
from common.cache import ProxyAgent, prefix_key
from common.utils.tz import left_seconds_today
from common.utils.decorator import cache_wrapper


_LOGGER = logging.getLogger(__name__)
_LOCK_TIMEOUT = 10
_ACCOUNT_EXPIRE = 3600
_TREND_COUNT = 1200  # 最近1200期的趋势图数据


@cache_wrapper
def set_image_code(user_id, code_str):
    key = prefix_key('imagecode:%s' % user_id)
    ProxyAgent().setex(key, 60, code_str)


@cache_wrapper
def get_image_code(user_id):
    key = prefix_key('imagecode:%s' % user_id)
    return ProxyAgent().get(key)


@cache_wrapper
def clear_image_code(user_id):
    key = prefix_key('imagecode:%s' % user_id)
    return ProxyAgent().delete(key)


_CODE_EXPIRE_TIME = 3 * 60  # 3 mins


@cache_wrapper
def save_auth_code(phone, code):
    ProxyAgent().setex(prefix_key(phone), _CODE_EXPIRE_TIME, code)


@cache_wrapper
def delete_auth_code(phone):
    ProxyAgent().delete(prefix_key(phone))


@cache_wrapper
def get_auth_code(phone):
    return ProxyAgent().get(prefix_key(phone))


@cache_wrapper
def get_auth_code_ttl(phone):
    return ProxyAgent().ttl(prefix_key(phone))


@cache_wrapper
def add_withdraw_daily_amount(amount):
    key = prefix_key('daily:withdraw')
    exists = ProxyAgent().exists(key)
    current_price = ProxyAgent().incrby(key, int(amount))
    if not exists:
        ttl = left_seconds_today()
        ProxyAgent().expire(key, ttl)
    return int(current_price)


@cache_wrapper
def get_withdraw_daily_amount():
    key = prefix_key('daily:withdraw')
    return int(ProxyAgent().get(key) or 0)


@cache_wrapper
def set_available_withdraw_channels(*channels):
    key = prefix_key('available_withdraw_channels')
    ProxyAgent().delete(key)
    return ProxyAgent().sadd(key, *channels)


@cache_wrapper
def get_available_withdraw_channels():
    key = prefix_key('available_withdraw_channels')
    return ProxyAgent().smembers(key) or []
