# -*- coding: utf-8 -*-
import logging
from common import agent_mg
from common.utils import tz
from common.utils.types import Enum
from bson.objectid import ObjectId
from common.utils.mg import paginator
from common.agent import db as account_db
from common.utils import exceptions as err

_LOGGER = logging.getLogger('agent')

CHN_RESOURCE_STATUS = Enum({
    "AVAILABLE": (0, u"可用"),
    "PENDING": (1, u"待审核"),
    "USED": (2, u"已分配"),
    "LOADING": (3, u"生成中"),
})


def get_platform_name_by_platform_id(platform_id):
    platform_chn = get_platform_chn()
    return platform_chn.get(platform_id, {}).get('name')


def transform_platform_chn(platform_chn, platform_name_as_key=False):
    return dict([(
        int(item['platform_id']) if not platform_name_as_key else item['platform_name'], {
            'id': int(item['platform_id']),
            'label': item['platform_label'],
            'name': item['platform_name'],
            'chn_types': dict([
                (chn_type['name'], chn_type) for chn_type in item['chn_types']])
        }
    ) for item in platform_chn])


def get_platform_chn():
    items = agent_mg.platform_conf.find()
    return transform_platform_chn(items)


def get_agent_chn_resource(agent_id, platform_id=None, chn_type=None):
    condition = dict(agent_id=agent_id)
    if platform_id:
        condition['project_id'] = platform_id
    if chn_type:
        condition['chn_type'] = chn_type
    return agent_mg.chn_pool.find(condition)


def get_chn_resource(status, agent_level, root_id, bind_at_start_time, bind_at_end_time, created_at_start_time,
                     created_at_end_time, updated_at_start_time, updated_at_end_time, project_id,
                     chn_type, chn, order_by='-_id', page=1, size=20):
    offset, size = paginator(dict(page=page, size=size))
    condition = {'status': {'$exists': 1}}
    for field in ('status', 'root_id', 'project_id', 'chn_type', 'chn',):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    if status:
        condition['status'] = status
    if agent_level:
        agents = agent_mg.agent_stats.find({'agent_level': agent_level})
        agent_id_list = [int(agent['_id']) for agent in agents]
        condition['agent_id'] = {'$in': agent_id_list}
    if root_id:
        condition['root_id'] = {'$eq': root_id}
    for time_field in ('bind_at', 'created_at', 'updated_at'):
        start_key, end_key = '_'.join([time_field, 'start_time']), '_'.join([time_field, 'end_time'])
        if locals()[start_key]:
            condition[time_field] = {'$gte': locals()[start_key]}
        if locals()[end_key]:
            if time_field in condition:
                condition[time_field]['$lt'] = locals()[end_key]
            else:
                condition[time_field] = {"$lt": locals()[end_key]}
    (sort_key, direction) = (order_by[1:], -1) if order_by.startswith('-') else (order_by, 1)
    chn_resource = agent_mg.chn_pool.find(condition).sort(sort_key, direction).skip(offset).limit(size)
    ret = dict(
        total=agent_mg.chn_pool.count(condition),
        root_users=account_db.get_root_user(),
        platform_chn=get_platform_chn()
    )
    resp_item = []
    for resource in chn_resource:
        if 'status' in resource and resource['status'] == 2:
            agent_stats = agent_mg.agent_stats.find_one({'_id': resource.get('agent_id')})
            if agent_stats:
                del agent_stats['_id']
                resource.update(agent_stats)
                resource['bind_at'] = tz.utc_to_local_str(resource['bind_at'])
        for time_field in ('created_at', 'updated_at',):
            if time_field in resource and resource[time_field]:
                resource[time_field] = tz.utc_to_local_str(resource[time_field])
        resp_item.append(resource)
    ret['chn'] = resp_item
    return ret


def batch_delete_chn(ids):
    ids = [ObjectId(obj_id) for obj_id in ids]
    valid_delete_chn = agent_mg.chn_pool.find({'_id': {'$in': ids},
                                               'status': {'$in': [CHN_RESOURCE_STATUS.AVAILABLE,
                                                                  CHN_RESOURCE_STATUS.LOADING]}})
    valid_ids = [chn['_id'] for chn in valid_delete_chn]
    agent_mg.chn_pool.delete_many({'_id': {'$in': valid_ids},
                                   'status': {'$in': [CHN_RESOURCE_STATUS.AVAILABLE,
                                                      CHN_RESOURCE_STATUS.LOADING]}})
    agent_mg.chn_pool.delete_many({'source_id': {'$in': valid_ids}})


def create_or_update_chn_type(project_name, chn_type_key, chn_type_name, android_link,
                              ios_link, qr_link, to_update):
    platform_conf = agent_mg.platform_conf.find_one({'platform_name': project_name})
    if not platform_conf:
        raise err.ParamError(u"无此平台类型")
    for chn_type_obj in platform_conf['chn_types']:
        if chn_type_key == chn_type_obj['name']:
            if not to_update:
                raise err.ParamError(u"渠道类型重复")
            agent_mg.platform_conf.update_one({'platform_name': project_name},
                                              {'$pull': {'chn_types': {'name': chn_type_key}}})
    new_chn_type = dict(
        name=chn_type_key,
        chn_type_name=chn_type_name,
        android_link=android_link,
        ios_link=ios_link,
        qr_link=qr_link
    )
    agent_mg.platform_conf.update_one({'platform_name': project_name},
                                      {'$push': {'chn_types': new_chn_type}}, upsert=False)


def get_chn_resource_filter(username, platform_name, chn_type, chn_resource_items):
    resp_item = []
    for item in chn_resource_items:
        if username and username != item['username']:
            continue
        if platform_name and platform_name != item['platform_name']:
            continue
        if chn_type and chn_type != item['chn_type']:
            continue
        resp_item.append(item)
    return resp_item


def get_root_chn_distribution(username_filter, platform_name_filter, chn_type_filter, page, size):
    offset, size = paginator(dict(page=page, size=size))
    platform_chn = get_platform_chn()
    root_user_dct = {root['id']: root['username'] for root in account_db.get_root_user()}
    root_user_stats = agent_mg.agent_stats.find({'_id': {'$in': root_user_dct.keys()}})
    root_user_activate_chn_type = {root['_id']: root.get('marketing_setup', {}).get('activated_platform')
                                   for root in root_user_stats}
    resp_items = []
    for root_id, username in root_user_dct.iteritems():
        root_activate_platform = root_user_activate_chn_type[root_id]
        platform_list = [platform['platform_name'] for platform in root_activate_platform]
        for platform, platform_item in platform_chn.iteritems():
            if platform_item['name'] not in platform_list:
                continue
            chn_type_dct = {chn['name']: chn['chn_type_name'] for platform in root_activate_platform
                            for chn in platform['chn_types']}
            chn_type_detail = agent_mg.chn_pool.aggregate([
                {'$match': {'project_id': platform, 'root_id': root_id}},
                {'$group': {
                    '_id': '$chn_type',
                    'available_count': {
                        "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.AVAILABLE]}, 1, 0]}
                    },
                    'used_count': {
                        "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.USED]}, 1, 0]}
                    },
                    'pending_count': {
                        "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.PENDING]}, 1, 0]}
                    },
                    'loading_count': {
                        "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.LOADING]}, 1, 0]}
                    }
                }}
            ])
            allocated_chn_type_list = []
            for type_detail in chn_type_detail:
                chn_type = type_detail['_id']
                if chn_type is None:
                    continue
                chn_type_name = platform_chn[platform]['chn_types'][chn_type]['chn_type_name']
                allocated_chn_type_list.append(chn_type)
                chn_type_item = dict(
                    root_id=root_id,
                    platform_id=platform,
                    username=username,
                    platform_name=platform_item['name'],
                    chn_type=chn_type,
                    chn_type_name=chn_type_name,
                    available_chn_count=type_detail['available_count'],
                    used_chn_count=type_detail['used_count'],
                    pending_chn_count=type_detail['pending_count'],
                    loading_chn_count=type_detail['loading_count'],
                )
                resp_items.append(chn_type_item)
            for chn_type, chn_type_item in platform_item['chn_types'].iteritems():
                if chn_type not in allocated_chn_type_list and chn_type in chn_type_dct.keys():
                    chn_type_item = dict(
                        root_id=root_id,
                        platform_id=platform,
                        username=username,
                        platform_name=platform_item['name'],
                        chn_type=chn_type,
                        chn_type_name=chn_type_item['chn_type_name'],
                        available_chn_count=0,
                        used_chn_count=0,
                        pending_chn_count=0,
                        loading_chn_count=0
                    )
                    resp_items.append(chn_type_item)
    resp_items = get_chn_resource_filter(username_filter, platform_name_filter, chn_type_filter, resp_items)
    return dict(list=resp_items[offset:offset+size+1], total_count=len(resp_items))


def get_chn_type_resource(page, size):
    offset, size = paginator(dict(page=page, size=size))
    platform_chn = get_platform_chn()
    resp_items = []
    for platform, platform_item in platform_chn.iteritems():
        chn_type_detail = agent_mg.chn_pool.aggregate([
            {'$match': {'project_id': platform}},
            {'$group': {
                '_id': '$chn_type',
                'available_count': {
                    "$sum": {"$cond": [{"$eq": ["$root_id", 0]}, 1, 0]}
                },
                'used_count': {
                    "$sum": {"$cond": [{"$ne": ["$root_id", 0]}, 1, 0]}
                },
                'pending_count': {
                    "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.PENDING]}, 1, 0]}
                },
                'loading_count': {
                    "$sum": {"$cond": [{"$eq": ["$status", CHN_RESOURCE_STATUS.LOADING]}, 1, 0]}
                }
            }}
        ])
        allocated_chn_type_list = []
        for type_detail in chn_type_detail:
            chn_type = type_detail['_id']
            if chn_type is None:
                continue
            chn_type_name = platform_item['chn_types'][chn_type]['chn_type_name']
            allocated_chn_type_list.append(chn_type)
            chn_type_item = dict(
                platform_id=platform,
                platform_name=platform_item['name'],
                chn_type=chn_type,
                chn_type_name=chn_type_name,
                available_chn_count=type_detail['available_count'],
                used_chn_count=type_detail['used_count'],
                pending_chn_count=type_detail['pending_count'],
                loading_chn_count=type_detail['loading_count']
            )
            resp_items.append(chn_type_item)
        for chn_type, chn_type_item in platform_item['chn_types'].iteritems():
            if chn_type not in allocated_chn_type_list:
                chn_type_item = dict(
                    platform_id=platform,
                    platform_name=platform_item['name'],
                    chn_type=chn_type,
                    chn_type_name=chn_type_item['chn_type_name'],
                    available_chn_count=0,
                    used_chn_count=0,
                    pending_chn_count=0,
                    loading_chn_count=0
                )
                resp_items.append(chn_type_item)
    return dict(list=resp_items[offset:offset + size + 1], total_count=len(resp_items))
