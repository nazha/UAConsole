# -*- coding:utf-8 -*-
import re
import logging
from common import agent_mg
from common.agent import db as account_db
from common.utils import exceptions as err

_LOGGER = logging.getLogger('agent')

MAX_COUNT = 5
POST_RE = re.compile(r'.*(post\d)+.*')


def allocation_chn_to_root(root_id, project_id, chn_type, allocate_amount):
    assert allocate_amount != 0
    root = account_db.get_agent(root_id)
    if root.agent_level != 0:
        raise err.ParamError(u"非总推号")
    if allocate_amount > 0:
        available_resource_count = agent_mg.chn_pool.count({'root_id': 0, 'chn_type': chn_type,
                                                            'project_id': project_id, 'status': 0})
        if allocate_amount > available_resource_count:
            raise err.ParamError(u"可分配资源不足，仅有%s个%s渠道资源" % (available_resource_count, chn_type))
        resource_to_allocate = agent_mg.chn_pool.find(
            {'root_id': 0, 'chn_type': chn_type, 'project_id': project_id, 'status': 0}).limit(allocate_amount)
        ids_to_allocate = []
        for resource in resource_to_allocate:
            ids_to_allocate.append(resource['_id'])
        agent_mg.chn_pool.update_many({'root_id': 0, 'chn_type': chn_type, 'project_id': project_id, 'status': 0,
                                       '_id': {'$in': ids_to_allocate}}, {'$set': {'root_id': root_id}})
    else:
        restored_amount = abs(allocate_amount)
        allocated_resource_count = agent_mg.chn_pool.count(
            {'root_id': root_id, 'chn_type': chn_type, 'project_id': project_id, 'status': 0})
        if restored_amount > allocated_resource_count:
            raise err.ParamError(u"可收回资源不足，仅分配%s个%s渠道资源" % (allocated_resource_count, chn_type))
        resource_to_restore = agent_mg.chn_pool.find(
            {'root_id': root_id, 'chn_type': chn_type, 'project_id': project_id, 'status': 0}).limit(allocate_amount)
        ids_to_restore = []
        for resource in resource_to_restore:
            ids_to_restore.append(resource['_id'])
        agent_mg.chn_pool.update_many({'root_id': root_id, 'chn_type': chn_type, 'project_id': project_id, 'status': 0,
                                       '_id': {'$in': ids_to_restore}}, {'$set': {'root_id': 0}})
    return dict()
