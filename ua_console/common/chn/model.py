# -*- coding: utf-8 -*-

from common.utils.types import Enum

PLATFORM_TYPE = Enum({
    "DUROTAR": (0, "durotar"),
    "DARK": (1, "dltgjm"),
    "ZS": (2, "dltgzs"),
    "DARK3": (3, "dltghj3"),
    "TT": (4, "dltgtt"),
    "DARK4": (5, "dltghj4")
})


BIZ_MODEL_TYPE = Enum({
    "OFFICIAL": ('quanmdl', u"官方模型"),
    "YSTGDL": ('ystgdl', u"官方模型2")
})

CHN_STATUS = Enum({
    "AVAILABLE": (0, u"可用"),
    "PENDING": (1, u"审核中"),
    "USED": (2, u"已分配"),
    "CREATING": (3, u"资源生成中")
})
