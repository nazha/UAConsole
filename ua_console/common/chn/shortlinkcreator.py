# -*- coding: utf-8 -*-
from urlparse import urlparse
import requests
import json
import logging
from django.conf import settings
from common.utils.encryption import generate_sign

SHORT_LINK_PLATFORM_MAP = {
    "durotar": "dark1",
    "dltgjm": "dark2",
    "dltgzs": "zs",
    "dltghj3": "dark3",
    "dltgtt": "tt",
    "dltghj4": "dark3"
}

_LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
logging.getLogger('requests').setLevel(logging.ERROR)
AUTH_USER_HEADER_NAME = 'HMAC-USER'
AUTH_SIGN_HEADER_NAME = 'HMAC-SIGN'

SYSTEM_NAME = "promo"


def create_short_link(platform, long_link, creator_id="lucas"):
    short_links = create_short_links(platform, [long_link], creator_id)
    return short_links[0] if short_links else None


def create_short_links(platform, long_links, creator_id):
    assert isinstance(long_links, list)
    assert platform in SHORT_LINK_PLATFORM_MAP
    target = SHORT_LINK_PLATFORM_MAP.get(platform)
    path_query_list = []
    hosts_list = []
    for long_link in long_links:
        parsed_url = urlparse(long_link)
        path_query = '?'.join([parsed_url.path, parsed_url.query]) if parsed_url.query else parsed_url.path
        path_query_list.append(path_query)
        base_url = '://'.join([parsed_url.scheme, parsed_url.netloc])
        hosts_list.append(base_url)
    data = {"system": SYSTEM_NAME,
            "created_by": creator_id,
            "target": target,
            "long_links": path_query_list}
    headers = get_headers(data, settings.SHORT_LINK_API_USER, settings.SHORT_LINK_API_KEY)
    service_url = settings.SHORT_LINK_HOST + "/links"
    result = requests.post(url=service_url, json=data, headers=headers)
    short_codes = process_result(result)
    if short_codes:
        short_links = ['/s/'.join([host, code]) for code, host in zip(short_codes, hosts_list)]
        return short_links


def process_result(r):
    if r.status_code == requests.codes.ok:
        data = json.loads(r.content)
        if data.get('status') == 200:
            return data.get("data", {}).get("short_links")
        else:
            _LOGGER.info("Get short link error code:%s message:%s",
                         data.get('code'), data.get('message'))
    else:
        _LOGGER.info("Short link service is unavailable: %s", r.raise_for_status())


def get_headers(params, user, secret):
    sign = generate_sign(params=params, secret=secret)
    return {AUTH_USER_HEADER_NAME: user,
            AUTH_SIGN_HEADER_NAME: sign}


if __name__ == '__main__':
    short_link = create_short_link(1, 'https://s.test.com/ads/stat/landpage/show/?uid=5ca875e6b74197121044f6ca&channel_id=dltgjm_test_quanmdl_001')
    print short_link
