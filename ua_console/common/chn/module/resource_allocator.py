# -*- coding:utf-8 -*-
from common import agent_mg
from datetime import datetime
from common.chn.model import CHN_STATUS
from common.utils import exceptions as err


class ChannelResourceAllocator(object):
    def __init__(self, root_id, platform_id, chn_type):
        self.root_id = root_id
        self.platform_id = platform_id
        self.chn_type = chn_type
        self.chn_resource = None
        self.chn_instance = None

    def _update_agent_activated_platform(self, agent_id):
        assert self.chn_resource
        agents = agent_mg.agent_stats.find({'path': agent_id}).sort('agent_level', -1)
        total_count = agent_mg.agent_stats.count({'path': agent_id})
        activated_platform_mapper = dict()
        modification_status_list = [False] * total_count
        for idx, agent in enumerate(agents):
            activated_platform_mapper[agent['_id']] = agent['marketing_setup']['activated_platform']
            for platform in activated_platform_mapper[agent['_id']]:
                if platform['platform_id'] != self.platform_id:
                    continue
                for chn_type_item in platform['chn_types']:
                    if chn_type_item['name'] != self.chn_type:
                        continue
                    chn_type_item['activated'] = True
                    if agent['_id'] == agent_id:
                        chn_type_item['resource'] = self.chn_resource
                    modification_status_list[idx] = True
        if False in modification_status_list:
            raise err.DataError(u'数据异常，洽客服人员')
        for agent_id, activated_platform in activated_platform_mapper.iteritems():
            agent_mg.agent_stats.update_one({'_id': agent_id}, {'$set': {
                'marketing_setup.activated_platform': activated_platform
            }}, upsert=True)

    def update_activate_state(self, ancestor_path):
        agents = agent_mg.agent_stats.find({'_id': {"$in": ancestor_path}})
        platform_id = self.platform_id
        chn_type = self.chn_type
        for agent in agents:
            activated_platform = agent['marketing_setup']['activated_platform']
            for platform in activated_platform:
                for chn_item in platform['chn_types']:
                    if platform['platform_id'] == platform_id and chn_item['name'] == chn_type:
                        chn_item['activated'] = True
            agent_mg.agent_stats.update_one({'_id': agent['_id']}, {'$set': {
                'marketing_setup.activated_platform': activated_platform
            }}, upsert=True)

    @classmethod
    def activated_chn_resource(cls, agent_id, platform_id, chn_type):
        agent_stats = agent_mg.agent_stats.find_one({'_id': agent_id})
        if not agent_stats:
            raise err.DataError(u'代理不存在')
        allocator = cls(root_id=agent_stats['path'][0], platform_id=platform_id, chn_type=chn_type)
        allocator.allocate_chn_resource()
        try:
            allocator._update_agent_activated_platform(agent_id)
            allocator.update_activate_state(agent_stats['path'])
        except:
            allocator.roll_back_chn_resource()
        else:
            allocator.bind_chn_resource(agent_id)

    def allocate_chn_resource(self):
        """
        僅分配給渠道資源給於代理層級角色(agent_role==AGENT_ROLE.REGULAR)
        """
        cond = dict(status=CHN_STATUS.AVAILABLE, root_id=self.root_id,
                    project_id=self.platform_id, chn_type=self.chn_type)
        candidate_chn = agent_mg.chn_pool.find_one_and_update(cond, {'$set': {'status': CHN_STATUS.PENDING}})
        if not candidate_chn:
            raise err.ResourceInsufficient(u'无渠道资源')
        self.chn_resource = [candidate_chn['chn'], 'ios_' + candidate_chn['chn']]
        self.chn_instance = candidate_chn

    def roll_back_chn_resource(self):
        agent_mg.chn_pool.update_one({'_id': self.chn_instance['_id']},
                                     {'$set': {'status': CHN_STATUS.AVAILABLE}})

    def bind_chn_resource(self, agent_id):
        agent_mg.chn_pool.update_one({'_id': self.chn_instance['_id']},
                                     {'$set': {'status': CHN_STATUS.USED, 'agent_id': agent_id,
                                               'bind_at': datetime.utcnow()}})
        agent_mg.chn_pool.update_one({'source_id': self.chn_instance['_id']}, {'$set': {'agent_id': agent_id}})