# -*- coding: utf-8 -*-
import re
import os
import sys
import logging
# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'
from common import agent_mg
from common.utils.post import create_link
from common.chn.model import PLATFORM_TYPE
from common.chn.shortlinkcreator import create_short_link

_LOGGER = logging.getLogger('worker')


def change_post():
    items = agent_mg.chn_pool.find({'status': {'$exists': 1}, 'qr_link': {'$not': re.compile('uid')}})
    for item in items:
        obj_id = item['_id']
        chn = item['chn']
        new_qr_link = 'http://s.dl256.com:8082/ads/stat/landpage/click/?uid={}&channel_id={}'.format(
            obj_id, chn)
        new_short_link = create_short_link(PLATFORM_TYPE.to_dict()[item['project_id']], new_qr_link)
        new_post_links = create_link(obj_id, new_qr_link)
        agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': {
            'ios_link': 'https://plist4.ueswap.com/',
            'qr_link': new_qr_link,
            'short_link': new_short_link,
            'post_links': new_post_links
        }})


def update_post(refresh=False):
    """
    refresh - False: 保留旧的海报, True: 重新生成
    """
    items = agent_mg.chn_pool.find({'updated_post': {'$exists': 0}})
    for item in items:
        obj_id = item['_id']
        qr_link = item['qr_link']
        post_links = item['post_links']
        if len(post_links) == 7:
            continue
        new_post_links = []
        post_links_a = create_link(obj_id, qr_link, 'template_a')
        post_links_b = create_link(obj_id, qr_link, 'template_b')
        new_post_links.extend(post_links_b)
        new_post_links.extend(post_links_a)
        if not refresh:
            post_links.extend(new_post_links)
        else:
            post_links = new_post_links
        to_set = {
            'post_links': post_links,
            'updated_post': 1
        }
        if post_links:
            to_set['default_post_link'] = post_links[0]
        agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': to_set})
        print 'updated chn ', item['chn'], post_links


def update_qrlink():
    items = agent_mg.chn_pool.find({'status': {'$exists': 1}})
    for item in items:
        try:
            obj_id = item['_id']
            qr_link = item['qr_link']
            qr_link_list = qr_link.split('/')
            position = -1
            for idx, item in enumerate(qr_link_list):
                if item == 'ads':
                    position = idx
            qr_link = '/'.join(['https://s.jybgpo.com/'] + qr_link_list[position:])
            post_links = []
            post_links_a = create_link(obj_id, qr_link, 'template_a')
            post_links_b = create_link(obj_id, qr_link, 'template_b')
            post_links.extend(post_links_b)
            post_links.extend(post_links_a)
            short_link = create_short_link(PLATFORM_TYPE.to_dict()[item['project_id']], qr_link)
            agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': {
                'qr_link': qr_link,
                'short_link': short_link,
                'post_links': post_links
            }})
            print 'updated chn', qr_link, post_links, obj_id
        except Exception as err:
            _LOGGER.exception("exception: %s", err)


def update_android_link():
    items = agent_mg.chn_pool.find({'ios_link': re.compile('https://offline-apk.viyamall.com')})
    print agent_mg.chn_pool.count({'ios_link': re.compile('https://offline-apk.viyamall.com')})
    for item in items:
        obj_id = item['_id']
        ios_link = item['ios_link']
        if 'https://offline-apk.viyamall.com' not in ios_link:
            continue
        ios_link = ios_link.replace('https://offline-apk.viyamall.com', 'https://offline-apk.nonmp.com')
        agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': {
            'ios_link': ios_link,
        }})
        print 'updated chn', ios_link


if __name__ == "__main__":
    while agent_mg.chn_pool.count({'status': {'$exists': 1}}) > 0:
        try:
            update_qrlink()
        except:
            pass
