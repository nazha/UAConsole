# -*- coding: utf-8 -*-
import os
import sys
import logging
from datetime import datetime
from bson.objectid import ObjectId
from common.utils.exceptions import ParamError
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'
from common import agent_mg
from common.chn.db import get_platform_chn
from common.utils.post import create_link
from async.celery import app
from common.chn.model import BIZ_MODEL_TYPE, PLATFORM_TYPE, CHN_STATUS
from common.utils import exceptions as err
from common.chn.shortlinkcreator import create_short_link

_LOGGER = logging.getLogger('worker')


@app.task(name='common.create_resource')
def create_qr_link(qr_link_template, obj_id, chn, platform, chn_type, to_recreate=False):
    chn_obj = agent_mg.chn_pool.find_one({'_id': obj_id}) or {}
    if 'agent_id' in chn_obj:
        status = chn_obj['status']
    else:
        status = CHN_STATUS.AVAILABLE
    _LOGGER.info('ready to create qr link {} {} {}'.format(obj_id, chn, platform))
    qr_link = qr_link_template.format(obj_id, chn)
    short_link = create_short_link(platform, qr_link)
    try:
        post_links = create_link(obj_id, qr_link, platform=platform, chn_type=chn_type)
    except Exception as e:
        _LOGGER.exception('create post link fail. %s', e)
        return
    to_set = {
        'status': status,
        'qr_link': qr_link,
        'short_link': short_link,
        'post_links': post_links,
        'updated_at': datetime.utcnow()
    }
    if to_recreate and post_links:
        to_set['default_post_link'] = post_links[0]
    agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': to_set})
    print 'created chn', chn, qr_link, post_links
    _LOGGER.info('created chn {} {} {}'.format(chn, qr_link, post_links))


def create_resource(project_id, chn_type, qr_link_template, ios_link, android_link, chn_range):
    """
    :param project_id: 平台號
    :param chn_type: 渠道類型
    :param qr_link_template: 二維碼鏈結模板
    :param ios_link: ios鏈接模板
    :param android_link: 安卓鏈結模板
    :param chn_range: 渠道生成編號範圍
    :return:
    """
    platform_chn = get_platform_chn()
    if project_id not in platform_chn:
        raise ParamError(u'不支持该平台%s' % project_id)
    if chn_type not in platform_chn[project_id]['chn_types']:
        raise ParamError(u'不支持该渠道类型%s' % chn_type)

    chn_template = '_'.join([PLATFORM_TYPE.to_dict()[project_id], chn_type, '{}'])
    chn_list = [chn_template.format(str(x).zfill(3)) for x in xrange(chn_range[0], chn_range[1] + 1)]
    for chn in chn_list:
        if '{}' in ios_link:
            if not chn.startswith('ios_'):
                tmp_link = ios_link.format('ios_'+chn)
            else:
                tmp_link = ios_link.format(chn)
        else:
            tmp_link = ios_link
        chn_resource = {
            'project_id': project_id,
            'chn_type': chn_type,
            'root_id': 0,
            'chn': chn,
            'android_link': android_link.format(chn),
            'ios_link': tmp_link,
            'biz_model': BIZ_MODEL_TYPE.OFFICIAL,
            'status': CHN_STATUS.CREATING,
            'created_at': datetime.utcnow()
        }
        try:
            res = agent_mg.chn_pool.insert_one(chn_resource)
        except:
            raise err.ParamError(u'渠道资源{}已存在'.format(chn))
        obj_id = res.inserted_id
        agent_mg.chn_pool.insert_one({
            'chn': 'ios_' + chn,
            'source_id': obj_id
        })
        _LOGGER.info('create_resource chn:{}, obj_id:{}'.format(chn, obj_id))
        create_qr_link.delay(qr_link_template, obj_id, chn, platform=PLATFORM_TYPE.to_dict()[project_id],
                             chn_type=chn_type)


def batch_recreate_resource(chn_obj_ids, to_create_poster):
    """
    重新生成资源
    ids: chn ids
    """
    chn_obj_ids = [ObjectId(chn_obj_id) for chn_obj_id in chn_obj_ids]
    platform_chn = get_platform_chn()
    target_channels = agent_mg.chn_pool.find({'_id': {'$in': chn_obj_ids}})
    for chn_obj in target_channels:
        obj_id = chn_obj['_id']
        chn = chn_obj['chn']
        project_id = chn_obj['project_id']
        chn_type = chn_obj['chn_type']
        chn_conf = platform_chn[project_id]['chn_types'][chn_type]
        qr_link_template = chn_conf['qr_link']
        android_link_template = chn_conf['android_link']
        ios_link_template = chn_conf['ios_link']
        if to_create_poster:
            create_qr_link.delay(qr_link_template, obj_id, chn, platform=PLATFORM_TYPE.to_dict()[project_id],
                                 chn_type=chn_type, to_recreate=True)
        else:
            if '{}' in ios_link_template:
                if not chn.startswith('ios_'):
                    ios_link = ios_link_template.format('ios_' + chn)
                else:
                    ios_link = ios_link_template.format(chn)
            else:
                ios_link = ios_link_template
            agent_mg.chn_pool.update_one({'_id': obj_id}, {'$set': {
                'android_link': android_link_template.format(chn),
                'ios_link': ios_link,
                'updated_at': datetime.utcnow()
            }})

