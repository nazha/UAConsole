# -*- coding: utf-8 -*-
import logging
from common import orm
from common import agent_mg
from common.agent.db import get_username_by_id
from common.admin.db import get_nickname_by_id
from common.system_recharge import db as system_recharge_db
from common.transaction.model import TRANSACTION_TYPE
from common.transaction.db import create_transaction
from common.utils import tz
from common.utils import exceptions as err
from common.utils.tz import utc_to_local_str
from common.utils.currency import convert_yuan_to_fen, convert_fen_to_yuan

_LOGGER = logging.getLogger('agent')


def create_system_trans(trans_id, system_recharge_batch, operator_id):
    operator = get_nickname_by_id(operator_id)
    query = system_recharge_db.get_trans_id(trans_id)
    if not query:
        raise err.ParamError(u'订单重复')
    for each_system_recharge in system_recharge_batch:
        each_system_recharge['user_name'] = get_username_by_id(each_system_recharge['user_id'])
        if not each_system_recharge['user_name']:
            raise err.ParamError(u'未找到相关用户, %s', each_system_recharge['user_id'])
    batch_id = system_recharge_db.create_system_batch(trans_id, operator, len(system_recharge_batch))
    for each_system_recharge in system_recharge_batch:
        user_id = each_system_recharge['user_id']
        amount = convert_yuan_to_fen(each_system_recharge['amount'])
        note = each_system_recharge['note']
        user_name = each_system_recharge['user_name']
        date_str = tz.local_now().strftime('%Y-%m-%d')
        system_recharge_db.create_system_detail(trans_id, batch_id, user_name, user_id, amount, operator, note)
        create_transaction(user_id, TRANSACTION_TYPE.SYSTEM_RECHARGE, u'系统上分', amount)
        inc_value = dict(system_recharge=float(convert_fen_to_yuan(amount)))
        agent_mg.daily_agent_stats.update_one({'_id': '%s-%s' % (date_str, user_id)},
                                              {'$inc': inc_value}, upsert=False)
        agent_mg.agent_stats.update_one({'_id': user_id},
                                        {'$inc': inc_value}, upsert=False)
        _LOGGER.info('system recharge user_id: %s, amount: %s, operator: %s', user_id, amount, operator)
    orm.session.commit()


def list_all_detail(user_id, created_at_start_time, created_at_end_time, date_start_time, date_end_time,
                    page, size):
    system_recharges, total_count = system_recharge_db.list_all_detail(user_id, created_at_start_time,
                                                                       created_at_end_time, date_start_time,
                                                                       date_end_time, page, size)
    resp_items = []
    for item in system_recharges:
        data = item.as_dict()
        data.pop('batch_id', None)
        data['created_at'] = utc_to_local_str(data['created_at'])
        data['updated_at'] = utc_to_local_str(data['updated_at'])
        data['amount'] = convert_fen_to_yuan(data['amount'])
        resp_items.append(data)
    return dict(list=resp_items, page=page or 1, size=len(resp_items), total_count=total_count)


