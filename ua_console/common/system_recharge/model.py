# -*- coding: utf-8 -*-
from common import orm
from datetime import datetime


class SystemRechargeBatch(orm.Model):
    """
    系统充值-表单
    """
    __tablename__ = "system_recharge_batch"
    id = orm.Column(orm.BigInteger, primary_key=True)
    trans_id = orm.Column(orm.BigInteger)  # 交易表id，唯一性(前端给)
    operator = orm.Column(orm.VARCHAR)  # 操作人员
    count = orm.Column(orm.Integer)  # 批量人数
    note = orm.Column(orm.VARCHAR)  # 备注
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class SystemRechargeDetail(orm.Model):
    """
    系统充值
    """
    __tablename__ = "system_recharge_detail"
    id = orm.Column(orm.BigInteger, primary_key=True)
    batch_id = orm.Column(orm.BigInteger)  # SystemRechargeBatch ForeingKey
    trans_id = orm.Column(orm.VARCHAR)  # 交易id
    user_name = orm.Column(orm.VARCHAR)
    user_id = orm.Column(orm.BigInteger)
    amount = orm.Column(orm.BigInteger)
    operator = orm.Column(orm.VARCHAR)  # 操作人员
    note = orm.Column(orm.VARCHAR)  # 备注
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)
