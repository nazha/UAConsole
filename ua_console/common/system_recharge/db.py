# -*- coding: utf-8 -*-
import logging
from datetime import datetime, timedelta
from common import orm
from common.system_recharge.model import SystemRechargeBatch, SystemRechargeDetail
from common.utils.db import get_count, get_orderby, paginate
from common.utils import tz

_LOGGER = logging.getLogger('agent')


def list_all_detail(user_id, created_at_start_time, created_at_end_time, date_start_time, date_end_time,
                    page, size):
    _filter = list()
    if user_id:
        _filter.append(SystemRechargeDetail.user_id == user_id)
    for time_field in ('created_at', 'date'):
        start_key, end_key = '_'.join([time_field, 'start_time']), '_'.join([time_field, 'end_time'])
        if locals()[start_key]:
            _filter.append(getattr(SystemRechargeDetail, time_field) > locals()[start_key])
        if locals()[end_key]:
            _filter.append(getattr(SystemRechargeDetail, time_field) < locals()[end_key])
    query = SystemRechargeDetail.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby('-created_at', SystemRechargeDetail)
    if order_by is not None:
        query = query.order_by(order_by)
    query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def create_system_batch(trans_id, operator, count, content=''):
    form = SystemRechargeBatch()
    form.trans_id = trans_id
    form.operator = operator
    form.count = count
    form.note = content
    form.created_at = form.updated_at = datetime.utcnow()
    form.save(auto_commit=False)
    orm.session.flush()
    return form.id


def create_system_detail(trans_id, batch_id, user_name, user_id, amount,
                         operator, content):
    detail = SystemRechargeDetail()
    detail.trans_id = trans_id
    detail.batch_id = batch_id
    detail.user_name = user_name
    detail.user_id = user_id
    detail.amount = amount
    detail.operator = operator
    detail.note = content
    detail.created_at = detail.updated_at = datetime.utcnow()
    detail.save(auto_commit=False)
    orm.session.flush()
    return detail.as_dict()


def get_trans_id(trans_id):
    query = SystemRechargeBatch.query.with_for_update().filter(
            SystemRechargeBatch.trans_id == trans_id).first()
    if query:
        return False
    return True


def get_daily_system_recharge_amount(user_id, date_str=None):
    if not date_str:
        start_date = tz.get_utc_date()
    else:
        start_date = datetime.strptime(date_str, '%Y-%m-%d') - timedelta(hours=8)
    end_date = start_date + timedelta(days=1)
    amount = orm.session.query(orm.func.sum(SystemRechargeDetail.amount)).filter(
        SystemRechargeDetail.user_id == user_id,
        SystemRechargeDetail.created_at >= start_date,
        SystemRechargeDetail.created_at <= end_date).first()
    return amount[0] or 0


def get_users_daily_system_recharge(date_str=None):
    if not date_str:
        start_date = tz.get_utc_date()
    else:
        start_date = datetime.strptime(date_str, '%Y-%m-%d') - timedelta(hours=8)
    end_date = start_date + timedelta(days=1)
    system_recharge = orm.session.query(SystemRechargeDetail.user_id, orm.func.sum(SystemRechargeDetail.amount)).filter(
        SystemRechargeDetail.created_at >= start_date,
        SystemRechargeDetail.created_at <= end_date).group_by(SystemRechargeDetail.user_id).all()
    return system_recharge
