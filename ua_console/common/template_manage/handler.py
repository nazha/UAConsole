# -*- coding: utf-8 -*-
import os
from django.conf import settings
from common.template_manage import db
from common.utils import post
from common.utils import exceptions as err


def upload_file(tm_file, dir_name, platform):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    destination = open(os.path.join(dir_name, tm_file.name), 'wb+')
    for chunk in tm_file.chunks():
        destination.write(chunk)
    destination.close()
    url = post.upload_file(tm_file.name, os.path.join(dir_name, tm_file.name), key_prefix=platform)
    return url


def upload_template(tm_file, platform, chn_type, template_type, weight):
    if db.get_platform_template(platform=platform, title=tm_file.name):
        raise err.ParamError(u'命名重复')
    dir_name = os.path.join(settings.POST_DIR + platform, template_type + '/')
    resource_url = upload_file(tm_file, dir_name, platform)
    db.insert_template_manage(resource_url, tm_file.name, template_type, platform, chn_type, weight)


def list_template_manage(platform, chn_type):
    items, total_count = db.list_template_manage(platform, chn_type)
    resp_items = [item.as_dict() for item in items]
    return dict(list=resp_items, total_count=total_count)


def delete_template_manage(template_id):
    db.delete_template_manage(template_id)
