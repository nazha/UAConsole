# -*- coding: utf-8 -*-
from datetime import datetime
from common import admin_orm
from common.utils.types import Enum

TM_PLATFORM = Enum({
    "DARK": ('dark', u'黑金2'),
    "DUROTAR": ('durotar', u'黑金1')
})

TM_TYPE = Enum({
    "TM_A": ('template_a', u"模版二维码下"),
    "TM_B": ('template_b', u"模版二维码右下")
})


class TemplateManage(admin_orm.Model):
    __tablename__ = 'template_manage'
    id = admin_orm.Column(admin_orm.Integer, primary_key=True)
    resource = admin_orm.Column(admin_orm.VARCHAR)
    title = admin_orm.Column(admin_orm.VARCHAR)
    template_type = admin_orm.Column(admin_orm.VARCHAR)
    platform = admin_orm.Column(admin_orm.VARCHAR)
    chn_type = admin_orm.Column(admin_orm.VARCHAR)
    weight = admin_orm.Column(admin_orm.SmallInteger, default=0)
    created_at = admin_orm.Column(admin_orm.VARCHAR)
    updated_at = admin_orm.Column(admin_orm.VARCHAR)
