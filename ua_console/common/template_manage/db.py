# -*- coding: utf-8 -*-
from common.template_manage.model import TemplateManage
from datetime import datetime
from common import admin_orm as orm
from common.utils.db import get_count, get_orderby


def get_template_manage(tm_id):
    return TemplateManage.query.filter(TemplateManage.id == tm_id).first()


def insert_template_manage(resource, title, template_type, platform, chn_type, weight):
    template = TemplateManage()
    template.resource = resource
    template.title = title
    template.template_type = template_type
    template.platform = platform
    template.chn_type = chn_type
    template.weight = weight
    template.created_at = template.updated_at = datetime.utcnow()
    template.save()


def list_template_manage(platform, chn_type):
    _filter = list()
    for field in ('platform', 'chn_type'):
        if locals()[field] == 0 or locals()[field]:
            _filter.append(getattr(TemplateManage, field) == locals()[field])
    query = TemplateManage.query.filter(*_filter)
    order_by = get_orderby('-weight', TemplateManage)
    if order_by is not None:
        query = query.order_by(order_by)
    total_count = get_count(query)
    return query.all(), total_count


def delete_template_manage(tempalte_id):
    template = TemplateManage.query.with_for_update().filter(
        TemplateManage.id == tempalte_id).first()
    if template:
        template.delete()


def get_platform_template(platform, template_id=None, chn_type=None, title=None, get_count=None):
    orm.session.close()  # 待優化
    query = TemplateManage.query.filter(TemplateManage.platform == platform)
    if chn_type:
        query = query.filter(TemplateManage.chn_type == chn_type)
    if template_id:
        query = query.filter(TemplateManage.template_type == template_id)
    if title:
        query = query.filter(TemplateManage.title == title)
    if get_count is not None:
        return query.count()
    return query.all()