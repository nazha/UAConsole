# -*- coding: utf-8 -*-
import logging
import json
from common import orm
from common import agent_mg
from common.admin import db as admin_db
from common.agent.model import User
from common.chn.db import get_platform_name_by_platform_id
from common.agent import db
from common.utils import exceptions as err
from common.utils.encryption import encode_password
from common.agent.module.activated_platform_handler import ActivatedPlatformHandler
from common.agent.model import AGENT_ROLE

_LOGGER = logging.getLogger('agent')


class AgentUpdater:
    def __init__(self, agent_id, operator_id, nickname=None, password=None, bind_phone=None, bind_ip=None,
                 bind_alipay=None, bind_union=None, role=None, deactivated_reason=None, commission_rate=None,
                 disable_create_agent=None, disable_apply_agent=None, block_phone=None,
                 qq_contact_account=None, wechat_contact_account=None, activate_chn_type=None,
                 on_promote_chn_type=None, limit_commission_rate=None):
        self.invalid_input_list = (None, '', {}, [])
        self.agent_id = agent_id
        self.agent_stats = agent_mg.agent_stats.find_one({"_id": agent_id}) or {}
        assert self.agent_stats
        self.agent_role = db.agent_role_mapper(self.agent_stats['agent_level'])
        self.operator = admin_db.get_user(user_id=operator_id, to_dict=True)
        self.operator_name = '-'.join([u'控制台用户', self.operator.get('nickname', '')])
        self.nickname = nickname
        self.password = encode_password(password) if password not in self.invalid_input_list else password
        self.bind_phone = bind_phone
        self.bind_ip = bind_ip
        self.bind_alipay = bind_alipay
        self.bind_union = bind_union
        self.role = role
        self.deactivated_reason = deactivated_reason
        self.commission_rate = commission_rate
        self.disable_create_agent = disable_create_agent
        self.disable_apply_agent = disable_apply_agent
        self.qq_contact_account = qq_contact_account
        self.wechat_contact_account = wechat_contact_account
        self.block_phone = block_phone
        self.activate_chn_type = activate_chn_type
        self.on_promote_chn_type = on_promote_chn_type
        self.limit_commission_rate = limit_commission_rate
        self.agent_to_update_dct = dict()
        self.agent_stats_to_update = dict()

    def update_general(self):
        for field in ('password', 'bind_phone', 'bind_ip', 'bind_alipay', 'bind_union', 'role'):
            value_to_set = getattr(self, field)
            if value_to_set in (None, '', {}, []):
                continue
            if field in ('bind_alipay', 'bind_union'):
                bind_count = value_to_set.get('bind_count', 0)
                value_to_set['bind_count'] = bind_count + 1
                value_to_set = json.dumps(value_to_set, ensure_ascii=False)
            self.agent_to_update_dct[field] = value_to_set

    def update_nickname(self):
        if self.nickname:
            db.check_exists_nickname(self.nickname, self.agent_id)
            self.agent_to_update_dct['nickname'] = self.nickname
            self.agent_stats_to_update['nickname'] = self.nickname

    def update_user_commission_rate(self):
        if self.commission_rate or self.commission_rate == 0:
            self._validate_commission_rate()
            self.agent_to_update_dct['commission_rate'] = self.commission_rate
            self.agent_stats_to_update['commission_rate'] = self.commission_rate

    def _validate_commission_rate(self):
        if not isinstance(self.commission_rate, int):
            raise err.ParamError(u'参数错误')
        agent = db.get_agent(self.agent_id)
        if agent.agent_level != 0:
            parent = db.get_agent(agent.parent_id)
            parent_commission_rate = parent.commission_rate
        else:
            parent_commission_rate = 100
        direct_children_max_commission_rate = db.get_direct_children_max_commission_rate(self.agent_id)
        if not parent_commission_rate >= self.commission_rate >= direct_children_max_commission_rate:
            raise err.ParamError(u'分成比例须介于 %s~%s 之间' %
                                 (direct_children_max_commission_rate, parent_commission_rate))

    def update_user_activated_platform(self):
        if self.activate_chn_type:
            ActivatedPlatformHandler.update_activate_platform(self.agent_id, self.activate_chn_type)

    def update_limit_commission_rate(self):
        if self.limit_commission_rate is not None:
            assert self.agent_role == AGENT_ROLE.TEAM_LEADER
            self.agent_stats_to_update['limit_commission_rate'] = self.limit_commission_rate

    def update_disable_create_agent(self):
        if self.disable_create_agent not in (None, '', {}, []):
            assert self.agent_role in (AGENT_ROLE.BOSS, AGENT_ROLE.TEAM_LEADER, AGENT_ROLE.CUSTOM_SERVICE)
            if self.disable_create_agent not in (0, 1):
                raise err.ParamError(u'参数错误')
            self.agent_stats_to_update['disable_create_agent'] = self.disable_create_agent

    def update_block_phone(self):
        if self.block_phone not in (None, '', {}, []):
            assert self.agent_role == AGENT_ROLE.BOSS
            if self.block_phone not in (0, 1,):
                raise err.ParamError(u'参数错误')
            self.agent_stats_to_update['block_phone'] = self.block_phone

    def update_disable_apply_agent(self):
        if self.disable_apply_agent not in (None, '', {}, []):
            assert self.agent_role == AGENT_ROLE.CUSTOM_SERVICE
            if self.disable_apply_agent not in (0, 1):
                raise err.ParamError(u'参数错误')
            self.agent_stats_to_update['disable_apply_agent'] = self.disable_apply_agent

    def update_contact_info(self):
        if self.qq_contact_account is not None or self.wechat_contact_account is not None:
            assert self.agent_role == AGENT_ROLE.CUSTOM_SERVICE
            for key, contact_info in {'qq_account': self.qq_contact_account,
                                      'wechat_account': self.wechat_contact_account}.iteritems():
                if contact_info is not None:
                    self.agent_stats_to_update.setdefault('contact_info', dict())
                    self.agent_stats_to_update['contact_info'][key] = contact_info

    def update_on_promote_chn_type(self):
        if self.on_promote_chn_type:
            assert self.agent_role in (AGENT_ROLE.CUSTOM_SERVICE, AGENT_ROLE.REGULAR)
            on_promote_platform_name = get_platform_name_by_platform_id(self.on_promote_chn_type['platform_id'])
            assert on_promote_platform_name in self.activate_chn_type
            assert self.on_promote_chn_type['chn_type'] in self.activate_chn_type[on_promote_platform_name]
            assert len(self.on_promote_chn_type.keys()) == 2
            self.agent_stats_to_update['marketing_setup.on_promote_chn_type'] = self.on_promote_chn_type

    def pre_process_update(self):
        self.update_block_phone()
        self.update_disable_create_agent()
        self.update_disable_apply_agent()
        self.update_user_commission_rate()
        self.update_limit_commission_rate()
        self.update_on_promote_chn_type()
        self.update_contact_info()
        self.update_general()
        self.update_nickname()

    def update_agent_db(self):
        user = User.query.with_for_update().filter(User.id == self.agent_id).first()
        if not user:
            raise err.ParamError(u'用户不存在')
        if self.agent_to_update_dct.get('role') is not None and user.role != self.agent_to_update_dct['role']:
            db.create_deactivate_record(user.id, self.agent_to_update_dct['role'], self.operator_name,
                                        self.deactivated_reason)
        for k, v in self.agent_to_update_dct.iteritems():
            setattr(user, k, v)
        user.save(auto_commit=False)

    def update_agent_stats(self):
        if self.agent_stats_to_update:
            agent_mg.agent_stats.update_one({'_id': self.agent_id},
                                            {'$set': self.agent_stats_to_update})

    @classmethod
    def update_agent(cls, agent_id, operator_id, nickname=None, password=None, bind_phone=None, bind_ip=None,
                     bind_alipay=None, bind_union=None, role=None, deactivated_reason=None, commission_rate=None,
                     disable_create_agent=None, disable_apply_agent=None, block_phone=None, qq_contact_account=None,
                     wechat_contact_account=None, activate_chn_type=None, on_promote_chn_type=None,
                     limit_commission_rate=None):
        agent_updater = cls(agent_id, operator_id, nickname=nickname, password=password, bind_phone=bind_phone,
                            bind_ip=bind_ip, bind_alipay=bind_alipay, bind_union=bind_union, role=role,
                            deactivated_reason=deactivated_reason, commission_rate=commission_rate,
                            disable_create_agent=disable_create_agent, disable_apply_agent=disable_apply_agent,
                            block_phone=block_phone, qq_contact_account=qq_contact_account,
                            wechat_contact_account=wechat_contact_account, activate_chn_type=activate_chn_type,
                            on_promote_chn_type=on_promote_chn_type, limit_commission_rate=limit_commission_rate)
        agent_updater.pre_process_update()
        agent_updater.update_agent_db()
        try:
            agent_updater.update_user_activated_platform()
            agent_updater.update_agent_stats()
        except Exception as e:
            _LOGGER.exception('update agent fail: %s', e)
            raise err.ParamError(u'{}'.format(e))
        else:
            orm.session.commit()
