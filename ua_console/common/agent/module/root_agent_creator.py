# -*- coding: utf-8 -*-
import json
import logging
from datetime import datetime
from common import orm
from common.agent import db
from common.agent.model import User, ROLE
from common.chn.db import get_platform_chn
from common.agent.db import agent_role_mapper
from common.agent.model import AGENT_ROLE, MIN_AGENT_LEVEL
from common.chn.model import BIZ_MODEL_TYPE
from common import agent_mg

_LOGGER = logging.getLogger('agent')


class RootAgentCreator:
    """
    总推生成方法
    """
    def __init__(self, username, password, nickname, biz_model, commission_rate,
                 activated_platform, block_phone=None, disable_create_agent=None):
        """
        :param parent_id: 父節點代理ID
        :param username:  用戶名
        :param password:  用戶密碼
        :param nickname: 用戶暱稱
        :param commission_rate: 分成比例
        :param activated_platform: 開放下級平台權限，格式：{'platform_id_1': ['chn_type_1', 'chn_type_2', '...']}，僅於開
        總推、組長、客服時填入
        """
        self.parent_id = 0
        self.username = username
        self.password = password
        self.nickname = nickname
        self.commission_rate = commission_rate
        self.biz_model = biz_model
        self.agent_level = MIN_AGENT_LEVEL
        self.agent_role = agent_role_mapper(self.agent_level)
        self.agent = None           # 待生成代理的生成後信息
        self.activated_platform = activated_platform
        self.block_phone = block_phone
        self.disable_create_agent = disable_create_agent if disable_create_agent is not None else 0

    @staticmethod
    def get_create_root_preset():
        return {'biz_model': BIZ_MODEL_TYPE.to_dict(), 'platform_conf': get_platform_chn()}

    def validate_activate_platform(self):
        """數據邏輯驗證"""
        if self.agent_role in (AGENT_ROLE.BOSS, AGENT_ROLE.TEAM_LEADER, AGENT_ROLE.CUSTOM_SERVICE):
            assert self.activated_platform
        else:
            assert self.activated_platform is None
        platform_conf = get_platform_chn()
        platform_conf = {v['name']: {'chn_types': v['chn_types'], 'id': k} for k, v in platform_conf.iteritems()}
        for platform_name, chn_type_list in self.activated_platform.iteritems():
            assert platform_name in platform_conf
            assert isinstance(chn_type_list, list)
            valid_chn_type_name = [chn_type for chn_type in platform_conf[platform_name]['chn_types']]
            for chn_type in chn_type_list:
                assert chn_type in valid_chn_type_name

    def get_marketing_setup(self):
        platform_conf = get_platform_chn()
        activated_platform = list()
        for platform_id, platform_item in platform_conf.iteritems():
            if platform_item['name'] in self.activated_platform:
                if not self.activated_platform[platform_item['name']]:
                    continue
                available_chn_types = list()
                for chn_type, chn_type_item in platform_item['chn_types'].iteritems():
                    if chn_type in self.activated_platform[platform_item['name']]:
                        available_chn_types.append(dict(
                            name=chn_type_item['name'],
                            chn_type_name=chn_type_item['chn_type_name'],
                            resource=list()
                        ))
                activated_platform.append(dict(
                    platform_id=platform_id,
                    platform_name=platform_item['name'],
                    platform_label=platform_item['label'],
                    chn_types=available_chn_types
                ))
        marketing_setup = dict(
            activated_platform=activated_platform
        )
        return marketing_setup

    def _create_agent(self, auto_commit=False):
        user = User()
        user.parent_id = self.parent_id
        user.username = self.username
        user.password = db.encode_password(self.password)
        user.nickname = self.nickname
        user.agent_level = self.agent_level
        user.commission_rate = self.commission_rate
        user.role = ROLE.USER
        user.extend = json.dumps({}, ensure_ascii=False)
        user.created_at = user.updated_at = user.last_login = datetime.utcnow()
        user.save(auto_commit=auto_commit)
        orm.session.flush()
        self.agent = user.as_dict()

    def _create_agent_stats(self):
        assert self.agent['id']
        post_dict = dict(_id=self.agent['id'],
                         username=self.username,
                         nickname=self.nickname,
                         parent_id=self.parent_id,
                         agent_level=self.agent_level,
                         biz_model=self.biz_model,
                         commission_rate=self.commission_rate,
                         disable_create_agent=self.disable_create_agent,
                         path=[self.agent['id']],
                         marketing_setup=self.get_marketing_setup(),
                         block_phone=self.block_phone,)
        agent_mg.agent_stats.insert_one(post_dict)

    def start_create_agent(self):
        self._create_agent()
        self._create_agent_stats()
        orm.session.commit()

    def validate_create_agent_params(self):
        self.validate_activate_platform()
        db.check_agent_level(self.agent_level)
        db.check_biz_model(self.biz_model)
        db.check_exists_user(self.username)
        db.check_exists_nickname(self.nickname)

    def create_root_agent(self):
        """
        创建子代理
        1. 參數、數據邏輯驗證
        2. 匹配渠道資源、更新渠道資源狀態至待生成狀態
        3. 代理創建(mysql, mongodb)，若失敗則釋放待生成渠道資源至可用狀態
        4. 更新渠道資源狀態至綁定狀態
        """
        self.validate_create_agent_params()
        self.start_create_agent()
