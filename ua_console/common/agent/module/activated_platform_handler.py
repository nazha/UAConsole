# -*- coding: utf-8 -*-
import logging
import copy
from common import agent_mg
from common.utils import exceptions as err
from common.chn.db import get_platform_chn, transform_platform_chn
from common.agent.db import agent_role_mapper
from common.agent.model import AGENT_ROLE

_LOGGER = logging.getLogger('agent')


class ActivatedPlatformHandler:
    def __init__(self, agent_id):
        self.agent_id = agent_id
        self.agent_stats = agent_mg.agent_stats.find_one({'_id': agent_id})
        assert self.agent_stats

    @staticmethod
    def validate_activated_platform(agent_id, activated_platform):
        """
        # 1. 檢查platform_name、chn_type是否存在
        # 2. 檢查分配給與總推的資源是否收回，
        """
        agent = agent_mg.agent_stats.find_one({'_id': agent_id})
        if not agent:
            raise err.ParamError(u'无此代理')
        previous_activated_platform = agent.get('marketing_setup', {}).get('activated_platform', [])
        for platform_item in previous_activated_platform:
            platform_chn_type_list = activated_platform.get(platform_item['platform_name'], [])
            for chn_type_item in platform_item['chn_types']:
                if chn_type_item['name'] not in platform_chn_type_list:
                    raise err.ParamError(u'分配渠道后不可收回 {}'.format(chn_type_item['name']))
        if agent_role_mapper(agent['agent_level']) != AGENT_ROLE.BOSS:
            parent = agent_mg.agent_stats.find_one({'_id': agent['parent_id']})
            parent_activate_chn_type = transform_platform_chn(parent['marketing_setup']['activated_platform'],
                                                              platform_name_as_key=True)
            for platform_name, platform_item in activated_platform.iteritems():
                if platform_name not in parent_activate_chn_type:
                    raise err.ParamError(u'权限大于上级')
                for chn_name in platform_item:
                    if chn_name not in parent_activate_chn_type[platform_name]['chn_types']:
                        raise err.ParamError(u'权限大于上级')

    @staticmethod
    def get_platform_item_template(platform_id, platform_name, platform_label, chn_types):
        return dict(platform_id=platform_id,
                    platform_name=platform_name,
                    platform_label=platform_label,
                    chn_types=chn_types)

    @staticmethod
    def get_chn_item_template(chn_type, chn_type_name):
        return dict(name=chn_type, chn_type_name=chn_type_name, resource=list(), activated=False)

    @classmethod
    def transform_activated_platform(cls, activated_platform):
        platform_conf = get_platform_chn()
        transformed_activated_platform = list()
        for platform_id, platform_item in platform_conf.iteritems():
            if platform_item['name'] in activated_platform:
                if not activated_platform[platform_item['name']]:
                    continue
                available_chn_types = list()
                for chn_type, chn_type_item in platform_item['chn_types'].iteritems():
                    if chn_type in activated_platform[platform_item['name']]:
                        available_chn_types.append(cls.get_chn_item_template(
                            chn_type=chn_type_item['name'],
                            chn_type_name=chn_type_item['chn_type_name'],
                        ))
                transformed_activated_platform.append(cls.get_platform_item_template(
                    platform_id=platform_id,
                    platform_name=platform_item['name'],
                    platform_label=platform_item['label'],
                    chn_types=available_chn_types
                ))
        return transformed_activated_platform

    def adjust_activated_platform(self, agent_id, activated_platform):
        transformed_activated_platform = self.transform_activated_platform(activated_platform)
        agent = agent_mg.agent_stats.find_one({'_id': agent_id})
        previous_activated_platform = agent['marketing_setup']['activated_platform']
        for platform_item in transformed_activated_platform:
            previous_activated_platform_copy = copy.deepcopy(previous_activated_platform)
            for idx_1, previous_platform_item in enumerate(previous_activated_platform):
                if previous_platform_item['platform_name'] != platform_item['platform_name']:
                    if idx_1 + 1 == len(previous_activated_platform_copy):
                        previous_activated_platform.append(platform_item)
                else:
                    for chn_item in platform_item['chn_types']:
                        previous_chn_types_copy = copy.deepcopy(previous_platform_item['chn_types'])
                        for idx_2, previous_chn_item in enumerate(previous_platform_item['chn_types']):
                            if previous_chn_item['name'] != chn_item['name']:
                                if idx_2 + 1 == len(previous_chn_types_copy):
                                    previous_platform_item['chn_types'].append(chn_item)
                            else:
                                break
                    break
        if not previous_activated_platform:
            previous_activated_platform = transformed_activated_platform
        agent_mg.agent_stats.update_one({'_id': agent_id},
                                        {'$set': {'marketing_setup.activated_platform': previous_activated_platform}},
                                        upsert=True)

    @classmethod
    def update_activate_platform(cls, agent_id, activated_platform):
        handler = cls(agent_id)
        handler.validate_activated_platform(agent_id, activated_platform)
        agent_stats = agent_mg.agent_stats.find_one({'_id': agent_id})
        assert agent_stats
        agent_role = agent_role_mapper(agent_stats['agent_level'])
        assert agent_role in (AGENT_ROLE.BOSS, AGENT_ROLE.TEAM_LEADER, AGENT_ROLE.CUSTOM_SERVICE)
        if agent_role in (AGENT_ROLE.BOSS, AGENT_ROLE.TEAM_LEADER):
            handler.adjust_activated_platform(agent_id, activated_platform)
        elif agent_role == AGENT_ROLE.CUSTOM_SERVICE:
            agents = agent_mg.agent_stats.find({'path': agent_id})
            for agent in agents:
                handler.adjust_activated_platform(agent['_id'], activated_platform)

