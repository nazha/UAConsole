# -*- coding: utf-8 -*-
import copy
import requests
import logging
import json
from django.conf import settings
from sqlalchemy import func
from datetime import timedelta
from common.utils import tz
from common import agent_mg
from common.agent import db
from common.agent.module.root_agent_creator import RootAgentCreator
from common.chn.module.resource_allocator import ChannelResourceAllocator
from common.admin.model import ROLE
from common.admin import db as admin_db
from common.agent.model import User, AGENT_ROLE
from common.agent.module.agent_updater import AgentUpdater
from common.chn.db import get_platform_chn
from common.utils import exceptions as err
from common.utils.mg import paginator
from common.utils.export import redirect_to_file, gen_filename
from common.utils.encryption import generate_sign
from common.utils.currency import convert_fen_to_yuan

_LOGGER = logging.getLogger('agent')
logging.basicConfig(level=logging.INFO)
logging.getLogger('requests').setLevel(logging.ERROR)


def _format_agent_item(admin_id, item, block_phone=None, block_financial_account=None):
    admin_user = admin_db.get_user(user_id=admin_id, to_dict=True)
    if admin_user['role'] <= ROLE.SENIOR_USER:
        if block_phone:
            item['bind_phone'] = u'***********' if item['bind_phone'] else item['bind_phone']
        if block_financial_account:
            bind_alipay = json.loads(item.get('bind_alipay') or '{}')
            bind_union = json.loads(item.get('bind_union') or '{}')
            if bind_alipay.get("alipay_no"):
                item['bind_alipay'] = json.dumps({"bind_count": 1, "alipay_no": "*****", "name": "***"})
            if bind_union.get("bank_no"):
                item['bind_union'] = json.dumps({"bank_name": "****", "name": "***", "bind_count": 1,
                                                 "region": "***/***", "branch": "***", "bank_no": "**************"})
    item['balance'] = convert_fen_to_yuan(item['balance'])
    item['total_reward'] = convert_fen_to_yuan(item['total_reward'])
    item['updated_at'] = tz.utc_to_local_str(item['updated_at'])
    item['created_at'] = tz.utc_to_local_str(item['created_at'])
    item['chn'] = item.get('channels', [])
    item.pop('password', None)
    return item


def _export_agent_list(admin_id, user_dct):
    filename = gen_filename('agent_list')
    header = ('id', 'created_at', 'parent_id', 'username', 'nickname', 'agent_level', 'bind_phone', 'path',
              'chn_name', 'post_link', 'qr_link', 'balance', 'total_reward', 'updated_at',)
    cn_header = ('ID', u'获取时间', u'上级代理ID', u'代理账户', u'代理昵称', u'代理层级', u'绑定手机号', u'上级推广链',
                 u'渠道号', u'推广海报', u'推广二维码', u'余额', u'账户总额', u'账户更新时间')
    export_items = []
    for user_id, item in user_dct.iteritems():
        item = _format_agent_item(admin_id, item)
        export_data = []
        for x in header:
            if x == 'chn_name':
                channels = [chn_item['chn'] for chn_item in item['chn']]
                export_data.append(','.join(channels))
            elif x == 'path':
                export_data.append(item['stats']['path'])
            elif x == 'post_link':
                posts = [chn_item['post_links'] for chn_item in item['chn'] if 'post_link' in chn_item]
                export_data.append(posts)
            elif x == 'qr_link':
                qrs = [chn_item['qr_link'] for chn_item in item['chn'] if 'qr_link' in chn_item]
                export_data.append(','.join(qrs))
            else:
                export_data.append(item.get(x, '-'))
        export_items.append(export_data)
    return redirect_to_file(export_items, cn_header, filename)


def list_agent(admin_id, agent_id, parent_id, nickname, agent_level, role, bind_phone, root_id, channel,
               bind_union, bind_alipay, bankcard_no, alipay_no, register_start_time, register_end_time,
               to_export_file, page, size):
    user_pool = None
    if root_id:
        root_user = db.get_agent_stats(root_id)
        if not root_user or db.agent_role_mapper(root_user['agent_level']) != AGENT_ROLE.BOSS:
            raise err.ParamError(u'{}非总推号'.format(root_id))
        children_user_id_list = db.get_children_id(root_id) + [root_id]
        user_pool = children_user_id_list
    if channel:
        chn = agent_mg.chn_pool.find_one({'chn': channel})
        user_pool = set.intersection(set(user_pool), set(str(chn['agent_id']))) if user_pool else [str(chn['agent_id'])]
    if agent_id:
        user_pool = set.intersection(set(user_pool), set(agent_id)) if user_pool else [agent_id]
    disable_paginate = True if to_export_file else False
    items, total_count = db.list_agent(user_pool, parent_id, nickname, agent_level, role, bind_phone, bind_union,
                                       bind_alipay, bankcard_no, alipay_no, register_start_time, register_end_time,
                                       page, size, disable_paginate=disable_paginate)
    user_dct = {user.id: user.as_dict() for user in items}
    chn_items = agent_mg.chn_pool.find({'agent_id': {'$in': user_dct.keys()}})
    for chn_item in chn_items:
        user_dct[chn_item['agent_id']].setdefault('channels', [])
        user_dct[chn_item['agent_id']]['channels'].append(chn_item)
    agent_stats_items = agent_mg.agent_stats.find({'_id': {'$in': user_dct.keys()}})
    for agent_stats_item in agent_stats_items:
        user_dct[agent_stats_item['_id']]['stats'] = agent_stats_item
        user_dct[agent_stats_item['_id']]['root_id'] = agent_stats_item['path'][0]
    if to_export_file:
        return _export_agent_list(admin_id, user_dct)
    resp_items = []
    for user_id, item in user_dct.iteritems():
        resp_items.append(_format_agent_item(admin_id, item, block_phone=True, block_financial_account=True))
    return dict(list=resp_items, page=page, size=len(resp_items), total_count=total_count)


def get_agent(admin_id, user_id, username, nickname):
    user, user_list = {}, []
    if user_id:
        user_list.append(db.get_agent(user_id, to_dict=True))
    if username:
        user_list.append(db.get_user_by_username(username=username))
    if nickname:
        user_list.append(db.get_user_by_nickname(nickname=nickname))
    for _user in user_list:
        if not _user:
            continue
        if user and _user and user['id'] != _user['id']:
            raise err.ParamError(u'用户不存在')
        user = _user
    if not user:
        raise err.ParamError(u'用户不存在')
    agent_accu_stats = db.get_agent_accu_stats(user['id'])
    user.update(agent_accu_stats)
    agent_stats = db.get_agent_stats(user['id'])
    user.update(agent_stats)
    admin_user = admin_db.get_user(user_id=admin_id, to_dict=True)
    user = _format_agent_item(admin_user['id'], user, block_phone=True, block_financial_account=True)
    return user


def update_agent(agent_id, operator_id, nickname=None, password=None, bind_phone=None, bind_ip=None,
                 bind_alipay=None, bind_union=None, role=None, deactivated_reason=None, commission_rate=None,
                 disable_create_agent=None, disable_apply_agent=None, block_phone=None, qq_contact_account=None,
                 wechat_contact_account=None, activate_chn_type=None, on_promote_chn_type=None,
                 limit_commission_rate=None):
    AgentUpdater.update_agent(agent_id, operator_id, nickname=nickname, password=password, bind_phone=bind_phone,
                              bind_ip=bind_ip, bind_alipay=bind_alipay, bind_union=bind_union, role=role,
                              deactivated_reason=deactivated_reason, commission_rate=commission_rate,
                              disable_create_agent=disable_create_agent, disable_apply_agent=disable_apply_agent,
                              block_phone=block_phone, qq_contact_account=qq_contact_account,
                              wechat_contact_account=wechat_contact_account,
                              activate_chn_type=activate_chn_type, on_promote_chn_type=on_promote_chn_type,
                              limit_commission_rate=limit_commission_rate)


def create_root_agent(username, password, nickname, commission_rate,
                      biz_model=None, activated_platform=None, block_phone=None, disable_create_agent=None):
    """
    创建总推
    """
    root_agent_creator = RootAgentCreator(username=username,
                                          password=password,
                                          nickname=nickname,
                                          biz_model=biz_model,
                                          commission_rate=commission_rate,
                                          activated_platform=activated_platform,
                                          block_phone=block_phone,
                                          disable_create_agent=disable_create_agent)
    root_agent_creator.create_root_agent()
    return root_agent_creator.agent


def get_login_record(user_id, username, ip, status, order_by, page, size):
    if user_id:
        user = db.get_agent(user_id, to_dict=True)
        username = user['username']
    login_record, total_count = db.get_login_record(username, ip, status, order_by, page, size)
    username_list = [record.username.lower() for record in login_record]
    users = User.query.filter(func.lower(User.username).in_(username_list)).all()
    user_id_mapper = {user.username: user.id for user in users}
    resp_items = []
    for record in login_record:
        record_dct = record.as_dict()
        record_dct['user_id'] = user_id_mapper.get(record.username.lower(), '-')
        record_dct['created_at'] = str(record_dct['created_at'] + timedelta(hours=8))
        resp_items.append(record_dct)
    return dict(list=resp_items, page=page, size=len(resp_items), total_count=total_count)


def get_deactivated_user_list(agent_id, root_id, parent_id, platform_id, chn_type, page, size):
    deactivated_users = db.list_deactivated_users()
    condition = dict(_id={'$in': deactivated_users})
    if agent_id:
        if agent_id in deactivated_users:
            condition['_id'] = agent_id
        else:
            condition['_id'] = None
    if root_id:
        root_user_ids = [root_user['id'] for root_user in db.get_root_user()]
        root_id = None if root_id not in root_user_ids else root_id
        condition['path'] = root_id
    for field in ('parent_id', 'platform_id', 'chn_type'):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    offset, size = paginator(dict(page=page, size=size))
    deactivated_user_stats = agent_mg.agent_stats.find(condition).sort('agent_level', -1).skip(offset).limit(size)
    total_count = agent_mg.agent_stats.count(condition)
    platform_conf = get_platform_chn()
    resp_items = []
    for user_stats in deactivated_user_stats:
        platform_id, chn_type = user_stats.get('platform_id'), user_stats.get('chn_type')
        user = db.get_agent([user_stats['_id']], to_dict=True)
        first_deactivated_time = db.get_first_deactivated_time(user_stats['_id'])
        item = dict(
            agent_id=user_stats['_id'],
            role=user.get('role'),
            root_id=user_stats['path'][0],
            agent_level=user_stats['agent_level'],
            parent_id=user_stats['parent_id'],
            platform_name=platform_conf.get(platform_id, {}).get('label', '-'),
            chn_name=platform_conf.get(platform_id, {}).get('chn_types', {}).get(
                chn_type, {}).get('chn_type_name', '-'),
            created_at=tz.utc_to_local_str(user['created_at']),
            first_deactivated=tz.utc_to_local_str(first_deactivated_time) if first_deactivated_time else '-'
        )
        resp_items.append(item)
    return dict(list=resp_items, total_count=total_count)


def get_deactivate_history_list(agent_id, root_id, parent_id, platform_id, chn_type, start_time,
                                end_time, page, size):
    deactivated_users = db.list_deactivated_users()
    condition = dict(agent_id={'$in': deactivated_users}, role=0, updated_at={'$gt': '2018-01-18'})
    if agent_id:
        condition['agent_id'] = agent_id if agent_id in deactivated_users else None
    if root_id:
        condition['path'] = root_id
    for field in ('parent_id', 'platform_id', 'chn_type'):
        if locals()[field] == 0 or locals()[field]:
            condition[field] = locals()[field]
    if start_time:
        condition['day'] = {'$gte': start_time}
    if end_time:
        if 'day' in condition:
            condition['day']['$lt'] = end_time
        else:
            condition['day'] = {'$lt': end_time}
    offset, size = paginator(dict(page=page, size=size))
    deactivate_stats = agent_mg.daily_agent_stats.find(condition).sort('agent_level', -1).skip(offset).limit(size)
    total_count = agent_mg.daily_agent_stats.count(condition)
    platform_conf = get_platform_chn()
    resp_items = []
    for each_stats in deactivate_stats:
        agent_stats = db.get_agent_stats(agent_id=each_stats['agent_id'])
        platform_id = agent_stats.get('platform_id')
        chn_type = agent_stats.get('chn_type')
        item = dict(
            day=each_stats['day'],
            agent_id=each_stats['agent_id'],
            root_id=each_stats['path'][0],
            parent_id=each_stats['parent_id'],
            platform_name=platform_conf.get(platform_id, {}).get('label', '-'),
            chn_name=platform_conf.get(platform_id, {}).get('chn_types', {}).get(
                chn_type, {}).get('chn_type_name', '-'),
            total_reward=each_stats.get('total_reward', 0),
            total_tax=each_stats.get('total_tax', 0),
            total_recharge=each_stats.get('extend', {}).get('total', {}).get('recharge', 0),
            total_withdraw=each_stats.get('extend', {}).get('total', {}).get('withdraw', 0),
            total_active=each_stats.get('extend', {}).get('total', {}).get('active', 0),
            total_register=each_stats.get('extend', {}).get('total', {}).get('register', 0),
        )
        resp_items.append(item)
    return dict(list=resp_items, total_count=total_count)


def get_deactivate_record_list(agent_id):
    records = db.list_deactivate_record(agent_id)
    resp_items = []
    for record in records:
        item = record.as_dict()
        item['created_at'] = tz.utc_to_local_str(item['created_at'])
        item['updated_at'] = tz.utc_to_local_str(item['updated_at'])
        resp_items.append(item)
    return resp_items


def list_root_agent(page, size):
    roots, total_count = db.list_agent(agent_level=0, page=page, size=size)
    resp_items = list()
    for root in roots:
        root = root.as_dict()
        resp_items.append(dict(
            id=root['id'],
            username=root['username'],
            nickname=root['nickname'],
            commission_rate=root['commission_rate'],
            created_at=tz.utc_to_local_str(root['created_at']),
            updated_at=tz.utc_to_local_str(root['updated_at']),
        ))
    return dict(list=resp_items, page=page, size=len(resp_items), total_count=total_count)


def list_team_leader(team_leader_id_filter, root_id_filter, platform_id_filter, chn_type_filter, page, size):
    condition = dict(agent_level=1)
    if team_leader_id_filter:
        condition['_id'] = team_leader_id_filter
    if root_id_filter:
        root_user = db.get_agent_stats(root_id_filter)
        if db.agent_role_mapper(root_user['agent_level']) != AGENT_ROLE.BOSS:
            raise err.ParamError(u'{}非总推号'.format(root_id_filter))
        condition['path'] = root_id_filter
    team_leader_stats = agent_mg.agent_stats.find(condition)
    offset, size = paginator(dict(page=page, size=size))
    resp_items = []
    for each_stats in team_leader_stats:
        activated_platform = each_stats['marketing_setup']['activated_platform']
        platform = {platform['platform_id']: platform['platform_label']
                    for platform in activated_platform}
        chn = {chn_type['name']: chn_type['chn_type_name']
               for platform in activated_platform
               for chn_type in platform['chn_types']}
        if platform_id_filter and platform_id_filter not in platform.keys():
            continue
        if chn_type_filter and chn_type_filter not in chn.keys():
            continue
        item = dict(
            team_leader_id=each_stats['_id'],
            username=each_stats['username'],
            nickname=each_stats['nickname'],
            root_id=each_stats['path'][0],
            platform_label=','.join([unicode(key) for key in platform.values()]),
            activated_platform=activated_platform,
            chn_type_name=','.join([unicode(key) for key in chn.values()]),
            commision_rate=each_stats['commission_rate'],
            limit_commission_rate=each_stats.get('limit_commission_rate', {}),
            disable_create_agent=each_stats.get('disable_create_agent', 0)
        )
        resp_items.append(item)
    return dict(list=resp_items[offset:offset+size+1], total_count=len(resp_items))


def _get_custom_service_stats_from_maestro(agent_id_list):
    maestro_api_key = settings.MAESTRO_API_KEY
    params = {"agent_id_list": json.dumps(agent_id_list), "key": 'agent'}
    sign = generate_sign(params, maestro_api_key)
    params['sign'] = sign
    api_url = settings.MAESTRO_GET_CUSTOM_STATS_API
    res = requests.get(api_url, params=params, timeout=10)
    res_json = json.loads(res.content)
    return res_json['data']


def list_custom_service(platform_id, chn_type, agent_id, parent_id, root_id, disable_create_agent,
                        disable_apply_agent, page, size):
    platform_conf = get_platform_chn()
    condition = dict(agent_level=2)
    resource_type_condition = dict()
    agent_pool = list()
    if resource_type_condition:
        resource_type_condition['status'] = 2
        chn_resource = agent_mg.chn_pool.find(resource_type_condition)
        agent_pool = [resource['agent_id'] for resource in chn_resource]
        agent_pool = agent_pool if not agent_id else list(set(agent_pool) & set(agent_id))
    if agent_id:
        agent_pool = [agent_id] if not agent_pool else list(set(agent_pool) & set(agent_id))
    if agent_pool:
        condition['_id'] = {'$in': agent_pool}
    if platform_id:
        condition['marketing_setup.on_promote_chn_type.platform_id'] = platform_id
    if chn_type:
        condition['marketing_setup.on_promote_chn_type.chn_type'] = chn_type
    for field in ('disable_create_agent', 'disable_apply_agent'):
        if locals()[field]:
            condition[field] = locals()[field]
        elif locals()[field] == 0:
            condition[field] = {'$ne': 1}
    if parent_id:
        condition['parent_id'] = parent_id
    if root_id:
        root_user = db.get_agent_stats(root_id)
        if not root_user or db.agent_role_mapper(root_user['agent_level']) != AGENT_ROLE.BOSS:
            raise err.ParamError(u'{}非总推号'.format(root_id))
        condition['path'] = root_id
    custom_service_agents = agent_mg.agent_stats.find(condition)
    agent_id_list = [agent['_id'] for agent in copy.deepcopy(custom_service_agents)]
    total_count = agent_mg.agent_stats.count(condition)
    apply_stats = _get_custom_service_stats_from_maestro(agent_id_list)
    resp_items = list()
    for agent in custom_service_agents:
        on_promote_chn_type = agent['marketing_setup']['on_promote_chn_type']
        agent_apply_stats = apply_stats.get(str(agent['_id']), {})
        item = dict(
            disable_create_agent=agent.get('disable_create_agent', 0),
            disable_apply_agent=agent.get('disable_apply_agent', 0),
            agent_id=agent['_id'],
            parent_id=agent['parent_id'],
            root_id=agent['path'][0],
            nickname=agent['nickname'],
            commission_rate=agent['commission_rate'],
            marketing_setup=agent['marketing_setup'],
            platform_name=platform_conf.get(on_promote_chn_type['platform_id'], {}).get('label', '-'),
            chn_name=platform_conf.get(on_promote_chn_type['platform_id'], {}).get('chn_types', {}).get(
                on_promote_chn_type['chn_type'], {}).get('chn_type_name', '-'),
            entrance_ticket=agent.get('entrance_code', ''),
            apply_page_visit_count=agent_apply_stats.get('apply_page_visit_count', 0),
            apply_child_agent_count=agent_apply_stats.get('apply_child_agent_count', 0),
            apply_child_agent_approve_count=agent_apply_stats.get('apply_child_agent_approve_count', 0),
            qq_account=agent.get('contact_info', {}).get('qq_account', ''),
            wechat_account=agent.get('contact_info', {}).get('wechat_account', ''),
        )
        resp_items.append(item)
    return dict(list=resp_items, total_count=total_count)


def list_child_agent_application_form(agent_id, page, size):
    offset, size = paginator(dict(page=page, size=size))
    application_form = agent_mg.register_apply.find({'parent_id': agent_id}).sort(
        'apply_time', -1).skip(offset).limit(size)
    resp_items = []
    for form in application_form:
        form['apply_time'] = tz.utc_to_local_str(form['apply_time']).split('.')[0]
        if 'verified_time' in form:
            form['verified_time'] = tz.utc_to_local_str(form['verified_time']).split('.')[0]
        resp_items.append(form)
    platform_conf = get_platform_chn()
    agent_stats = agent_mg.agent_stats.find_one({'_id': agent_id})
    team_leader_agent_stats = agent_mg.agent_stats.find_one({'_id': agent_stats['path'][1]})
    limit_custom_commission_rate = team_leader_agent_stats.get('limit_commission_rate', {})
    on_promote_platform_id = agent_stats['marketing_setup']['on_promote_chn_type']['platform_id']
    on_promote_chn_type = agent_stats['marketing_setup']['on_promote_chn_type']['chn_type']
    platform_name = platform_conf.get(on_promote_platform_id, {}).get('name', '-')
    commission_rate_restriction = min(
        limit_custom_commission_rate.get(platform_name, {}).get(on_promote_chn_type, 18),
        agent_stats.get('commission_rate', 0))
    return dict(list=resp_items, child_max_commission_rate=commission_rate_restriction)


def verify_child_agent_application_form(agent_id, application_form_id, verification, commission_rate, note):
    user_agent_url = settings.USER_AGENT_URL
    user_agent_super_key = settings.USER_AGENT_SUPER_KEY
    verify_apply_agent_url = user_agent_url + '/agent/admin/user/apply_child_agent/verification/post/'
    header = {"X-AUTH-USER": agent_id, 'X-AUTH-TOKEN': user_agent_super_key}
    data = dict(
        application_form_id=application_form_id,
        verification=verification,
        commission_rate=commission_rate,
        note=note
    )
    res = requests.post(verify_apply_agent_url, headers=header, data=json.dumps(data), timeout=5)
    res_json = json.loads(res.content)
    if res_json['status'] != 0:
        raise err.DataError(res_json['msg'])
    return res_json['data']


def list_regular_agent(agent_id, parent_id, root_id, page, size):
    platform_conf = get_platform_chn()
    condition = dict(agent_level={"$gte": 3})
    if root_id:
        root_user = db.get_agent_stats(root_id)
        if db.agent_role_mapper(root_user['agent_level']) != AGENT_ROLE.BOSS:
            raise err.ParamError(u'{}非总推号'.format(root_id))
        condition['path'] = root_id
    if agent_id:
        condition['_id'] = agent_id
    if parent_id:
        condition['parent_id'] = parent_id
    offset, size = paginator(dict(page=page, size=size))
    regular_agents = agent_mg.agent_stats.find(condition).sort('_id', -1).skip(offset).limit(size)
    total_count = agent_mg.agent_stats.count(condition)
    user_pool = [agent['_id'] for agent in copy.deepcopy(regular_agents)]
    items, _ = db.list_agent(user_pool=user_pool, disable_paginate=True)
    user_dct = {user.id: user.as_dict() for user in items}
    resp_items = []
    for agent in regular_agents:
        activated_platform = agent['marketing_setup']['activated_platform']
        activated_chn_type = {platform['platform_label']: filter(lambda x: x.get('activated'), platform['chn_types'])
                              for platform in activated_platform}
        on_promote_chn_type = agent['marketing_setup']['on_promote_chn_type']
        resp_items.append(dict(
            agent_id=agent['_id'],
            nickname=agent['nickname'],
            role=user_dct.get(agent['_id'], {}).get('role'),
            agent_level=agent['agent_level'],
            parent_id=agent['parent_id'],
            root_id=agent['path'][0],
            activated_platform=agent['marketing_setup']['activated_platform'],
            commission_rate=agent['commission_rate'],
            platform=','.join(filter(lambda x: activated_chn_type[x], activated_chn_type.keys())),
            chn_type=','.join([x['chn_type_name'] for item in activated_chn_type.values() for x in item]),
            main_platform=platform_conf[on_promote_chn_type['platform_id']]['label'],
            main_chn_type=platform_conf[on_promote_chn_type['platform_id']]['chn_types'][
                on_promote_chn_type['chn_type']]['chn_type_name']
        ))
    return dict(list=resp_items, page=page, size=len(resp_items), total_count=total_count)


def activate_chn_resource(agent_id, platform_id, chn_type):
    ChannelResourceAllocator.activated_chn_resource(agent_id, platform_id, chn_type)


def get_parent_activated_platform(agent_id):
    agent = db.get_agent_stats(agent_id)
    parent = db.get_agent_stats(agent['parent_id'])
    if not parent:
        return dict()
    return dict([(
        int(item['platform_id']), {
            'label': item['platform_label'],
            'name': item['platform_name'],
            'chn_types': dict([
                (chn_type['name'], chn_type) for chn_type in item['chn_types']])
        }
    ) for item in parent['marketing_setup']['activated_platform']])
