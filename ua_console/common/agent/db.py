# -*- coding: utf-8 -*-
import json
from hashlib import md5
from datetime import datetime
from future.utils import raise_with_traceback
from common import orm
from common.agent.model import (
    User,
    ROLE,
    LoginRecord,
    AGENT_ROLE,
    DeactivateRecord,
    MIN_AGENT_LEVEL,
    MAX_AGENT_LEVEL
)
from common.chn.model import BIZ_MODEL_TYPE
from common.utils import exceptions as err
from common.utils import tz
from common.utils.respcode import StatusCode
from common import agent_mg
from common.utils.db import get_count, get_orderby, paginate

_SALT = u"%*eQg#GH"


def encode_password(passwd):
    return md5(passwd.encode('utf-8') + _SALT).hexdigest()


def check_exists_user(username):
    user = get_user_by_username(username=username)
    if user:
        raise err.ParamError(u'用户名已存在')
    return user


def check_exists_nickname(nickname, user_id=None):
    user = get_user_by_nickname(nickname=nickname)
    if user and user['id'] != user_id:
        raise err.ParamError(u'昵称已存在')
    return user


def check_agent_level(agent_level):
    if not isinstance(agent_level, int):
        raise err.DataError(u'代理层级异常')
    if agent_level > MAX_AGENT_LEVEL:
        raise err.DataError(u'代理层级已达到上限')
    elif agent_level < MIN_AGENT_LEVEL:
        raise err.DataError(u'代理层级异常')


def check_biz_model(biz_model):
    if biz_model is not None and biz_model not in BIZ_MODEL_TYPE.to_dict():
        raise err.DataError(u'营商模型错误')


def get_agent(user_id, to_dict=False):
    user = User.query.filter(User.id == user_id).first()
    if not to_dict:
        return user
    return user.as_dict() if user else dict()


def get_target_agents(target_agent_pool, to_dict=False):
    target_agents = User.query.filter(User.id.in_(target_agent_pool)).all()
    if not to_dict:
        return target_agents
    return [agent.as_dict() for agent in target_agents]


def get_user_by_phone(phone):
    return User.query.filter(User.bind_phone == phone).first()


def get_user_by_nickname(nickname, get_model_obj=False):
    """
    :param nickname: 用戶暱稱
    :param get_model_obj: 是否傳回model對象 True: 傳回model對象、False:傳回字典
    :return:
    """
    user = User.query.filter(User.nickname == nickname).first()
    if get_model_obj:
        return user
    return user.as_dict() if user else dict()


def get_user_by_username(username, get_model_obj=False):
    user = User.query.filter(User.username == username).first()
    if get_model_obj:
        return user
    return user.as_dict() if user else dict()


def get_username_by_id(user_id):
    user = orm.session.query(User).filter(User.id == user_id).first()
    if user:
        return user.username


def get_user_id_by_username(user_name):
    user = User.query.filter(User.username == user_name).first()
    if user:
        return user.id


def get_direct_children_max_commission_rate(user_id):
    direct_children = User.query.filter(User.parent_id == user_id).all()
    max_commission_rate = 0
    for direct_child in direct_children:
        if direct_child.commission_rate > max_commission_rate:
            max_commission_rate = direct_child.commission_rate
    return max_commission_rate


def get_children_id(user_id):
    child_agents = agent_mg.agent_stats.find({'path': user_id, '_id': {'$ne': user_id}})
    return [agent['_id'] for agent in child_agents]


def get_root_user():
    root_users = User.query.filter(User.agent_level == 0).all()
    resp = []
    for root_user in root_users:
        root_user = root_user.as_dict()
        resp.append(root_user)
    return resp


def list_agent(user_pool=None, parent_id=None, nickname=None, agent_level=None, role=None, bind_phone=None,
               bind_union=None, bind_alipay=None, bankcard_no=None, alipay_no=None, register_start_time=None,
               register_end_time=None, page=1, size=20, disable_paginate=False):
    _filter = list()
    if user_pool is not None:
        _filter.append(User.id.in_(user_pool))
    if register_start_time:
        _filter.append(User.created_at >= register_start_time)
    if register_end_time:
        _filter.append(User.created_at < register_end_time)
    for field in ('parent_id', 'agent_level', 'role', 'bind_phone', 'nickname'):
        if locals()[field] == 0 or locals()[field]:
            _filter.append(getattr(User, field) == locals()[field])
    if bind_union:
        _filter.append(User.bind_union.isnot(None))
    if bind_alipay:
        _filter.append(User.bind_alipay.isnot(None))
    if bankcard_no:
        _filter.append(User.bind_union.contains('"bank_no": "{}"'.format(bankcard_no)))
    if alipay_no:
        _filter.append(User.bind_alipay.contains('"alipay_no": "{}"'.format(alipay_no)))
    query = User.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby('-created_at', User)
    if order_by is not None:
        query = query.order_by(order_by)
    if not disable_paginate:
        query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def get_agent_accu_stats(agent_id):
    accu_agent_stats = agent_mg.daily_agent_stats.aggregate([{
        '$match': {
            'agent_id': agent_id,
            'role': {'$ne': 0}
        }}, {
        '$group': {
            '_id': None,
            'total_tax': {"$sum": '$total_tax'},
            'total_reward': {"$sum": '$total_reward'},
            'total_active_count': {"$sum": '$extend.total.active_count'},
            'total_recharge': {"$sum": '$extend.total.total_recharge'},
            'user_tax': {'$sum': '$user_tax'},
            'user_reward': {'$sum': '$user_reward'},
            'sub_tax': {'$sum': '$sub_tax'},
            'sub_reward': {'$sum': '$sub_reward'}
        }}])
    accu_agent_stats = accu_agent_stats.next() if accu_agent_stats.alive else {}
    daily_agent_stats = agent_mg.daily_agent_stats.find_one({'agent_id': agent_id,
                                                             'day': str(tz.local_now()).split(' ')[0]}) or {}
    return dict(accu_agent_stats=accu_agent_stats, daily_agent_stats=daily_agent_stats)


def get_login_record(username, ip, status, order_by, page, size):
    _filter = list()
    for field in ('username', 'ip', 'status',):
        if locals()[field] == 0 or locals()[field]:
            _filter.append(getattr(LoginRecord, field) == locals()[field])
    query = LoginRecord.query.filter(*_filter)
    total_count = get_count(query)
    order_by = get_orderby(order_by, LoginRecord)
    if order_by is not None:
        query = query.order_by(order_by)
    query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def create_deactivate_record(user_id, account_status, operator, reason, auto_commit=False):
    record = DeactivateRecord()
    record.user_id = user_id
    record.account_status = account_status
    record.operator = operator
    record.reason = reason
    record.created_at = record.updated_at = datetime.utcnow()
    record.save(auto_commit=False)
    if auto_commit:
        orm.session.commit()


def get_agent_stats(agent_id):
    return agent_mg.agent_stats.find_one({'_id': agent_id}) or {}


def agent_role_mapper(agent_level):
    if agent_level == 0:
        return AGENT_ROLE.BOSS
    elif agent_level == 1:
        return AGENT_ROLE.TEAM_LEADER
    elif agent_level == 2:
        return AGENT_ROLE.CUSTOM_SERVICE
    elif agent_level >= 3:
        return AGENT_ROLE.REGULAR


def list_deactivated_users():
    deactivated_users = orm.session.query(DeactivateRecord.user_id).distinct()
    return [res[0] for res in deactivated_users]


def get_first_deactivated_time(user_id):
    record = DeactivateRecord.query.filter(DeactivateRecord.user_id == user_id).order_by(
        DeactivateRecord.created_at).first()
    return record.created_at if record else None


def list_deactivate_record(agent_id):
    return DeactivateRecord.query.filter(DeactivateRecord.user_id == agent_id).all()
