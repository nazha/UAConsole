# -*- coding: utf-8 -*-
from datetime import datetime
from common import orm
from common.utils.types import Enum


ROLE = Enum({
    "FORBIDDEN": (0, "forbidden"),  # 封号
    "USER": (1, "user"),
})

AGENT_ROLE = Enum({
    "BOSS": (0, u"总推"),
    "TEAM_LEADER": (1, u"组长"),
    "CUSTOM_SERVICE": (2, u"客服"),
    "REGULAR": (3, u"一般代理")
})

AGENT_APPLICATION_FORM_STATUS = Enum({
    "PENDING": (0, u"申请中"),
    "SUCCESS": (1, u'审核通过'),
    "FAIL": (2, u'审核不通过')
})


MAX_AGENT_LEVEL = 12
MIN_AGENT_LEVEL = 0
REGULAR_AGENT_START_LEVEL = 3


class User(orm.Model):
    __tablename__ = 'user'
    id = orm.Column(orm.Integer, primary_key=True)
    parent_id = orm.Column(orm.Integer, default=0)  # 上级代理id
    username = orm.Column(orm.VARCHAR)
    password = orm.Column(orm.VARCHAR)
    trade_password = orm.Column(orm.VARCHAR)
    nickname = orm.Column(orm.VARCHAR)
    balance = orm.Column(orm.Integer, default=0)  # 账户余额，整型：分為單位
    total_reward = orm.Column(orm.Integer, default=0.0)  # 历史总收入，整型：分為單位
    commission_rate = orm.Column(orm.Integer, default=0)  # 分成比例，整型: 30表示百分之30
    role = orm.Column(orm.SmallInteger)
    agent_level = orm.Column(orm.SmallInteger, default=0)
    bind_phone = orm.Column(orm.VARCHAR)
    bind_ip = orm.Column(orm.VARCHAR)
    bind_alipay = orm.Column(orm.VARCHAR)  # 绑定的支付宝账号
    bind_union = orm.Column(orm.VARCHAR)  # 绑定的银行卡账号
    extend = orm.Column(orm.Text)
    last_login = orm.Column(orm.DATETIME)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class UserToken(orm.Model):
    __tablename__ = 'user_token'
    user_id = orm.Column(orm.Integer, primary_key=True)
    token = orm.Column(orm.VARCHAR, primary_key=True)
    deleted = orm.Column(orm.SmallInteger)
    login_ts = orm.Column(orm.Integer)    # 登陆时间戳
    active_ts = orm.Column(orm.Integer)   # 活跃时间戳，用于设置过期时间
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class DeactivateRecord(orm.Model):
    __tablename__ = "deactivate_record"
    id = orm.Column(orm.Integer, primary_key=True)
    user_id = orm.Column(orm.Integer)
    account_status = orm.Column(orm.SmallInteger)
    operator = orm.Column(orm.VARCHAR)
    reason = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)
    updated_at = orm.Column(orm.DATETIME)


class LoginRecord(orm.Model):
    __tablename__ = 'login_record'
    id = orm.Column(orm.Integer, primary_key=True)
    username = orm.Column(orm.VARCHAR)
    ip = orm.Column(orm.VARCHAR)
    msg = orm.Column(orm.VARCHAR)
    status = orm.Column(orm.SmallInteger)
    desc = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME)

