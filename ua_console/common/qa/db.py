# -*- coding: utf-8 -*-
from datetime import datetime
from common.qa.model import QA
from common.utils.db import get_count, paginate


def get_qa(qa_id):
    return QA.query.filter(QA.id == qa_id).first()


def create_qa(qa_type, question, answer):
    qa = QA()
    qa.qa_type = qa_type
    qa.question = question
    qa.answer = answer
    qa.created_at = qa.updated_at = datetime.utcnow()
    qa.save()


def list_qa(page, size):
    query = QA.query
    total_count = get_count(query)
    query = paginate(query, dict(page=page, size=size))
    return query.all(), total_count


def delete_qa(qa_id):
    qa = QA.query.with_for_update().filter(QA.id == qa_id).first()
    if qa:
        qa.delete()


def update_ua(qa_id, qa_type, question, answer):
    qa = QA.query.with_for_update().filter(QA.id == qa_id).first()
    qa.qa_type = qa_type
    qa.question = question
    qa.answer = answer
    qa.save()

