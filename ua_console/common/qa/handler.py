# -*- coding: utf-8 -*-=
from common.qa import db
from common.utils import tz


def list_qa(page, size):
    items, total_count = db.list_qa(page, size)
    resp_items = []
    for item in items:
        data = item.as_dict()
        for key in ('created_at', 'updated_at',):
            data[key] = tz.utc_to_local_str(data[key])
        resp_items.append(data)
    return dict(list=resp_items, page=page or 1, size=len(resp_items), total_count=total_count)


def create_qa(qa_type, question, answer):
    db.create_qa(qa_type, question, answer)


def get_qa(qa_id):
    qa_item = db.get_qa(qa_id)
    for key in ('created_at', 'updated_at',):
        qa_item[key] = tz.utc_to_local_str(qa_item[key])
    return qa_item


def update_qa(qa_id, qa_type, question, answer):
    db.update_ua(qa_id, qa_type, question, answer)


def delete_qa(qa_id):
    db.delete_qa(qa_id)
