# -*- coding: utf-8 -*-=
from datetime import datetime
from common import orm
from common.utils.types import Enum


QA_TYPE = Enum({
    "ACCOUNT": (0, u"账号问题"),
    "TRANSACTION": (1, u"兑换问题"),
    "APPEAL": (2, u"申诉问题"),
    "Promotion": (3, u"推广问题")
})


class QA(orm.Model):
    __tablename__ = 'qa'
    id = orm.Column(orm.BigInteger, primary_key=True)
    qa_type = orm.Column(orm.Integer)
    question = orm.Column(orm.VARCHAR)
    answer = orm.Column(orm.VARCHAR)
    created_at = orm.Column(orm.DATETIME, default=datetime.utcnow())
    updated_at = orm.Column(orm.DATETIME, default=datetime.utcnow())
