# -*- coding: utf-8 -*-
from django.http import QueryDict
import json
import logging
from common import orm, admin_orm
from common.admin.model import ROLE
from common.admin import db as admin_db
from common.utils import JsonResponse
from common.utils.exceptions import PermissionError


_LOGGER = logging.getLogger(__name__)


def has_perm(target_perm, user_perm):
    if not target_perm:
        return True
    if not user_perm:
        return False
    user_perm = user_perm.split('|')
    target_perm = target_perm.split('|')
    intersects = list(set(user_perm) & set(target_perm))
    return False if not intersects else True


def check_perm(url_array, perm, role, user_perm):
    length = len(url_array)
    while length >= 1:
        url = '/'.join(url_array[0:length])
        if not url.endswith('/'):
            url += '/'
        k = admin_db.get_perm(url, perm)
        if k and not k.user_perm:
            return True
        if k and k.min_role <= role and (role == ROLE.ADMIN or has_perm(k.user_perm, user_perm)):
            return True
        length -= 1
    return False


class UserMiddleware(object):

    """get user_id and token from header"""
    def process_request(self, req):
        try:
            if req.method == 'GET':
                req.DATA = QueryDict(req.META['QUERY_STRING'])
            else:
                request_body = unicode(req.body or '{}', "utf-8")
                data = json.loads(request_body)
                req.DATA = QueryDict('', mutable=True)
                req.DATA.update(data)
        except:
            pass
        user_id, token = req.META.get('HTTP_X_AUTH_USER'), req.META.get('HTTP_X_AUTH_TOKEN')
        if not user_id:
            user_id, token = req.COOKIES.get('ua_user_id'), req.COOKIES.get('ua_user_token')
        if user_id and token:
            try:
                user_id = long(user_id)
            except ValueError:
                _LOGGER.error('user id format wrong!')
                req.user_id = None
                return
            if req.path.startswith('/ua/admin'):
                if req.path == '/ua/admin/user/' and req.method == 'POST':
                    return
                info = admin_db.get_online_info(user_id, token)
                if info and info.deleted == 0:
                    user = admin_db.get_user(user_id)
                    req.user_role = user.role
                    req.user_id = user_id
                    req.user_token = token
                    if user.role > 0:
                        url_array = req.path.split('/')
                        if req.method == 'GET':
                            need_perm = 1
                        else:
                            need_perm = 2
                        if not check_perm(url_array, need_perm, user.role, user.perm):
                            return JsonResponse(dict(
                                status=PermissionError.STATUS,
                                msg=str('permission not enough')),
                                status=PermissionError.HTTPCODE)
                        else:
                            req.user = user
                            return
                    else:
                        return JsonResponse(dict(
                            status=PermissionError.STATUS,
                            msg=str("user is forbidden or not activited")),
                            status=PermissionError.HTTPCODE)

        _LOGGER.warn('middleware not get user token, %s %s', user_id, token)
        req.user_id = req.user = None

    def process_response(self, req, resp):
        """拦截所有request出去, 把trace_id和此次访问所耗时间打印到log"""
        try:
            orm.session.close()
            admin_orm.session.close()
        except:
            _LOGGER.exception('middleware process response fail')
        return resp
