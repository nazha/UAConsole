# -*- coding: utf-8 -*-
"""
Django settings for base project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from base.jsonlogger import JsonFormatter

# 生成ID时使用，不同的机器应当使用不同的ID
SERVICE_ID = 1

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'mit2ojrfc54x#9%dptxc4c-!pa3ppj!h(5=@^h*346p-fs8888'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

COUNTRY = 'cn'

CELERY_BROKER = 'redis://127.0.0.1:6379//'

# 控制台文件导出路径
EXPORT_PATH = '/tmp/export_data/'
if not os.path.isdir(EXPORT_PATH):
    os.mkdir(EXPORT_PATH)

# Application definition

INSTALLED_APPS = (
    'admin',
    'corsheaders',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'base.middleware.UserMiddleware',
)

ROOT_URLCONF = 'base.urls'

WSGI_APPLICATION = 'base.wsgi.application'

SITE_ROOT = os.path.dirname(os.path.realpath(__file__))

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates"
    # "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(SITE_ROOT, 'templates'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_URL = '/ua/admin/static/'

STATICFILES_DIRS = [
    '/home/ubuntu/ua_console_res/',
]


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'zh-cn'

LOCALE_PATHS = ['/home/ubuntu/af-env/user_agent/locale']

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = True

USE_TZ = True

APPEND_SLASH = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

MYSQL_CONF = {
    'db': 'mysql://root:123456@127.0.0.1:3306/user_agent?charset=utf8',
    'DEBUG': True
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://root:123456@127.0.0.1:3306/user_agent_admin?charset=utf8',
    'DEBUG': True
}

MONGO_GAME_ADDR = '127.0.0.1:27017'
MONGO_AGENT_ADDR = '127.0.0.1:27017'

ENABLE_CODIS = False

REDIS_HOST = 'localhost'
REDIS_PORT = 6379

# LOG CONFIG
LOG_DIR = "/var/log/console/"
LOG_FILE = os.path.join(LOG_DIR, "agent.log")
LOG_ERR_FILE = os.path.join(LOG_DIR, "agent.err.log")
API_LOG_FILE = os.path.join(LOG_DIR, "api.log")
API_LOG_ERR_FILE = os.path.join(LOG_DIR, "api.err.log")
TRANS_LOG_FILE = os.path.join(LOG_DIR, "trans.log")
TRANS_LOG_ERR_FILE = os.path.join(LOG_DIR, "trans.err.log")
WORKER_LOG_FILE = os.path.join(LOG_DIR, "worker.log")
WORKER_LOG_ERR_FILE = os.path.join(LOG_DIR, "worker.err.log")
TRACK_LOG = os.path.join(LOG_DIR, 'track.json')
THIRD_LOG_FILE = os.path.join(LOG_DIR, "third.log")
THIRD_LOG_ERR_FILE = os.path.join(LOG_DIR, "third.err.log")


try:
    from base.env_settings import *
except ImportError:
    import logging
    logging.warn('no env specified settings loaded, use default.')
    pass


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'json_stat': {
            '()': JsonFormatter,
            'format': '%(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
        'detail': {
            'format': '%(levelname)s %(asctime)s [%(module)s.%(funcName)s line:%(lineno)d] %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_FILE,
        },
        'third_file': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': THIRD_LOG_FILE,
        },
        'trans_file': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': TRANS_LOG_FILE,
        },
        'api_file': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': API_LOG_FILE,
        },
        'worker_file': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': WORKER_LOG_FILE,
        },
        'err_file': {
            'level': 'WARN',
            'formatter': 'detail',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_ERR_FILE,
        },
        'trans_err_file': {
            'level': 'WARN',
            'formatter': 'detail',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': TRANS_LOG_ERR_FILE,
        },
        'api_err_file': {
            'level': 'WARN',
            'formatter': 'detail',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': API_LOG_ERR_FILE,
        },
        'third_err_file': {
            'level': 'WARN',
            'formatter': 'detail',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': THIRD_LOG_ERR_FILE,
        },
        'worker_err_file': {
            'level': 'WARN',
            'formatter': 'detail',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': WORKER_LOG_ERR_FILE,
        },
        'track_file': {
            'level': 'INFO',
            'formatter': 'json_stat',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': TRACK_LOG
        }
    },
    'loggers': {
        'agent': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['file', 'err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'trans': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['trans_file', 'trans_err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'api': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['api_file', 'api_err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'third': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['third_file', 'third_err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'worker': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['worker_file', 'worker_err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'timer': {
            'handlers': ['console', 'file', 'err_file'] if DEBUG else ['worker_file', 'worker_err_file'],
            'level': 'INFO',
            'propagate': False,
        },
        'tracker': {
            'handlers': ['track_file'],
            'level': 'INFO',
            'propagate': False
        },
        'dicttoxml': {
            'handlers': ['file', 'err_file'],
            'level': 'WARN',
            'propagate': False
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['console', 'file', 'err_file'] if DEBUG else ['file', 'err_file']
    }
}
