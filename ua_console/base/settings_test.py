# coding=utf-8
DEBUG = False

TEST_ENV = True

CORS_ORIGIN_ALLOW_ALL = True

MYSQL_CONF = {
    'db': 'mysql://root:123456@172.31.23.95:3306/user_agent?charset=utf8',
    'DEBUG': DEBUG
}

SLAVE_CONF = MYSQL_CONF

ADMIN_CONF = {
    'db': 'mysql://root:123456@172.31.23.95:3306/user_agent_admin?charset=utf8',
    'DEBUG': DEBUG
}

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379

MONGO_GAME_ADDR = '172.31.23.95:27017'
MONGO_AGENT_ADDR = '172.31.23.95:27017'


UNIONAGENCY_MCH_ID = '7'
UNIONAGENCY_API_KEY = '87918f5eece349e38c4cc0e01aad9009'
UNIONAGENCY_WITHDRAW_CHARGE_URL = 'http://test.maiunion.net:8084/api/v2/withdraw/create/'
UNIONAGENCY_WITHDRAW_QUERY_URL = 'http://test.maiunion.net:8084/api/v2/withdraw/query/'
UNIONAGENCY_WITHDRAW_NOTIFY_URL = 'http://14.29.47.211:8889/ua/admin/withdraw/notify/union/'


JUSTPAY_MCH_ID = '6001018'
JUSTPAY_API_KEY = '72b2a4d6d418469faa882099b98f403d'
JUSTPAY_TRANS_URL = 'http://pay.xyz115.com:9999/pay/api/trans/create/'
JUSTPAY_TRANS_QUERY_CHANNELS = 'http://pay.xyz115.com:9999/pay/api/trans/query_channels/'
JUSTPAY_WITHDRAW_NOTIFY_URL = 'http://14.29.47.211:8889/ua/admin/withdraw/notify/just/'

USER_AGENT_URL = 'http://18.136.80.153:8081/'
USER_AGENT_SUPER_KEY = '10eda57b-8149-44c1-8412-7426065cb3bc'

MAESTRO_API_KEY = 'b5384a02ab10ba532b9e611fcf15c08e83b39c1c'
MAESTRO_GET_CUSTOM_STATS_API = 'http://www.dissmy.com/api/dwc/agent/get_custom_service_report/'
MAESTRO_MANUAL_UPDATE_AGENT_CHN = 'http://www.dissmy.com/api/dwc/agent/{}/manual_bind_chn/'
MAESTRO_GET_USER_AGENT_INFO = "http://www.dissmy.com/api/dwc/agent/{}/get_user_agent_chn/"
MAESTRO_GET_FINANCIAL_REPORT = "http://www.dissmy.com/api/dwc/agent/{}/get_financial_report/"
MAESTRO_PLATFORM_MAPPER = {
    1: 'dark2',
    2: 'zs',
    3: 'dark3',
    4: 'tt',
    5: 'dark3'
}

SHORT_LINK_HOST = 'http://3.1.59.147:9942'
SHORT_LINK_API_USER = 'charles'
SHORT_LINK_API_KEY = '1bd6625f-e141-4027-9ac9-fc3e8bbf1778'

POST_DIR = '/www/agentweb/'
POST_BG_GROUP = {
    'template_a': {
        'qr_size': (365, 365),
        'qr_pos': (207, 754),
    },
    'template_b': {
        'qr_size': (225, 225),
        'qr_pos': (527, 1028),
    }
}

ROOT_IDS = {
}

SPECIAL_ROOTS = {
}
