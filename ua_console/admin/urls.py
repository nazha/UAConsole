# coding=utf-8
from django.conf.urls import patterns, url
from admin.views.user import user
from admin.views.withdraw import withdraw
from admin.views.agent import agent
from admin.views.stats import stats
from admin.views.announce import announce
from admin.views.qa import qa
from admin.views.template_management import template_management
from admin.views.resource import resource
from admin.views.system_recharge import system_recharge
from admin.views.wechat import wechat
from admin.views.user_chn import user_chn

urlpatterns = patterns(
    '',
    # 控制台用戶相關
    url(r'^user/login/?$', user.login),
    url(r'^user/logout/?$', user.logout),
    url(r'^user/?$', user.UserView.as_view()),
    url(r'^user/(?P<user_id>\d+)/?$', user.SingleUserView.as_view()),
    url(r'^permission/?$', user.PermissionView.as_view()),
    # 代理用戶相關
    url(r'^agent/?$', agent.AgentView.as_view()),
    url(r'^agent/(?P<user_id>\d+)/?$', agent.SingleAgentView.as_view()),
    url(r'^agent/get_parent_activated_platform/?', agent.get_parent_activated_platform),
    url(r'^agent/create_root_agent/?$', agent.create_root_agent),
    url(r'^agent/get_create_root_preset/?$', agent.get_create_root_preset),
    url(r'^agent/login_record/?$', agent.login_record),
    url(r'^agent/deactivated/get_deactivated_user_list/?$', agent.get_deactivated_user_list),
    url(r'^agent/deactivated/get_deactivate_history_list/?$', agent.get_deactivate_history_list),
    url(r'^agent/deactivated/get_deactivate_stats_list/?$', agent.get_deactivate_record_list),
    # 代理管理
    url(r'^agent/management/root/?$', agent.list_root_agent),
    url(r'^agent/management/root/(?P<agent_id>\d+)/?$', agent.update_root_agent),
    url(r'^agent/management/team_leader/(?P<agent_id>\d+)/?$', agent.update_team_leader),
    url(r'^agent/management/team_leader/?$', agent.list_team_leader),
    url(r'^agent/management/team_leader/(?P<agent_id>\d+)/?$', agent.update_team_leader),
    url(r'^agent/management/custom_service/?$', agent.list_custom_service),
    url(r'^agent/management/custom_service/(?P<agent_id>\d+)/?$', agent.update_custom_service),
    url(r'^agent/management/custom_service/list_application_form/(?P<agent_id>\d+)/?$',
        agent.list_child_agent_application_form),
    url(r'^agent/management/custom_service/verify_application/?$', agent.verify_child_agent_application_form),
    url(r'^agent/management/regular_agent/?$', agent.list_regular_agent),
    url(r'^agent/management/regular_agent/activate_chn/?$', agent.activate_chn_resource),
    # 報表相關
    url(r'^stats/agent/?$', stats.list_agent_stats),
    url(r'^stats/financial/?$', stats.list_financial_stats),
    url(r'^stats/platform/?$', stats.list_platform_stats),
    url(r'^stats/root_reward/?$', stats.list_custom_service_reward_stats),
    # 提現相關
    url(r'^withdraw/?$', withdraw.WithdrawView.as_view()),
    url(r'^withdraw/(?P<withdraw_id>\d+)/?$', withdraw.SingleWithdrawView.as_view()),
    url(r'^withdraw/check/?$', withdraw.batch_check_withdraw),
    url(r'^withdraw/check/(?P<withdraw_id>\d+)/(?P<check_status>\d+)/?$', withdraw.check_withdraw),
    url(r'^withdraw/notify/union/?$', withdraw.union_withdraw_notify),
    url(r'^withdraw/notify/just/?$', withdraw.just_withdraw_notify),
    # 代理公告、幫助相關
    url(r'^announce/?$', announce.AnnounceView.as_view()),
    url(r'^announce/(?P<announce_id>\d+)/?$', announce.SingleAnnounceView.as_view()),
    url(r'^qa/?$', qa.QAView.as_view()),
    url(r'^qa/(?P<qa_id>\d+)/?$', qa.SingleQAView.as_view()),
    # 推廣海報模板相關
    url(r'^template/create_template/?$', template_management.set_list_template_manage),
    url(r'^template/list_template/?$', template_management.lists_template_manage),
    url(r'^template/delete_template/?$', template_management.delete_template_manage),
    # 渠道資源相關
    url(r'^resource/get_platform_conf/?$', resource.get_platform_conf),
    url(r'^resource/create_chn_resource/?$', resource.create_chn_resource),
    url(r'^resource/recreate_chn_resource/?$', resource.recreate_chn_resource),
    url(r'^resource/get_chn_resource/?$', resource.get_chn_resource),
    url(r'^resource/get_root_chn_distribution/?$', resource.get_root_chn_distribution),
    url(r'^resource/get_chn_type_distribution/?$', resource.get_chn_type_distribution),
    url(r'^resource/allocate_chn_to_root/?$', resource.allocate_chn_to_root),
    url(r'^resource/delete_chn_resource/?$', resource.delete_chn_resource),
    # 渠道類型相關
    url(r'^resource/create_or_update_chn_type/?$', resource.create_or_update_chn_type),
    url(r'^resource/get_chn_type_detail/?$', resource.get_chn_type_detail),
    # 遊戲用戶渠道相關
    url(r'^user_chn/manual_assign_chn/?$', user_chn.manual_assign_chn),
    url(r'^user_chn/list_manual_assign_chn/?$', user_chn.list_manual_assign_chn),
    url(r'^user_chn/get_user_chn_info/?$', user_chn.get_user_chn_info),
    url(r'^user_chn/get_ios_chn/?$', user_chn.get_ios_chn),
    # 系統充值
    url(r'^system_recharge/?$', system_recharge.SysRechargeView.as_view()),
    url(r'^system_recharge/create/(?P<trans_id>[^/]+)/?$', system_recharge.create_sys_recharge),
    # 微信管理
    url(r'^wechat/get/?$', wechat.fetch_wechat),
    url(r'^wechat/post/?$', wechat.submit_wechat),
    url(r'^wechat/delete/?$', wechat.delete_wechat),
)
