# -*- coding: utf-8 -*-
import os
import sys
import logging
from datetime import datetime, timedelta
# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'
from django.core.management.base import BaseCommand
from common import agent_mg


_LOGGER = logging.getLogger('agent')


class Command(BaseCommand):
    def __init__(self):
        super(Command, self).__init__()

    @staticmethod
    def update_user_tax():
        user_tax_without_path = agent_mg.user_tax.find({"agent_path": {"$exists": 0}})
        for idx, user_tax in enumerate(user_tax_without_path):
            day = str(user_tax['updated_at'] + timedelta(hours=8)).split(' ')[0]
            if day < str(datetime(2019, 3, 10)):
                continue
            agent = agent_mg.daily_agent_stats.find_one({'agent_id': user_tax['agent_id'], 'day': day})
            _id = user_tax['_id']
            agent_mg.user_tax.update_one({'_id': _id},
                                         {'$set': {'agent_path': agent['path']}})
            print idx, day

    @staticmethod
    def move_user_tax_to_back_up():
        pass

    def handle(self, **options):
        # self.create_daily_agent_split_stats_for_agent_role()
        self.update_user_tax()
