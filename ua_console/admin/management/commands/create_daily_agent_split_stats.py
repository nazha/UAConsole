# -*- coding: utf-8 -*-
import os
import sys
import copy
import logging
from datetime import datetime, timedelta
# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'
from django.core.management.base import BaseCommand
from common import agent_mg
from common.chn.model import CHN_STATUS
from common.chn.db import get_platform_chn


_LOGGER = logging.getLogger('agent')


class Command(BaseCommand):
    def __init__(self):
        super(Command, self).__init__()
        self.platform_conf = get_platform_chn()

    def get_platform_id_by_chn_type(self, chn_type):
        for platform_id, platform_item in self.platform_conf.iteritems():
            chn_type_list = [chn.replace('ios_', '') for chn in platform_item['chn_types']]
            if chn_type not in chn_type_list:
                continue
            else:
                return platform_id

    def create_daily_agent_split_stats_for_agent_role(self):
        agent_daily_agent_stats = agent_mg.daily_agent_stats.find({'agent_level': 3,
                                                                   'day': {'$lt': '2019-04-24',
                                                                           '$gte': '2018-09-01'}})
        for stats in agent_daily_agent_stats:
            day = stats['day']
            channels = stats['chns']
            if 'platform_id' not in stats or 'chn_type' not in stats:
                chn_resource = agent_mg.chn_pool.find_one({"chn": {"$in": channels}, 'status': CHN_STATUS.USED})
                if not chn_resource:
                    print channels
                stats['platform_id'] = self.get_platform_id_by_chn_type(chn_resource['chn_type'])
                stats['chn_type'] = chn_resource['chn_type']
            stats['platform_chn'] = str(int(stats['platform_id'])) + '%' + stats['chn_type']
            key = '%s-%s-%s-%s' % (day, str(stats['agent_id']), str(stats['platform_id']), stats['chn_type'])
            stats.pop('_id', None)
            print('successfully create {}-{}'.format(stats['agent_id'], stats['day']))
            agent_mg.daily_agent_split_stats.update_one({'_id': key}, {'$set': stats}, upsert=True)

    def create_daily_agent_split_stats_for_others(self):
        day = datetime(2018, 9, 5)
        while str(day) < str(datetime(2019, 4, 24)):
            day_str = str(day).split(' ')[0]
            agent_daily_agent_stats = agent_mg.daily_agent_stats.find({'agent_level': {"$lt": 3}, 'day': day_str}).sort(
                [('agent_level', -1), ('day', -1)])
            agent_dct = {}
            for agent in copy.deepcopy(agent_daily_agent_stats):
                agent_dct.setdefault(int(agent['agent_id']), [])
            regular_agent_stats = agent_mg.daily_agent_split_stats.find({'agent_level':  3, 'day': day_str})
            for agent in regular_agent_stats:
                agent_dct.setdefault(int(agent['parent_id']), [])
                agent_dct[int(agent['parent_id'])].append(agent)
            for idx, stats in enumerate(agent_daily_agent_stats):
                child_agent_stats = agent_dct[int(stats['agent_id'])]
                res = {}
                for child_stats in child_agent_stats:
                    res.setdefault(child_stats['chn_type'], {'extend': {'total': {}, 'sub': {}, 'user': {}}})
                    res[child_stats['chn_type']]['platform_id'] = child_stats['platform_id']
                    for key in ('new_sub_tax', 'sub_tax', 'new_total_tax', 'total_tax',
                                'new_user_tax', 'user_tax'):
                        if key in child_stats:
                            res[child_stats['chn_type']].setdefault(key, 0)
                            if 'user' not in key:
                                res[child_stats['chn_type']][key] += child_stats[key]
                            else:
                                res[child_stats['chn_type']][key] = 0
                    for key in ('new_sub_reward', 'new_total_reward', 'new_user_reward', 'sub_reward', 'total_reward'
                                , 'user_reward'):
                        if key in child_stats:
                            res[child_stats['chn_type']].setdefault(key, 0)
                            if 'user' not in key:
                                if 'new' in key:
                                    res[child_stats['chn_type']][key] += child_stats['new_total_tax'] * (stats['commission_rate'] - child_stats['commission_rate']) / 100.0
                                else:
                                    res[child_stats['chn_type']][key] += child_stats['total_tax'] * (stats['commission_rate'] - child_stats['commission_rate']) / 100.0

                            else:
                                res[child_stats['chn_type']][key] = 0
                    for stats_type in ['total', 'sub', 'user']:
                        for target_field in ['active_count', 'register_count', 'total_recharge', 'agency_recharge',
                                             'total_withdraw', 'recharge_count', 'withdraw_count', 'online_recharge']:
                            for user_type in ['all', 'new']:
                                if stats_type == 'user':
                                    if user_type == 'new':
                                        if 'new' not in child_stats['extend'][stats_type]:
                                            continue
                                        res[child_stats['chn_type']]['extend'][stats_type].setdefault(user_type, {})
                                        res[child_stats['chn_type']]['extend'][stats_type][user_type][target_field] = 0
                                    else:
                                        res[child_stats['chn_type']]['extend'][stats_type][target_field] = 0
                                elif user_type == 'new':
                                    if 'new' not in child_stats['extend'][stats_type]:
                                        continue
                                    target_value = child_stats['extend']['total'][user_type][target_field]
                                    res[child_stats['chn_type']]['extend'][stats_type].setdefault(user_type, {})
                                    res[child_stats['chn_type']]['extend'][stats_type][user_type].setdefault(target_field, 0)
                                    res[child_stats['chn_type']]['extend'][stats_type][user_type][target_field] += target_value

                                else:
                                    target_value = child_stats['extend']['total'][target_field]
                                    res[child_stats['chn_type']]['extend'][stats_type].setdefault(target_field, 0)
                                    res[child_stats['chn_type']]['extend'][stats_type][target_field] += target_value

                print('successfully create {}-{}, count: {}'.format(stats['agent_id'], stats['day'], len(res)))
                for chn_type, split_stats in res.iteritems():
                    if chn_type is None:
                        print('error {}'.format(stats['agent_id']))
                        exit()
                    split_stats['agent_id'] = stats['agent_id']
                    split_stats['chns'] = []
                    split_stats['day'] = stats['day']
                    split_stats['username'] = stats['username']
                    split_stats['nickname'] = stats['nickname']
                    split_stats['bind_phone'] = stats['bind_phone']
                    split_stats['parent_id'] = stats['parent_id']
                    split_stats['agent_level'] = stats['agent_level']
                    split_stats['path'] = stats['path']
                    split_stats['role'] = stats['role']
                    split_stats['commission_rate'] = stats['commission_rate']
                    platform_id = self.get_platform_id_by_chn_type(chn_type)
                    split_stats['chn_type'] = chn_type
                    split_stats['platform_id'] = platform_id
                    split_stats['platform_chn'] = str(int(split_stats['platform_id'])) + '%' + split_stats['chn_type']
                    key = '%s-%s-%s-%s' % (day_str, str(stats['agent_id']), str(platform_id), chn_type)
                    agent_mg.daily_agent_split_stats.update_one({'_id': key}, {'$set': split_stats}, upsert=True)
                    agent_dct.setdefault(int(stats['parent_id']), [])
                    agent_dct[int(stats['parent_id'])].append(split_stats)
            day = day + timedelta(days=1)

    def handle(self, **options):
        #self.create_daily_agent_split_stats_for_agent_role()
        self.create_daily_agent_split_stats_for_others()
