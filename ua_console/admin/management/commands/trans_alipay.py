#! -*- coding:utf-8 -*-
import fcntl
import json
import logging
from django.core.management.base import BaseCommand
from common.withdraw.db import withdraw_back, CAN_BE_BACK_STATUS
from common.withdraw.model import WITHDRAW_TYPE, WITHDRAW_STATUS, Withdraw, CHECK_STATUS
from common.utils import tz
from common.utils import exceptions as err
from common.withdraw import check_user_risk, get_available_withdraw_type
from common.withdraw.unionagency_withdraw import create_charge
from common.utils.currency import convert_fen_to_yuan
from common.withdraw.just_withdraw import create_order as just_withdraw_pay

_LOGGER = logging.getLogger('trans')

_SYSTEM_ADMIN_ID = -1


def back_disabled_orders():
    """
    对当前关闭的提现方式自动返款，未通过审核的订单需人工返款
    """
    items = Withdraw.query.filter(Withdraw.check_status == CHECK_STATUS.NULL). \
        filter(Withdraw.status.in_(CAN_BE_BACK_STATUS)). \
        filter(Withdraw.target_type.notin_(get_available_withdraw_type())).all()
    withdraw_ids = []
    for item in items:
        withdraw_ids.append(item.id)
    for withdraw_id in withdraw_ids:
        try:
            withdraw_back(_SYSTEM_ADMIN_ID, withdraw_id, u'上游通道故障，提现失败返款')
        except err.DbError:
            print 'withdraw status invalid, withdraw_id: %s' % withdraw_id


def check_wait_order():
    items = Withdraw.query.filter(
        Withdraw.status == WITHDRAW_STATUS.WAIT,
        Withdraw.check_status == CHECK_STATUS.NULL,
        Withdraw.target_type.in_(get_available_withdraw_type())).all()
    for item in items:
        uid = item.user_id
        _LOGGER.info('check_wait_order, ready to check id:%s, uid:%s, price:%s',
                     item.id, uid, item.price)
        try:
            passed, reason = check_user_risk(uid, item)
        except Exception as e:
            _LOGGER.exception('check_user_risk error, %s', e)
            continue
        if not passed:
            _LOGGER.warn(u'check_wait_order, id %s uid %s in risk, reason:%s skip it!',
                         item.id, uid, reason)
            updated_info = json.loads(item.extend)
            updated_info.update({'auto_risk': reason, 'incheck_time': tz.local_now().strftime('%Y-%m-%d %H:%M:%S')})
            item.extend = json.dumps(updated_info, ensure_ascii=False)
            item.check_status = CHECK_STATUS.WAITING
            item.save()
            continue
        item.status = WITHDRAW_STATUS.AUTO
        item.save()
        _LOGGER.info('check_wait_order, check id:%s succ', item.id)


def apply_unionagency_withdraw_wait_order():
    items = Withdraw.query.filter(
        Withdraw.target_type == WITHDRAW_TYPE.BANKCARD).filter(
        Withdraw.status == WITHDRAW_STATUS.AUTO).all()
    _LOGGER.info('Auto bankcard withdraw, fetch %d withdraw items', len(items))
    for item in items:
        item.real_price = int(item.price)
        item.save()
        try:
            res = create_charge(item.user_id, item)
            _LOGGER.info('create_charge results: %s', res)
        except Exception as e:
            _LOGGER.exception('bankcard submit to third exception')


def pay(order, amount, alipay_num, alipay_name):
    amount = convert_fen_to_yuan(amount)
    oid = order.id
    count = sum([int(i) for i in str(oid)[-3:]])
    trans_info = {}
    # 三个个位数之和的 范围 0～27
    if count >= 0:
        # JustPay 下分
        _LOGGER.info('ready to pay by just, order %s', oid)
        status = WITHDRAW_STATUS.SUBMIT_TO_THIRD
        just_withdraw_pay(order.id, alipay_num, alipay_name, amount, order.user_id)
        trans_info.update({
            'payer_no': 'justpay',
            'code': 'submit',
            'amount': float(amount),
            'order_id': oid,
            'out_biz_no': '',
            'pay_date': tz.local_now().strftime('%Y-%m-%d %H:%M:%S'),
        })
    return status, trans_info


def apply_alipay_withdraw():
    items = Withdraw.query.filter(
        Withdraw.target_type == WITHDRAW_TYPE.ALIPAY).filter(
        Withdraw.status == WITHDRAW_STATUS.AUTO).all()
    _LOGGER.info('Auto trans alipay, fetch %d withdraw items', len(items))
    for item in items:
        item = Withdraw.query.filter(Withdraw.id == item.id).filter(
            Withdraw.status == WITHDRAW_STATUS.AUTO).with_lockmode('update').first()
        if not item:
            continue
        updated_info = json.loads(item.extend)
        try:
            alipay_num = updated_info['info']['alipay_no']
        except:
            continue
        alipay_name = updated_info['info']['name']
        # trans api
        amount = item.price
        item.real_price = item.price
        status, trans_info = pay(item, amount, alipay_num, alipay_name)
        item.status = status
        updated_info.update({'auto_trans_info': trans_info})
        item.extend = json.dumps(updated_info, ensure_ascii=False)
        item.save()


def start():
    check_wait_order()
    # back_disabled_orders()
    apply_unionagency_withdraw_wait_order()
    apply_alipay_withdraw()


class Command(BaseCommand):
    def handle(self, **kwargs):
        try:
            f = open('/tmp/flock_ua_withdraw', 'w')
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            start()
            fcntl.flock(f, fcntl.LOCK_UN)
        except Exception as e:
            _LOGGER.exception('trans error')
            raise e
