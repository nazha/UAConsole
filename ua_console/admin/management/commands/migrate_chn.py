#! -*- coding:utf-8 -*-
import logging
import copy
from django.core.management.base import BaseCommand
from common.agent import handler
from common import agent_mg


_LOGGER = logging.getLogger('agent')


MIGRATE_AGENT_INPUT_FILE = None
ORIGIN_PLATFORM_ID = 4
ORIGIN_CHN_TYPE = 'yydl'
MIGRATE_PLATFORM_ID = 5
MIGRATE_CHN_TYPE = 'yyhqdl'


def get_agent_chn_resource(agent_id, platform_id=None, chn_type=None):
    condition = dict(agent_id=agent_id)
    if platform_id:
        condition['project_id'] = platform_id
    if chn_type:
        condition['chn_type'] = chn_type
    return agent_mg.chn_pool.find(condition)


class Command(BaseCommand):
    def __init__(self):
        super(Command, self).__init__()
        self.migrate_agent_input_file_path = MIGRATE_AGENT_INPUT_FILE
        self.origin_platform_id = ORIGIN_PLATFORM_ID
        self.origin_chn_type = ORIGIN_CHN_TYPE
        self.migrate_platform_id = MIGRATE_PLATFORM_ID
        self.migrate_chn_type = MIGRATE_CHN_TYPE
        self.migrate_agents = self.get_migrate_agent()

    def get_migrate_agent(self):
        if self.migrate_agent_input_file_path:
            with open(self.migrate_agent_input_file_path) as f:
                migrate_agents = [int(e.strip()) for e in f]
        else:
            channels = agent_mg.chn_pool.find(
                {"project_id": self.origin_platform_id, "chn_type": self.origin_chn_type, "status": 2})
            migrate_agents = [e['agent_id'] for e in channels]
        return migrate_agents

    def validate_migrate_agent(self):
        """
        1. 檢查代理合法性：
           a. 代理id存在
           b. 代理層級為2
        2. 檢查代理主產品合法性：
           a. 主產品為轉移前產品
        3. 檢查代理客服主是否包含此主產品
        4. 渠道數量充足
        """
        print('{}个代理即将转移'.format(len(self.migrate_agents)))
        custom_service_cache = dict()
        root_resource_map = dict()
        for idx, agent_id in enumerate(self.migrate_agents):
            agent = agent_mg.agent_stats.find_one({"_id": agent_id})
            migrate_chn = get_agent_chn_resource(agent_id, self.migrate_platform_id, self.migrate_chn_type)
            if not migrate_chn.count():
                print(agent_id)
                root_resource_map.setdefault(agent['path'][0], 0)
                root_resource_map[agent['path'][0]] += 1
            print('index：{}，合法转移：{}'.format(idx, agent_id))
        print('客服列表: {}'.format(custom_service_cache.keys()))
        for root_id, required in root_resource_map.iteritems():
            available_chn_count = agent_mg.chn_pool.count({'root_id': root_id, "status": 0,
                                                           "project_id": self.migrate_platform_id,
                                                           "chn_type": self.migrate_chn_type})
            print('黑棋国际，总推ID{} 需要：{}个, 已有：{}个资源'.format(root_id, required, available_chn_count))
            assert available_chn_count >= required, '资源不足'

    def allocate_chn(self):
        for agent_id in self.migrate_agents:
            migrate_chn = get_agent_chn_resource(agent_id, self.migrate_platform_id, self.migrate_chn_type)
            if not migrate_chn.count():
                handler.activate_chn_resource(agent_id, self.migrate_platform_id, self.migrate_chn_type)

    def switch_on_promote_chn(self):
        for agent_id in self.migrate_agents:
            agent_mg.agent_stats.update_one(
                {'_id': agent_id},
                {'$set': {'marketing_setup.on_promote_chn_type.platform_id': self.migrate_platform_id,
                          'marketing_setup.on_promote_chn_type.chn_type': self.migrate_chn_type}})

    def disable_origin_chn(self):
        for idx, agent_id in enumerate(self.migrate_agents):
            agent = agent_mg.agent_stats.find_one({"_id": agent_id})
            for ancestor_id in agent['path']:
                ancestor = agent_mg.agent_stats.find_one({"_id": ancestor_id})
                activated_platform = ancestor['marketing_setup']['activated_platform']
                for p_idx, platform in enumerate(copy.deepcopy(activated_platform)):
                    for c_idx, chn_type_item in enumerate(platform['chn_types']):
                        print(platform['platform_id'], self.origin_platform_id)
                        if platform['platform_id'] == self.origin_platform_id:
                            print(chn_type_item['name'], self.origin_chn_type)
                        if (platform['platform_id'] == self.origin_platform_id and
                                chn_type_item['name'] == self.origin_chn_type):
                            if len(platform['chn_types']) > 1:
                                print('test')
                                del activated_platform[p_idx]['chn_types'][c_idx]
                            else:
                                print('test1')
                                del activated_platform[p_idx]
                agent_mg.agent_stats.update_one({'_id': ancestor_id},
                                                {'$set': {'marketing_setup.activated_platform': activated_platform}})
                agent_mg.chn_pool.update_one({'agent_id': ancestor_id, 'project_id': self.origin_platform_id,
                                             'chn_type': self.origin_chn_type},
                                            {"$set": {"agent_id": None, "status": 3}})

    def handle(self, **options):
        self.validate_migrate_agent()
        self.allocate_chn()
        self.switch_on_promote_chn()
        self.disable_origin_chn()
