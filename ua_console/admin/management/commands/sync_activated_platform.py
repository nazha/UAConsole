# -*- coding: utf-8 -*-
import os
import sys
import copy
import logging
# add up one level dir into sys path
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__)))))
os.environ['DJANGO_SETTINGS_MODULE'] = 'base.settings'
from django.core.management.base import BaseCommand
from common import agent_mg
from common.chn.db import get_platform_chn
from common.agent.db import agent_role_mapper
from common.agent.model import AGENT_ROLE
from common.agent.module.activated_platform_handler import ActivatedPlatformHandler
from common.utils import exceptions as err


_LOGGER = logging.getLogger('agent')


class Command(BaseCommand):
    def __init__(self):
        super(Command, self).__init__()
        self.platform_conf = get_platform_chn()
        root_agents = agent_mg.agent_stats.find({'agent_level': 0})
        self.root_agent_mapper = {agent['_id']: agent for agent in root_agents}

    def pre_process_activate_chn_type(self, root_id, activate_chn_type):
        processed_activate_chn_type = dict()
        reversed_platform_conf = {platform_item['name']: platform_item
                                  for platform_id, platform_item in self.platform_conf.iteritems()}
        min_set_activate_chn_type = dict()
        platform_id_list = agent_mg.agent_stats.aggregate([
            {'$match': {'path': root_id}},
            {"$group": {'_id': "$platform_id"}}
        ])
        for platform in platform_id_list:
            if platform['_id'] is None:
                continue
            platform_name = self.platform_conf[platform['_id']]['name']
            min_set_activate_chn_type[platform_name] = []
            chn_type_list = agent_mg.agent_stats.aggregate([
                {'$match': {'path': root_id, 'platform_id': platform['_id']}},
                {"$group": {'_id': "$chn_type"}}
            ])
            for chn_type in chn_type_list:
                if chn_type['_id'] is None:
                    continue
                min_set_activate_chn_type[platform_name].append(chn_type['_id'])
        for platform_name in min_set_activate_chn_type:
            if platform_name not in activate_chn_type:
                activate_chn_type[platform_name] = []
        for platform_name, chn_type_list in activate_chn_type.iteritems():
            if 'all' in chn_type_list:
                chn_type_list = [chn_type_name for chn_type_name in reversed_platform_conf[platform_name]['chn_types']]
            if platform_name in min_set_activate_chn_type:
                for chn_type in min_set_activate_chn_type[platform_name]:
                    if chn_type not in chn_type_list:
                        chn_type_list.append(chn_type)
            processed_activate_chn_type[platform_name] = chn_type_list
        return processed_activate_chn_type

    def attach_attach_chn_resource(self):
        agents = agent_mg.agent_stats.find({'agent_level': {"$gte": 3}}).sort('agent_level', -1)
        for agent in agents:
            agent_id = agent['_id']
            platform_id = agent['platform_id']
            chn_type = agent['chn_type']
            chn_resource = agent['chns']
            assert chn_resource
            ancestors = agent_mg.agent_stats.find({'_id': {'$in': agent['path']}}).sort('agent_level', -1)
            total_count = agent_mg.agent_stats.count({'_id': {'$in': agent['path']}})
            activated_platform_mapper = dict()
            modification_status_list = [False] * total_count
            for idx, ancestor in enumerate(ancestors):
                activated_platform_mapper[ancestor['_id']] = ancestor['marketing_setup']['activated_platform']
                if platform_id not in [platform['platform_id'] for platform in
                                       copy.deepcopy(activated_platform_mapper[ancestor['_id']])]:
                    activated_platform_mapper[ancestor['_id']].append(dict(
                        platform_id=platform_id,
                        platform_name=self.platform_conf[platform_id]['name'],
                        platform_label=self.platform_conf[platform_id]['label'],
                        chn_types=[])
                    )
                for platform_idx, platform in enumerate(activated_platform_mapper[ancestor['_id']]):
                    if platform['platform_id'] != platform_id:
                        continue
                    if chn_type not in [chn_type_item['name'] for chn_type_item in
                                        copy.deepcopy(platform['chn_types'])]:
                        activated_platform_mapper[ancestor['_id']][platform_idx]['chn_types'].append(dict(
                            name=chn_type,
                            chn_type_name=self.platform_conf[platform_id]['chn_types'][chn_type]['chn_type_name'],
                            resource=list(),
                            activated=False
                        ))
                    for chn_type_item in platform['chn_types']:
                        if chn_type_item['name'] != chn_type:
                            continue
                        chn_type_item['activated'] = True
                        if ancestor['_id'] == agent_id:
                            chn_type_item['resource'] = chn_resource
                        print(idx, agent_id)
                        modification_status_list[idx] = True
            if False in modification_status_list:
                raise err.DataError(u'数据异常，洽客服人员')
            for agent_id, activated_platform in activated_platform_mapper.iteritems():
                agent_mg.agent_stats.update_one({'_id': agent_id}, {'$set': {
                    'marketing_setup.activated_platform': activated_platform
                }}, upsert=True)

    def sync_agent_activate_chn_type(self):
        agents = agent_mg.agent_stats.find({}).sort('agent_level', -1)
        for agent in agents:
            root_id = agent['path'][0]
            activate_chn_type = self.pre_process_activate_chn_type(root_id,
                                                                   self.root_agent_mapper[root_id]['activate_chn_type'])
            agent_role = agent_role_mapper(agent['agent_level'])
            if agent_role == AGENT_ROLE.BOSS:
                activated_platform = ActivatedPlatformHandler.transform_activated_platform(activate_chn_type)
                marketing_setup = {
                    'activated_platform': activated_platform
                }
            elif agent_role == AGENT_ROLE.TEAM_LEADER:
                platform_id = agent['platform_id']
                platform_name = self.platform_conf[platform_id]['name']
                activate_chn_type = {platform_name: activate_chn_type[platform_name]}
                activated_platform = ActivatedPlatformHandler.transform_activated_platform(activate_chn_type)
                marketing_setup = {
                    'activated_platform': activated_platform
                }
            else:
                if agent_role == AGENT_ROLE.REGULAR:
                    target_agent = agent_mg.agent_stats.find_one({'_id': agent['parent_id']})
                else:
                    target_agent = agent
                platform_id = target_agent['platform_id']
                chn_type = target_agent['chn_type']
                platform_name = self.platform_conf[platform_id]['name']
                activate_chn_type = {platform_name: [chn_type]}
                activated_platform = ActivatedPlatformHandler.transform_activated_platform(activate_chn_type)
                marketing_setup = {
                    'activated_platform': activated_platform,
                    'on_promote_chn_type': {
                        'platform_id': platform_id,
                        'chn_type': chn_type
                    }
                }
            agent_mg.agent_stats.update_one({'_id': agent['_id']},
                                            {'$set': {'marketing_setup': marketing_setup},
                                             '$unset': {'activate_chn_type': 1}},
                                            upsert=True)
            print(agent['_id'])

    def handle(self, **options):
        self.sync_agent_activate_chn_type()
        self.attach_attach_chn_resource()
