#! -*- coding:utf-8 -*-
import logging
from datetime import timedelta
from django.core.management.base import BaseCommand
from common.agent import db
from common.chn.db import get_platform_chn
from common.system_recharge.db import get_users_daily_system_recharge
from common.utils.currency import convert_fen_to_yuan
from common import agent_mg
from common.utils import tz
from optparse import make_option


_LOGGER = logging.getLogger('agent')


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            "-d",
            "--day",
            dest="day",
            help="day shift to calculate",
        ),
    )

    def __init__(self):
        super(Command, self).__init__()
        self.aggs_key_list = ('platform_id', 'chn_type',)
        self.platform_conf = get_platform_chn()
        self.day = tz.get_utc_date() + timedelta(hours=8)
        self.today_str = tz.utc_to_local(self.day).strftime('%Y-%m-%d')
        self.platform_system_recharge = {}

    def add_arguments(self, parser):
        parser.add_argument("--day", dest='day', help='please input a number....')

    @staticmethod
    def calc_platform_system_recharge(today_str):
        platform_system_recharge = dict()
        system_recharges = get_users_daily_system_recharge(date_str=today_str)
        for system_recharge in system_recharges:
            agent_stats = db.get_agent_stats(system_recharge[0])
            agent_platform_id = agent_stats.get('platform_id')
            if agent_platform_id:
                platform_system_recharge.setdefault(agent_platform_id, dict(total=0))
                platform_system_recharge[agent_platform_id]['total'] += float(system_recharge[1])
                agent_chn_type = agent_stats.get('chn_type')
                if agent_chn_type:
                    platform_system_recharge[agent_platform_id].setdefault(agent_chn_type, dict(total=0))
                    platform_system_recharge[agent_platform_id][agent_chn_type]['total'] += float(system_recharge[1])
        return platform_system_recharge

    def create_platform_stats(self, aggregate_key):
        assert aggregate_key in ('chn_type', 'platform_id',)
        platform_stats = agent_mg.daily_agent_split_stats.aggregate([{
            '$match': {
                'day': self.today_str,
                'agent_level': {"$gte": 2},
                'role': {'$ne': 0}
            }}, {
            '$group': {
                '_id': '$' + aggregate_key,
                'platform_id': {"$first": "$platform_id"},
                'total_active': {"$sum": '$extend.user.active_count'},
                'total_register': {"$sum": '$extend.user.register_count'},
                'total_recharge': {"$sum": '$extend.user.total_recharge'},
                'total_withdraw': {"$sum": '$extend.user.total_withdraw'},
                'total_profit': {'$sum': {'$subtract': [{"$ifNull": ['$extend.user.total_recharge', 0]},
                                                        {"$ifNull": ['$extend.user.total_withdraw', 0]}]}},
                'total_tax': {'$sum': '$user_tax'},
                'total_reward': {'$sum': '$user_reward'},
            }}
        ])
        for each_stats in platform_stats:
            if each_stats['_id'] is None:
                continue
            each_stats['day'] = self.today_str
            each_stats['platform_id'] = each_stats['platform_id']
            each_stats['platform_type'] = self.platform_conf.get(each_stats['platform_id'], {}).get('name')
            each_stats['platform_name'] = self.platform_conf.get(each_stats['platform_id'], {}).get('label')
            if aggregate_key == 'chn_type':
                stats_id = '-'.join([self.today_str, each_stats['platform_type'], str(each_stats['_id'])])
                each_stats['chn_type'] = each_stats['_id']
                each_stats['chn_type_name'] = self.platform_conf.get(each_stats['platform_id'], {}).get(
                    'chn_types', {}).get(each_stats['_id'], {}).get('chn_type_name', '-')
                each_stats['system_recharge'] = self.platform_system_recharge.get(
                    each_stats['platform_name'], {}).get(each_stats['chn_type'], {}).get('total', 0)
            else:
                stats_id = '-'.join([self.today_str, each_stats['platform_type']])
                each_stats['system_recharge'] = self.platform_system_recharge.get(
                    each_stats['platform_name'], {}).get('total', 0)
            each_stats['total_cost'] = float(each_stats['total_reward']) + float(
                convert_fen_to_yuan(each_stats['system_recharge']))
            each_stats.pop('_id')
            agent_mg.platform_stats.update_one({'_id': stats_id}, {'$set': each_stats}, upsert=True)

    def handle(self, **options):
        day = options['day'] or 2
        for _ in range(0, int(day)):
            self.day = self.day - timedelta(days=1)
            self.today_str = tz.utc_to_local(self.day).strftime('%Y-%m-%d')
            self.platform_system_recharge = self.calc_platform_system_recharge(self.today_str)
            for aggregate_key in self.aggs_key_list:
                self.create_platform_stats(aggregate_key)
