import logging
import hashlib
import json
import requests
from datetime import timedelta
from django.core.management.base import BaseCommand
from common.agent.db import get_agent_stats, get_children_id
from common.chn.db import get_platform_chn
from common.withdraw.db import get_today_withdraw_amount
from common.system_recharge.db import get_daily_system_recharge_amount
from common.utils.currency import convert_fen_to_yuan
from django.conf import settings
from common import agent_mg
from optparse import make_option
from common.utils import tz


_LOGGER = logging.getLogger('agent')


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option(
            "-d",
            "--day",
            dest="day",
            help="day shift",
            metavar="day"
        ),
    )

    def __init__(self):
        super(Command, self).__init__()
        self.platform_conf = get_platform_chn()
        self.day = tz.get_utc_date() + timedelta(hours=8)
        self.day_str = tz.utc_to_local(self.day).strftime('%Y-%m-%d')
        self.financial_report_from_maestro = dict()

    def add_arguments(self, parser):
        parser.add_argument("--day", dest='day', help='please input a number....')

    @staticmethod
    def generate_sign(parameter, key):
        s = ''
        for k in sorted(parameter.keys()):
            s += '%s=%s&' % (k, parameter[k])
        s += 'key=%s' % key
        m = hashlib.md5()
        m.update(s.encode('utf8'))
        sign = m.hexdigest().upper()
        return sign

    def request_data_from_maestro(self, platform_id):
        index = settings.MAESTRO_PLATFORM_MAPPER[platform_id]
        time_range = [str(self.day), str(self.day + timedelta(days=1))]
        params = dict(time_range=json.dumps(time_range))
        params['key'] = 'friday'
        params['sign'] = 'maestro_secret'
        api_url = settings.MAESTRO_GET_FINANCIAL_REPORT.format(index)
        res = requests.get(api_url, params=params, timeout=60)
        res_json = json.loads(res.content)
        self.financial_report_from_maestro[platform_id] = {e['_chn']: e for e in res_json['data']}

    def get_field_from_maestro_report(self, platform_id, chn_names, field_name):
        assert field_name in ('recharge_new_user', 'recharge_new_amount', 'recharge_user',
                              'first_recharge_user', 'first_recharge_amount')
        field_value = 0
        for chn in chn_names:
            channel = [chn, 'ios_'+chn]
            field_value += self.financial_report_from_maestro.get(
                platform_id, {}).get(channel[0], {}).get(field_name, 0) + self.financial_report_from_maestro.get(
                platform_id, {}).get(channel[1], {}).get(field_name, 0) if channel else 0
        return field_value

    def create_financial_stats(self, user_stats):
        user_id = user_stats['agent_id']
        children_id = get_children_id(user_id)
        agent_stats = get_agent_stats(user_id)
        platform_id = user_stats.get('platform_id', '') or agent_stats.get('platform_id')
        chn_type = user_stats.get('chn_type', '') or agent_stats.get('chn_type')
        children_chn = agent_mg.chn_pool.find({'agent_id': {"$in": children_id + [user_id]},
                                               'chn_type': chn_type})
        children_chn_names = [e['chn'] for e in children_chn]
        stats_id = '-'.join([user_stats['_id'], str(platform_id)])
        user_financial_stats = dict(
            agent_id=user_stats['agent_id'],
            day=user_stats['day'],
            platform_id=platform_id,
            platform_name=self.platform_conf.get(platform_id, {}).get('label'),
            chn_type=chn_type,
            chn_type_name=self.platform_conf.get(platform_id, {}).get(
                'chn_types', {}).get(chn_type, {}).get('chn_type_name', '-'),
            agent_level=user_stats['agent_level'],
            total_register=user_stats.get('extend', {}).get('total', {}).get('register_count', 0),
            total_active=user_stats.get('extend', {}).get('total', {}).get('active_count', 0),
            total_recharge=user_stats.get('extend', {}).get('total', {}).get('total_recharge', 0),
            total_withdraw=user_stats.get('extend', {}).get('total', {}).get('total_withdraw', 0),
            total_profit=(user_stats.get('extend', {}).get('total', {}).get('total_recharge', 0) -
                          user_stats.get('extend', {}).get('total', {}).get('total_withdraw', 0)),
            recharge_new_user=self.get_field_from_maestro_report(platform_id, children_chn_names, 'recharge_new_user'),
            recharge_new_amount=self.get_field_from_maestro_report(platform_id, children_chn_names,
                                                                   'recharge_new_amount'),
            recharge_user=self.get_field_from_maestro_report(platform_id, children_chn_names, 'recharge_user'),
            first_recharge_user=self.get_field_from_maestro_report(platform_id, children_chn_names,
                                                                   'first_recharge_user'),
            first_recharge_amount=self.get_field_from_maestro_report(platform_id, children_chn_names,
                                                                     'first_recharge_amount'),
            total_tax=user_stats.get('total_tax', 0),
            agent_withdraw=float(convert_fen_to_yuan(
                get_today_withdraw_amount(user_stats['agent_id'], self.day_str)
            )),
            system_recharge=float(convert_fen_to_yuan(
                get_daily_system_recharge_amount(user_stats['agent_id'], self.day_str)
            )),
            commission_rate=user_stats.get('commission_rate', 0)
        )
        group_stats = agent_mg.daily_agent_split_stats.aggregate([
            {"$match": {'day': self.day_str, 'path': user_stats['agent_id'], 'chn_type': chn_type,
                        'agent_level': {"$gte": 3}}},
            {"$group": {"_id": None, "total_reward": {"$sum": "$total_reward"}}}
        ])
        group_stats = group_stats.next() if group_stats.alive else {}
        user_financial_stats['total_reward'] = group_stats.get('total_reward', 0)
        user_financial_stats['total_cost'] = user_financial_stats['total_reward']
        agent_mg.financial_stats.update_one({'_id': stats_id}, {'$set': user_financial_stats}, upsert=True)

    def handle(self, **options):
        day = options['day'] or 1
        for i in range(0, int(day)):
            self.day = tz.get_utc_date() + timedelta(hours=8) - timedelta(days=i)
            self.day_str = tz.utc_to_local(self.day).strftime('%Y-%m-%d')
            self.financial_report_from_maestro = dict()
            for platform_id in self.platform_conf:
                self.request_data_from_maestro(platform_id)
            print(self.day_str)
            daily_agent_stats = agent_mg.daily_agent_split_stats.find({'day': self.day_str})
            for each_stats in daily_agent_stats:
                self.create_financial_stats(each_stats)
