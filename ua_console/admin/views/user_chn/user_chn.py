# -*- coding: utf-8 -*-
import logging
from django.views.decorators.http import require_POST, require_GET
from common.user_chn import handler
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from admin.views.agent import agent_form


_LOGGER = logging.getLogger('agent')


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.ManualAssignChannel)
def manual_assign_chn(req, cleaned_data):
    operator_id = req.user_id
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    agent_id = cleaned_data['agent_id']
    user_id = cleaned_data['user_id']
    bind_chn = cleaned_data['bind_chn']
    note = cleaned_data['note'] or ''
    return handler.manual_assign_chn(platform_id=platform_id,
                                     chn_type=chn_type,
                                     agent_id=agent_id,
                                     user_id=user_id,
                                     bind_chn=bind_chn,
                                     operator_id=operator_id,
                                     note=note)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.ListManualAssignChannel)
def list_manual_assign_chn(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    agent_id = cleaned_data['agent_id']
    user_id = cleaned_data['user_id']
    bind_chn = cleaned_data['bind_chn']
    agent_level = cleaned_data['agent_level']
    return handler.list_manual_assign_chn(platform_id, chn_type, bind_chn, user_id, agent_id, agent_level)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.SearchUserChannelField)
def get_user_chn_info(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    user_id = cleaned_data['user_id']
    return handler.get_user_chn_info(platform_id, user_id)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetIosChannelField)
def get_ios_chn(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    return handler.get_ios_chn(platform_id, chn_type)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetIosChannelField)
def fix_agent_stats(req, cleaned_data):

    pass

