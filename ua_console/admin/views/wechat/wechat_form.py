from django import forms


class SubmitWechatForm(forms.Form):
    item_id = forms.CharField()
    wechat_list = forms.CharField()
    platform_id = forms.IntegerField()
    description = forms.CharField(required=False)


class DeleteWechatForm(forms.Form):
    item_id = forms.CharField()
