# -*- coding: utf-8 -*-
import time
from django.views.decorators.http import require_GET, require_POST
from common import agent_mg
from common.utils.decorator import response_wrapper, validate_form
from common.utils import tz
from common.utils import exceptions as err
from common.utils.api import token_required
from common.chn.db import get_platform_chn
from common.utils.types import Enum
from bson.objectid import ObjectId
from admin.views.wechat import wechat_form

WECHAT_STATUS = Enum({
    "DEACTIVATED": (0, u"未使用"),
    "ACTIVATED": (1, u"使用中")
})


@require_GET
@response_wrapper
@token_required
def fetch_wechat(req):
    w_list = list()
    total_count = agent_mg.agent_wechat.count({})
    items = agent_mg.agent_wechat.find({})
    for item in items:
        agent = agent_mg.agent_stats.find_one({'_id': item['agent_id']}) or {}
        w_list.append(dict(
            _id=item['_id'],
            agent_id=item['agent_id'],
            username=agent.get('username'),
            platform_id=item.get('platform_id'),
            wechat_list=item['wechat_list'],
            updated_at=tz.ts_to_local_datetime_str(item['ts']),
            description=item.get('description')
        ))
    return dict(list=w_list, total_count=total_count)


@require_POST
@response_wrapper
@token_required
@validate_form(wechat_form.SubmitWechatForm)
def submit_wechat(req, cleaned_data):
    wechat_item_id = cleaned_data['item_id']
    wechat_list = cleaned_data['wechat_list']
    description = cleaned_data['description']
    platform_id = cleaned_data['platform_id']
    if platform_id not in get_platform_chn().keys() + [-1]:
        raise err.ParamError(u'商户不存在')
    to_set = dict(
        platform_id=platform_id,
        status=WECHAT_STATUS.ACTIVATED if platform_id != -1 else WECHAT_STATUS.DEACTIVATED,
        wechat_list=wechat_list.split(','),
        ts=int(time.time()),
        description=description
    )
    agent_mg.agent_wechat.update({'_id': ObjectId(wechat_item_id)},
                                 {'$set': to_set}, upsert=True)
    return dict()


@require_POST
@response_wrapper
@token_required
@validate_form(wechat_form.DeleteWechatForm)
def delete_wechat(req, cleaned_data):
    item_id = cleaned_data['item_id']
    agent_mg.agent_wechat.delete_one({'_id': ObjectId(item_id)})
    return dict()


