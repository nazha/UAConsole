import json
import logging
from django import forms
from common.chn.db import get_platform_chn
from common.utils.custom_form_field import JsonField, DateTimeStringField

_LOGGER = logging.getLogger('agent')


class AgentAlipayJsonField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(AgentAlipayJsonField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            try:
                value = json.loads(value)
            except:
                pass
            assert isinstance(value, dict)
            assert 'name' in value and 'alipay_no' in value
        except Exception as e:
            _LOGGER.info('test {}'.format(e))
            raise forms.ValidationError("Invalid data in AgentAlipayJsonField")
        return value


class AgentUnionJsonField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(AgentUnionJsonField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            try:
                value = json.loads(value)
            except:
                pass
            assert isinstance(value, dict)
            assert ('name' in value and
                    'bank_name' in value and
                    'bank_no' in value and
                    'branch' in value and
                    'region' in value)
        except:
            raise forms.ValidationError("Invalid data in AgentAlipayJsonField")
        return value


class ActivatedPlatformField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(ActivatedPlatformField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            try:
                value = json.loads(value)
            except:
                pass
            platform_chn = get_platform_chn()
            platform_chn = {v['name']: {'chn_types': v['chn_types'], 'id': k} for k, v in platform_chn.iteritems()}
            for platform_name, chn_type_list in value.iteritems():
                assert platform_name in platform_chn
                assert isinstance(chn_type_list, list)
                valid_chn_type_name = [chn_type for chn_type in platform_chn[platform_name]['chn_types']]
                for chn_type in chn_type_list:
                    assert chn_type in valid_chn_type_name
        except:
            raise forms.ValidationError("Invalid data in ActivatedPlatformField")
        return value


class LimitCommissionRateField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(LimitCommissionRateField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return {}
            try:
                value = json.loads(value)
            except:
                pass
            platform_chn = get_platform_chn()
            platform_chn = {v['name']: {'chn_types': v['chn_types'], 'id': k} for k, v in platform_chn.iteritems()}
            for platform_name, chn_type_item in value.iteritems():
                assert platform_name in platform_chn
                assert isinstance(chn_type_item, dict)
                valid_chn_type_name = [chn_type for chn_type in platform_chn[platform_name]['chn_types']]
                for chn_type_name, limit_commission_rate in chn_type_item.iteritems():
                    assert chn_type_name in valid_chn_type_name
                    assert isinstance(limit_commission_rate, int)
                    assert 0 <= limit_commission_rate <= 100
        except:
            raise forms.ValidationError("Invalid data in ActivatedPlatformField")
        return value




class ListAgentForm(forms.Form):
    id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    agent_level = forms.IntegerField(required=False)
    nickname = forms.CharField(required=False)
    role = forms.IntegerField(required=False)
    bind_phone = forms.CharField(required=False)
    root_id = forms.IntegerField(required=False)
    chn = forms.CharField(required=False)
    bind_union = forms.IntegerField(required=False)
    bind_alipay = forms.IntegerField(required=False)
    bankcard_no = forms.CharField(required=False)
    alipay_no = forms.CharField(required=False)
    register_start_time = DateTimeStringField(required=False)
    register_end_time = DateTimeStringField(required=False)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetAgentForm(forms.Form):
    id = forms.IntegerField(required=False)
    username = forms.CharField(required=False)
    nickname = forms.CharField(required=False)


class CreateAgentForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()
    nickname = forms.CharField()
    disable_create_agent = forms.IntegerField(required=False)
    commission_rate = forms.IntegerField()
    block_phone = forms.IntegerField(required=False)
    biz_model = forms.CharField()
    activate_chn_type = ActivatedPlatformField(required=False)
    disable_create_agent = forms.IntegerField(required=False)


class UpdateAgentForm(forms.Form):
    password = forms.CharField(required=False)
    nickname = forms.CharField(required=False)
    role = forms.IntegerField(required=False)
    commission_rate = forms.IntegerField(required=False)
    limit_custom_commission_rate = forms.IntegerField(required=False)
    block_phone = forms.IntegerField(required=False)
    bind_ip = forms.CharField(required=False)
    bind_phone = forms.CharField(required=False)
    bind_alipay = AgentAlipayJsonField(required=False)
    bind_union = AgentUnionJsonField(required=False)
    activate_chn_type = ActivatedPlatformField(required=False)
    reason = forms.CharField(required=False)


class ListTeamLeaderForm(forms.Form):
    root_id = forms.IntegerField(required=False)
    team_leader_id = forms.IntegerField(required=False)
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class ListRootForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class UpdateRootAgentForm(forms.Form):
    password = forms.CharField(required=False)
    disable_create_agent = forms.IntegerField(required=False)
    activate_chn_type = ActivatedPlatformField()
    block_phone = forms.IntegerField()
    commission_rate = forms.IntegerField()
    nickname = forms.CharField()


class UpdateTeamLeaderForm(forms.Form):
    disable_create_agent = forms.IntegerField()
    activate_chn_type = ActivatedPlatformField()
    limit_commission_rate = LimitCommissionRateField()


class ListLoginRecordForm(forms.Form):
    user_id = forms.IntegerField(required=False)
    username = forms.CharField(required=False)
    ip = forms.CharField(required=False)
    status = forms.IntegerField(required=False)
    order_by = forms.CharField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetDeactivatedUserListForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    root_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetDeactivateHistoryListForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    root_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    start_time = DateTimeStringField(required=False, to_date=True)
    end_time = DateTimeStringField(required=False, to_date=True)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetDeactivatedRecordListForm(forms.Form):
    agent_id = forms.IntegerField()


class ListCustomServiceForm(forms.Form):
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    agent_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    root_id = forms.IntegerField(required=False)
    disable_create_agent = forms.IntegerField(required=False)
    disable_apply_agent = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class UpdateCustomServicePermissionForm(forms.Form):
    disable_create_agent = forms.IntegerField()
    disable_apply_agent = forms.IntegerField()
    qq_account = forms.CharField(required=False)
    commission_rate = forms.IntegerField(required=False)
    wechat_account = forms.CharField(required=False)
    commission_rate = forms.IntegerField(required=False)
    activate_chn_type = ActivatedPlatformField()
    on_promote_chn_type = JsonField()


class ListApplyUserRegisterForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class VerifyChildAgentApplicationForm(forms.Form):
    agent_id = forms.IntegerField()
    application_form_id = forms.CharField()
    verification = forms.IntegerField()
    commission_rate = forms.IntegerField(required=False)
    note = forms.CharField(required=False)


class ListRegularAgentForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    root_id = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class ActivateChnForm(forms.Form):
    agent_id = forms.IntegerField()
    platform_id = forms.IntegerField()
    chn_type = forms.CharField()


class GetParentMarketingSetUpForm(forms.Form):
    agent_id = forms.IntegerField()


class ManualAssignChannel(forms.Form):
    platform_id = forms.IntegerField()
    chn_type = forms.CharField()
    agent_id = forms.IntegerField()
    user_id = forms.IntegerField()
    bind_chn = forms.CharField(max_length=64)
    note = forms.CharField(required=False)


class ListManualAssignChannel(forms.Form):
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    agent_id = forms.IntegerField(required=False)
    user_id = forms.IntegerField(required=False)
    bind_chn = forms.CharField(required=False)
    agent_level = forms.IntegerField(required=False)


class SearchUserChannelField(forms.Form):
    platform_id = forms.IntegerField()
    user_id = forms.IntegerField()


class GetIosChannelField(forms.Form):
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
