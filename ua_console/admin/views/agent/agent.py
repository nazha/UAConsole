# -*- coding: utf-8 -*-
import re
import logging
from django.views.decorators.http import require_POST, require_GET
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from common.agent import handler
from common.agent.module.root_agent_creator import RootAgentCreator
from common.utils.decorator import response_wrapper, validate_form
from common.utils import exceptions as err
from common.utils.api import token_required
from admin.views.agent import agent_form

_LOGGER = logging.getLogger('agent')


class AgentView(TemplateView):
    @method_decorator(token_required)
    @method_decorator(validate_form(agent_form.ListAgentForm))
    def get(self, req, cleaned_data, *args, **kwargs):
        agent_id = cleaned_data['id']
        parent_id = cleaned_data['parent_id']
        agent_level = cleaned_data['agent_level']
        nickname = cleaned_data['nickname']
        role = cleaned_data['role']
        bind_phone = cleaned_data['bind_phone']
        root_id = cleaned_data['root_id']
        channel = cleaned_data['chn']
        bind_union = cleaned_data['bind_union']
        bind_alipay = cleaned_data['bind_alipay']
        bankcard_no = cleaned_data['bankcard_no']
        alipay_no = cleaned_data['alipay_no']
        register_start_time = cleaned_data['register_start_time']
        register_end_time = cleaned_data['register_end_time']
        to_export_file = cleaned_data['to_export_file']
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_agent(admin_id=req.user_id,
                                  agent_id=agent_id,
                                  parent_id=parent_id,
                                  nickname=nickname,
                                  agent_level=agent_level,
                                  role=role,
                                  bind_phone=bind_phone,
                                  root_id=root_id,
                                  channel=channel,
                                  bind_union=bind_union,
                                  bind_alipay=bind_alipay,
                                  bankcard_no=bankcard_no,
                                  alipay_no=alipay_no,
                                  register_start_time=register_start_time,
                                  register_end_time=register_end_time,
                                  to_export_file=to_export_file,
                                  page=page,
                                  size=size)

    @method_decorator(response_wrapper)
    def dispatch(self, *args, **kwargs):
        return super(AgentView, self).dispatch(*args, **kwargs)


class SingleAgentView(TemplateView):
    @method_decorator(validate_form(agent_form.GetAgentForm))
    def get(self, req, user_id, cleaned_data):
        username = cleaned_data['username']
        nickname = cleaned_data['nickname']
        return handler.get_agent(admin_id=req.user_id,
                                 user_id=long(user_id),
                                 username=username,
                                 nickname=nickname)

    @method_decorator(validate_form(agent_form.UpdateAgentForm))
    def post(self, req, user_id, cleaned_data):
        password = cleaned_data['password']
        nickname = cleaned_data['nickname']
        role = cleaned_data['role']
        commission_rate = cleaned_data['commission_rate']
        block_phone = cleaned_data['block_phone']
        bind_ip = cleaned_data['bind_ip']
        bind_phone = cleaned_data['bind_phone']
        bind_alipay = cleaned_data['bind_alipay']
        bind_union = cleaned_data['bind_union']
        activate_chn_type = cleaned_data['activate_chn_type']
        reason = cleaned_data['reason']
        handler.update_agent(int(user_id), int(req.user_id), nickname=nickname,
                             password=password, bind_phone=bind_phone, bind_ip=bind_ip,
                             bind_alipay=bind_alipay, bind_union=bind_union, role=role,
                             deactivated_reason=reason, commission_rate=commission_rate,
                             block_phone=block_phone, activate_chn_type=activate_chn_type)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleAgentView, self).dispatch(*args, **kwargs)


def validate_chars(string):
    m = re.match(ur'^[a-zA-Z0-9_]{1,50}$', string)
    if m is None:
        raise err.ParamError(u'用户名与密码只能使用数字字母')
    return string


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.CreateAgentForm)
def create_root_agent(req, cleaned_data):
    username = validate_chars(cleaned_data['username'])
    password = validate_chars(cleaned_data['password'])
    nickname = cleaned_data['nickname']
    disable_create_agent = cleaned_data['disable_create_agent']
    commission_rate = cleaned_data['commission_rate']
    biz_model = cleaned_data['biz_model']
    activated_platform = cleaned_data['activate_chn_type']
    block_phone = cleaned_data['block_phone']
    disable_create_agent = cleaned_data['disable_create_agent']
    return handler.create_root_agent(username=username,
                                     password=password,
                                     nickname=nickname,
                                     commission_rate=commission_rate,
                                     biz_model=biz_model,
                                     activated_platform=activated_platform,
                                     block_phone=block_phone,
                                     disable_create_agent=disable_create_agent)


@require_GET
@response_wrapper
@token_required
def get_create_root_preset(req):
    return RootAgentCreator.get_create_root_preset()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListLoginRecordForm)
def login_record(req, cleaned_data):
    user_id = cleaned_data['user_id']
    username = cleaned_data['username']
    ip = cleaned_data['ip']
    status = cleaned_data['status']
    order_by = cleaned_data['order_by']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.get_login_record(user_id=user_id,
                                    username=username,
                                    ip=ip,
                                    status=status,
                                    order_by=order_by,
                                    page=page, size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetDeactivatedUserListForm)
def get_deactivated_user_list(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    root_id = cleaned_data['root_id']
    parent_id = cleaned_data['parent_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.get_deactivated_user_list(agent_id=agent_id,
                                             root_id=root_id,
                                             parent_id=parent_id,
                                             platform_id=platform_id,
                                             chn_type=chn_type,
                                             page=page, size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetDeactivateHistoryListForm)
def get_deactivate_history_list(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    root_id = cleaned_data['root_id']
    parent_id = cleaned_data['parent_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    start_time = cleaned_data['start_time']
    end_time = cleaned_data['end_time']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.get_deactivate_history_list(agent_id=agent_id,
                                               root_id=root_id,
                                               parent_id=parent_id,
                                               platform_id=platform_id,
                                               chn_type=chn_type,
                                               start_time=start_time,
                                               end_time=end_time,
                                               page=page, size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetDeactivatedRecordListForm)
def get_deactivate_record_list(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    return handler.get_deactivate_record_list(agent_id=agent_id)


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListRootForm)
def list_root_agent(req, cleaned_data):
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_root_agent(page=page, size=size)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.UpdateRootAgentForm)
def update_root_agent(req, agent_id, cleaned_data):
    disable_create_agent = cleaned_data['disable_create_agent']
    password = cleaned_data['password']
    activate_chn_type = cleaned_data['activate_chn_type']
    block_phone = cleaned_data['block_phone']
    commission_rate = cleaned_data['commission_rate']
    nickname = cleaned_data['nickname']
    handler.update_agent(agent_id=int(agent_id),
                         operator_id=int(req.user_id),
                         password=password,
                         activate_chn_type=activate_chn_type,
                         block_phone=block_phone,
                         commission_rate=commission_rate,
                         nickname=nickname,
                         disable_create_agent=disable_create_agent)
    return dict()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListTeamLeaderForm)
def list_team_leader(req, cleaned_data):
    team_leader_id = cleaned_data['team_leader_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    root_id = cleaned_data['root_id']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_team_leader(team_leader_id_filter=team_leader_id,
                                    root_id_filter=root_id,
                                    platform_id_filter=platform_id,
                                    chn_type_filter=chn_type,
                                    page=page, size=size)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.UpdateTeamLeaderForm)
def update_team_leader(req, cleaned_data, agent_id):
    disable_create_agent = cleaned_data['disable_create_agent']
    activate_chn_type = cleaned_data['activate_chn_type']
    limit_commission_rate = cleaned_data['limit_commission_rate']
    handler.update_agent(agent_id=int(agent_id),
                         operator_id=int(req.user_id),
                         disable_create_agent=disable_create_agent,
                         activate_chn_type=activate_chn_type,
                         limit_commission_rate=limit_commission_rate)
    return dict()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListCustomServiceForm)
def list_custom_service(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    agent_id = cleaned_data['agent_id']
    parent_id = cleaned_data['parent_id']
    root_id = cleaned_data['root_id']
    disable_create_agent = cleaned_data['disable_create_agent']
    disable_apply_agent = cleaned_data['disable_apply_agent']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_custom_service(platform_id=platform_id,
                                       chn_type=chn_type,
                                       agent_id=agent_id,
                                       parent_id=parent_id,
                                       root_id=root_id,
                                       disable_create_agent=disable_create_agent,
                                       disable_apply_agent=disable_apply_agent,
                                       page=page,
                                       size=size)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.UpdateCustomServicePermissionForm)
def update_custom_service(req, agent_id, cleaned_data):
    disable_create_agent = cleaned_data['disable_create_agent']
    disable_apply_agent = cleaned_data['disable_apply_agent']
    qq_account = cleaned_data['qq_account']
    wechat_account = cleaned_data['wechat_account']
    commission_rate = cleaned_data['commission_rate']
    activate_chn_type = cleaned_data['activate_chn_type']
    on_promote_chn_type = cleaned_data['on_promote_chn_type']
    handler.update_agent(agent_id=int(agent_id),
                         operator_id=int(req.user_id),
                         commission_rate=commission_rate,
                         disable_create_agent=disable_create_agent,
                         disable_apply_agent=disable_apply_agent,
                         qq_contact_account=qq_account,
                         wechat_contact_account=wechat_account,
                         activate_chn_type=activate_chn_type,
                         on_promote_chn_type=on_promote_chn_type)
    return dict()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListRegularAgentForm)
def list_regular_agent(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    parent_id = cleaned_data['parent_id']
    root_id = cleaned_data['root_id']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_regular_agent(agent_id=agent_id,
                                      parent_id=parent_id,
                                      root_id=root_id,
                                      page=page,
                                      size=size)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.ActivateChnForm)
def activate_chn_resource(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    handler.activate_chn_resource(agent_id, platform_id, chn_type)
    return dict()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.ListApplyUserRegisterForm)
def list_child_agent_application_form(req, agent_id, cleaned_data):
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_child_agent_application_form(int(agent_id), page, size)


@require_POST
@response_wrapper
@token_required
@validate_form(agent_form.VerifyChildAgentApplicationForm)
def verify_child_agent_application_form(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    application_form_id = cleaned_data['application_form_id']
    verification = cleaned_data['verification']
    commission_rate = cleaned_data['commission_rate']
    note = cleaned_data['note']
    handler.verify_child_agent_application_form(agent_id, application_form_id, verification,
                                                commission_rate, note)
    return dict()


@require_GET
@response_wrapper
@token_required
@validate_form(agent_form.GetParentMarketingSetUpForm)
def get_parent_activated_platform(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    return handler.get_parent_activated_platform(agent_id)
