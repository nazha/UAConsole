# -*- coding: utf-8 -*-
import logging
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from common.qa import handler
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from admin.views.qa import qa_form

_LOGGER = logging.getLogger('agent')


class QAView(TemplateView):
    @method_decorator(validate_form(qa_form.ListQAForm))
    def get(self, req, cleaned_data, *args, **kwargs):
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_qa(page=page, size=size)

    @method_decorator(validate_form(qa_form.CreateQAForm))
    def post(self, req, cleaned_data):
        qa_type = cleaned_data['qa_type']
        question = cleaned_data['question']
        answer = cleaned_data['answer']
        handler.create_qa(qa_type=qa_type,
                          question=question,
                          answer=answer)
        return dict()

    @method_decorator(token_required)
    @method_decorator(response_wrapper)
    def dispatch(self, *args, **kwargs):
        return super(QAView, self).dispatch(*args, **kwargs)


class SingleQAView(TemplateView):
    def get(self, req, qa_id):
        return handler.get_qa(long(qa_id))

    @method_decorator(validate_form(qa_form.UpdateQAForm))
    def put(self, req, qa_id, cleaned_data):
        qa_type = cleaned_data['qa_type']
        question = cleaned_data['question']
        answer = cleaned_data['answer']
        handler.update_qa(qa_id=long(qa_id),
                          qa_type=qa_type,
                          question=question,
                          answer=answer)
        return dict()

    def delete(self, req, qa_id):
        handler.delete_qa(qa_id)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleQAView, self).dispatch(*args, **kwargs)
