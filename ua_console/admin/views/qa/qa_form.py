from django import forms


class ListQAForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class CreateQAForm(forms.Form):
    qa_type = forms.IntegerField()
    question = forms.CharField()
    answer = forms.CharField()


class UpdateQAForm(forms.Form):
    qa_type = forms.IntegerField()
    question = forms.CharField()
    answer = forms.CharField()


