# -*- coding: utf-8 -*-
import logging
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from common.announce import handler
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from common.utils import exceptions as err
from admin.views.announce import announce_form

_LOGGER = logging.getLogger('agent')


class AnnounceView(TemplateView):
    @method_decorator(validate_form(announce_form.ListAnnounceForm))
    def get(self, req, cleaned_data, *args, **kwargs):
        start_time = cleaned_data['start_time']
        end_time = cleaned_data['end_time']
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_announcement(start_time=start_time,
                                         end_time=end_time,
                                         page=page,
                                         size=size)

    @staticmethod
    def verify_time_interval(start_time, end_time):
        if start_time and end_time and start_time > end_time:
            raise err.ParamError(u'开始时间大于结束时间')

    @method_decorator(validate_form(announce_form.CreateAnnounceForm))
    def post(self, req, cleaned_data):
        title = cleaned_data['title']
        content = cleaned_data['content']
        start_time = cleaned_data['start_time']
        end_time = cleaned_data['end_time'] if cleaned_data['end_time'] else '2025-01-01 00:00:00'
        self.verify_time_interval(start_time, end_time)
        handler.create_announcement(title=title,
                                    content=content,
                                    start_time=start_time,
                                    end_time=end_time)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(AnnounceView, self).dispatch(*args, **kwargs)


class SingleAnnounceView(TemplateView):
    @method_decorator(validate_form(announce_form.UpdateAnnounceForm))
    def post(self, req, announcement_id, cleaned_data):
        title = cleaned_data['title']
        content = cleaned_data['content']
        start_time = cleaned_data['start_time']
        end_time = cleaned_data['end_time'] if cleaned_data['end_time'] else '2025-01-01 00:00:00'
        handler.update_announce(announcement_id=int(announcement_id),
                                title=title,
                                content=content,
                                start_time=start_time,
                                end_time=end_time)
        return dict()

    def delete(self, req, announcement_id):
        handler.delete_announcement(announcement_id=announcement_id)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleAnnounceView, self).dispatch(*args, **kwargs)
