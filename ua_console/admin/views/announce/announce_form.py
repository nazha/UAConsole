from django import forms
from common.utils.custom_form_field import DateTimeStringField


class ListAnnounceForm(forms.Form):
    start_time = forms.DateTimeField(required=False)
    end_time = forms.DateTimeField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class CreateAnnounceForm(forms.Form):
    title = forms.CharField()
    content = forms.CharField()
    start_time = DateTimeStringField(to_utc=True)
    end_time = DateTimeStringField(required=False, to_utc=True)


class UpdateAnnounceForm(forms.Form):
    announce_type = forms.IntegerField()
    title = forms.CharField()
    content = forms.CharField()
    start_time = DateTimeStringField(to_utc=True)
    end_time = DateTimeStringField(required=False, to_utc=True)
