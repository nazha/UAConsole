# -*- coding: utf-8 -*-
import logging
from django import forms
from common.utils import exceptions as err
from common.utils.custom_form_field import DateTimeStringField, JsonArrayField

_LOGGER = logging.getLogger('agent')


class ListSystemRechargeForm(forms.Form):
    user_id = forms.IntegerField(required=False)
    created_at_start_time = DateTimeStringField(required=False, to_utc=True)
    created_at_end_time = DateTimeStringField(required=False, to_utc=True)
    date_start_time = DateTimeStringField(required=False, to_utc=True)
    date_end_time = DateTimeStringField(required=False, to_utc=True)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class CreateSystemRechargeForm(forms.Form):
    test = forms.IntegerField(required=False)
    detail_info = JsonArrayField()

    def clean(self):
        form_data = self.cleaned_data
        for each_system_recharge in form_data['detail_info']:
            for field, data_type in {'user_id': int, 'amount': float, 'note': unicode}.iteritems():
                try:
                    data_type(each_system_recharge[field])
                except:
                    raise err.ParamError(u'参数错误')
        return form_data
