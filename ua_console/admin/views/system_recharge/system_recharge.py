# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.views.decorators.http import require_POST
from django.utils.decorators import method_decorator
from common.system_recharge import handler
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from admin.views.system_recharge import system_recharge_form


class SysRechargeView(TemplateView):
    @method_decorator(validate_form(system_recharge_form.ListSystemRechargeForm))
    def get(self, req, cleaned_data):
        user_id = cleaned_data['user_id']
        created_at_start_time = cleaned_data['created_at_start_time']
        created_at_end_time = cleaned_data['created_at_end_time']
        date_start_time = cleaned_data['date_start_time']
        date_end_time = cleaned_data['date_end_time']
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_all_detail(user_id=user_id,
                                       created_at_start_time=created_at_start_time,
                                       created_at_end_time=created_at_end_time,
                                       date_start_time=date_start_time,
                                       date_end_time=date_end_time,
                                       page=page, size=size)

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SysRechargeView, self).dispatch(*args, **kwargs)


@require_POST
@response_wrapper
@token_required
@validate_form(system_recharge_form.CreateSystemRechargeForm)
def create_sys_recharge(req, trans_id, cleaned_data):
    user_id = req.user_id
    system_recharge_batch = cleaned_data['detail_info']
    handler.create_system_trans(trans_id=trans_id,
                                system_recharge_batch=system_recharge_batch,
                                operator_id=user_id)
    return dict()
