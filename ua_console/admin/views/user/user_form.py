import logging
from django.utils.encoding import smart_unicode
from django import forms

_LOGGER = logging.getLogger('agent')


class LoginForm(forms.Form):
    email = forms.CharField()
    password = forms.CharField()


class ListUsersForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)
    show_all_user = forms.IntegerField(required=False)


class CreateUserForm(forms.Form):
    email = forms.CharField()
    password = forms.CharField()
    nickname = forms.CharField()

    def clean(self):
        form_data = self.cleaned_data
        if form_data['nickname']:
            form_data['nickname'] = smart_unicode(form_data['nickname'])
        return form_data


class UpdateUserForm(forms.Form):
    password = forms.CharField(required=False)
    nickname = forms.CharField(required=False)
    role = forms.IntegerField(required=False)
    perm = forms.CharField(required=False)

    def clean(self):
        form_data = self.cleaned_data
        if form_data['nickname']:
            form_data['nickname'] = smart_unicode(form_data['nickname'])
        return form_data


class ListPermissionForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)
