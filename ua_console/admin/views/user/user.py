# -*- coding: utf-8 -*-
import logging
from django.views.decorators.http import require_POST
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from common.admin import handler
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from admin.views.user import user_form

_LOGGER = logging.getLogger('agent')


@require_POST
@response_wrapper
@validate_form(user_form.LoginForm)
def login(req, cleaned_data):
    email = cleaned_data['email']
    password = cleaned_data['password']
    _LOGGER.info('{} {} tt'.format(email, password))
    return handler.login_user(email, password)


@require_POST
@response_wrapper
@token_required
def logout(req):
    handler.logout_user(req.user_id, req.user.token)
    return dict()


class UserView(TemplateView):
    @method_decorator(token_required)
    @method_decorator(validate_form(user_form.ListUsersForm))
    def get(self, req, cleaned_data, *args, **kwargs):
        page = cleaned_data['page']
        size = cleaned_data['size']
        show_all_user = cleaned_data['show_all_user']
        return handler.list_users(req.user_role, show_all_user, page, size)

    @method_decorator(validate_form(user_form.CreateUserForm))
    def post(self, req, cleaned_data):
        email = cleaned_data['email']
        password = cleaned_data['password']
        nickname = cleaned_data['nickname']
        handler.create_user(email, password, nickname)
        _LOGGER.info('register user, email: {}, nickname: {}, password: {}'.format(email, nickname, password))
        return dict()

    @method_decorator(response_wrapper)
    def dispatch(self, *args, **kwargs):
        return super(UserView, self).dispatch(*args, **kwargs)


class SingleUserView(TemplateView):
    def get(self, req, user_id):
        return handler.get_user(long(user_id))

    @method_decorator(validate_form(user_form.UpdateUserForm))
    def put(self, req, user_id, cleaned_data):
        nickname = cleaned_data['nickname']
        password = cleaned_data['password']
        role = cleaned_data['role']
        perm = cleaned_data['perm']
        handler.update_user(req.user_id, int(user_id), nickname, password, role, perm)
        _LOGGER.info('update user: nickname: {}, password: {}, role: {}, perm: {}, by_user: {}'.format(
            nickname, password, role, perm, req.user_id))
        return dict()

    def delete(self, req, user_id):
        handler.delete_user(req.user_id, int(user_id))
        _LOGGER.info('delete user %s, by %s', user_id, req.user_id)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleUserView, self).dispatch(*args, **kwargs)


class PermissionView(TemplateView):
    @method_decorator(validate_form(user_form.ListPermissionForm))
    def get(self, req, cleaned_data):
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_perm(page, size)

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(PermissionView, self).dispatch(*args, **kwargs)
