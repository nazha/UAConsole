# -*- coding: utf-8 -*-
import json
import logging
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_POST
from common.withdraw import db as withdraw_db
from common.withdraw import handler
from common.utils import exceptions as err
from common.withdraw import unionagency_withdraw
from common.withdraw import just_withdraw
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from admin.views.withdraw import withdraw_form

_LOGGER = logging.getLogger(__name__)


class WithdrawView(TemplateView):
    @method_decorator(validate_form(withdraw_form.ListWithdrawForm))
    def get(self, req, cleaned_data):
        withdraw_id = cleaned_data['id']
        user_id = cleaned_data['user_id']
        target_type = cleaned_data['target_type']
        status = cleaned_data['status']
        check_status = cleaned_data['check_status']
        order_by = cleaned_data['order_by']
        created_at_start_time = cleaned_data['created_at_start_time']
        created_at_end_time = cleaned_data['created_at_end_time']
        updated_at_start_time = cleaned_data['updated_at_start_time']
        updated_at_end_time = cleaned_data['updated_at_end_time']
        to_export_file = cleaned_data['to_export_file']
        page = cleaned_data['page']
        size = cleaned_data['size']
        return handler.list_withdraw(withdraw_id=withdraw_id,
                                     user_id=user_id,
                                     target_type=target_type,
                                     status=status,
                                     check_status=check_status,
                                     order_by=order_by,
                                     created_at_start_time=created_at_start_time,
                                     created_at_end_time=created_at_end_time,
                                     updated_at_start_time=updated_at_start_time,
                                     updated_at_end_time=updated_at_end_time,
                                     to_export_file=to_export_file,
                                     page=page,
                                     size=size)

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(WithdrawView, self).dispatch(*args, **kwargs)


class SingleWithdrawView(TemplateView):
    def get(self, req, withdraw_id):
        return handler.get_withdraw(int(withdraw_id))

    def post(self, req, withdraw_id):
        data = json.loads(req.body)
        withdraw_db.update_withdraw_status(req.user_id, withdraw_id, data)
        return dict()

    @method_decorator(response_wrapper)
    @method_decorator(token_required)
    def dispatch(self, *args, **kwargs):
        return super(SingleWithdrawView, self).dispatch(*args, **kwargs)


@require_POST
@response_wrapper
@token_required
@validate_form(withdraw_form.CheckWithdrawForm)
def check_withdraw(req, withdraw_id, check_status, cleaned_data):
    reason = cleaned_data['reason']
    handler.check_withdraw(admin_id=req.user_id,
                           withdraw_id=int(withdraw_id),
                           check_status=int(check_status),
                           reason=reason)
    _LOGGER.info('check_withdraw succ, %s-%s-%s', req.user_id, withdraw_id, check_status)
    return dict()


@require_POST
@response_wrapper
@token_required
@validate_form(withdraw_form.BatchCheckWithdrawForm)
def batch_check_withdraw(req, cleaned_data):
    ids = cleaned_data['ids']
    check_status = cleaned_data['check_status']
    withdraw_db.batch_check_withdraws(admin_id=req.user_id,
                                      withdraw_ids=ids,
                                      check_status=check_status)
    return dict()


@require_POST
@response_wrapper
def union_withdraw_notify(request):
    try:
        unionagency_withdraw.check_notify_sign(request)
        return HttpResponse('success', status=200)
    except Exception as e:
        _LOGGER.exception('UnionAgency notify exception.(%s)' % e)
        return HttpResponse('failure', status=500)


@require_POST
@response_wrapper
def just_withdraw_notify(req):
    try:
        just_withdraw.check_just_withdraw_notify(req)
        return HttpResponse('success', status=200)
    except err.ParamError as e:
        _LOGGER.warn('just_withdraw notify param error, %s', e)
        return HttpResponse('success', status=200)
    except Exception as e:
        _LOGGER.exception('just_withdraw notify exception.(%s)' % e)
        return HttpResponse('failed', status=200)
