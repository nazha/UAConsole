import logging
from common.utils.custom_form_field import DateTimeStringField
from django import forms

_LOGGER = logging.getLogger('agent')


class IntegerListField(forms.Field):
    def __init__(self, required=True, *args, **kwargs):
        super(IntegerListField, self).__init__(*args, **kwargs)
        self.required = required

    def clean(self, value):
        try:
            if not self.required and not value:
                return []
            assert isinstance(value, list)
            value = [int(element) for element in value]
        except:
            raise forms.ValidationError("Invalid data in AgentAlipayJsonField")
        return value


class ListWithdrawForm(forms.Form):
    id = forms.IntegerField(required=False)
    user_id = forms.IntegerField(required=False)
    target_type = forms.IntegerField(required=False)
    status = forms.IntegerField(required=False)
    check_status = forms.IntegerField(required=False)
    order_by = forms.CharField(required=False)
    created_at_start_time = DateTimeStringField(required=False, to_utc=True)
    created_at_end_time = DateTimeStringField(required=False, to_utc=True)
    updated_at_start_time = DateTimeStringField(required=False)
    updated_at_end_time = DateTimeStringField(required=False)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class CheckWithdrawForm(forms.Form):
    reason = forms.CharField(required=False)


class BatchCheckWithdrawForm(forms.Form):
    ids = IntegerListField()
    check_status = forms.IntegerField()