from django import forms


class ListTemplateForm(forms.Form):
    platform = forms.CharField(required=False)
    chn_type = forms.CharField(required=False)


class DeleteTemplateForm(forms.Form):
    tm_id = forms.CharField()
