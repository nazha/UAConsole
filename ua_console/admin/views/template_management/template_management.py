# -*- coding: utf-8 -*-
from django.views.decorators.http import require_POST, require_GET
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from common.template_manage import handler
from common.utils import exceptions as err
from admin.views.template_management import template_form


@require_POST
@response_wrapper
@token_required
def set_list_template_manage(req):
    platform = req.POST.get('platform', '')
    chn_type = req.POST.get('chn_type', '')
    template_type = req.POST.get('template_type', '')
    weight = req.POST.get('weight', 0)
    tm_file = req.FILES.get('tm_file', None)
    if not tm_file:
        raise err.ParamError(u'请上传图片')
    handler.upload_template(tm_file=tm_file,
                            platform=platform,
                            chn_type=chn_type,
                            template_type=template_type,
                            weight=weight)
    return dict(upload='yes')


@require_GET
@response_wrapper
@token_required
@validate_form(template_form.ListTemplateForm)
def lists_template_manage(req, cleaned_data):
    platform = cleaned_data['platform']
    chn_type = cleaned_data['chn_type']
    return handler.list_template_manage(platform=platform,
                                        chn_type=chn_type)


@require_POST
@response_wrapper
@token_required
@validate_form(template_form.DeleteTemplateForm)
def delete_template_manage(req, cleaned_data):
    template_id = cleaned_data['tm_id']
    handler.delete_template_manage(template_id=template_id)
    return dict()
