# -*- coding: utf-8 -*-
import logging
from django.views.decorators.http import require_GET, require_POST
from common.utils.decorator import response_wrapper, validate_form
from common.chn import db as chn_db
from common.chn import handler as chn_handler
from common.chn.creator import create_resource, batch_recreate_resource
from common.utils.api import token_required
from admin.views.resource import resource_form

_LOGGER = logging.getLogger('agent')


@require_GET
@response_wrapper
@token_required
def get_platform_conf(req):
    return chn_db.get_platform_chn()


@require_POST
@response_wrapper
@validate_form(resource_form.CreateChnResourceForm)
def create_chn_resource(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    qr_link = cleaned_data['qr_link']
    android_link = cleaned_data['android_link']
    ios_link = cleaned_data['ios_link']
    chn_range = cleaned_data['range']
    create_resource(project_id=platform_id,
                    chn_type=chn_type,
                    qr_link_template=qr_link,
                    ios_link=ios_link,
                    android_link=android_link,
                    chn_range=chn_range)
    return dict()


@require_POST
@response_wrapper
@validate_form(resource_form.RecreateChnResourceForm)
def recreate_chn_resource(req, cleaned_data):
    obj_ids = cleaned_data['ids']
    to_create_poster = cleaned_data['to_create_poster']
    batch_recreate_resource(chn_obj_ids=obj_ids, to_create_poster=to_create_poster)
    return dict()


@require_GET
@response_wrapper
@validate_form(resource_form.ListChnForm)
def get_chn_resource(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    chn = cleaned_data['chn']
    status = cleaned_data['status']
    agent_level = cleaned_data['agent_level']
    root_id = cleaned_data['root_id']
    bind_at_start_time = cleaned_data['bind_at_start_time']
    bind_at_end_time = cleaned_data['bind_at_end_time']
    created_at_start_time = cleaned_data['created_at_start_time']
    created_at_end_time = cleaned_data['created_at_end_time']
    updated_at_start_time = cleaned_data['updated_at_start_time']
    updated_at_end_time = cleaned_data['updated_at_end_time']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return chn_db.get_chn_resource(status=status,
                                   agent_level=agent_level,
                                   root_id=root_id,
                                   bind_at_start_time=bind_at_start_time,
                                   bind_at_end_time=bind_at_end_time,
                                   created_at_start_time=created_at_start_time,
                                   created_at_end_time=created_at_end_time,
                                   updated_at_start_time=updated_at_start_time,
                                   updated_at_end_time=updated_at_end_time,
                                   project_id=platform_id,
                                   chn_type=chn_type,
                                   chn=chn,
                                   page=page,
                                   size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(resource_form.GetRootChnDistributionForm)
def get_root_chn_distribution(req, cleaned_data):
    username = cleaned_data['username']
    platform_name = cleaned_data['platform_name']
    chn_type = cleaned_data['chn_type']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return chn_db.get_root_chn_distribution(username_filter=username,
                                            platform_name_filter=platform_name,
                                            chn_type_filter=chn_type,
                                            page=page,
                                            size=size)


@require_GET
@response_wrapper
@validate_form(resource_form.GetChnTypeDistributionForm)
def get_chn_type_distribution(req, cleaned_data):
    page = cleaned_data['page']
    size = cleaned_data['size']
    return chn_db.get_chn_type_resource(page=page, size=size)


@require_POST
@response_wrapper
@validate_form(resource_form.AllocateChnToRootForm)
def allocate_chn_to_root(req, cleaned_data):
    root_id = cleaned_data['root_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    allocate_amount = cleaned_data['allocate_amount']
    return chn_handler.allocation_chn_to_root(root_id=root_id,
                                              project_id=platform_id,
                                              chn_type=chn_type,
                                              allocate_amount=allocate_amount)


@require_POST
@response_wrapper
@validate_form(resource_form.DeleteChnResourceForm)
def delete_chn_resource(req, cleaned_data):
    object_ids = cleaned_data['ids']
    chn_db.batch_delete_chn(object_ids)
    return dict()


@require_POST
@response_wrapper
@validate_form(resource_form.CreateOrUpdateChnTypeForm)
def create_or_update_chn_type(req, cleaned_data):
    project_name = cleaned_data['project_name']
    chn_type = cleaned_data['chn_type']
    chn_type_name = cleaned_data['chn_type_name']
    android_link = cleaned_data['android_link']
    ios_link = cleaned_data['ios_link']
    qr_link = cleaned_data['qr_link']
    to_update = cleaned_data['to_update']
    chn_db.create_or_update_chn_type(project_name=project_name,
                                     chn_type_key=chn_type,
                                     chn_type_name=chn_type_name,
                                     android_link=android_link,
                                     ios_link=ios_link,
                                     qr_link=qr_link,
                                     to_update=to_update)
    return dict()


@require_GET
@response_wrapper
@validate_form(resource_form.GetChnTypeDetailForm)
def get_chn_type_detail(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    platform_config = chn_db.get_platform_chn()
    return platform_config.get(platform_id, {}).get('chn_types', {}).get(chn_type, {})
