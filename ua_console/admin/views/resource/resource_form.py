# -*- coding: utf-8 -*-
from django import forms
from common.utils import exceptions as err
from common.chn.db import get_platform_chn
from common.utils.custom_form_field import JsonArrayField, DateTimeStringField


class ListChnForm(forms.Form):
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    chn = forms.CharField(required=False)
    status = forms.IntegerField(required=False)
    agent_level = forms.IntegerField(required=False)
    root_id = forms.IntegerField(required=False)
    bind_at_start_time = DateTimeStringField(required=False)
    bind_at_end_time = DateTimeStringField(required=False)
    created_at_start_time = DateTimeStringField(required=False)
    created_at_end_time = DateTimeStringField(required=False)
    updated_at_start_time = DateTimeStringField(required=False)
    updated_at_end_time = DateTimeStringField(required=False)
    order_by = forms.CharField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetRootChnDistributionForm(forms.Form):
    username = forms.CharField(required=False)
    platform_name = forms.CharField(required=False)
    chn_type = forms.CharField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class GetChnTypeDistributionForm(forms.Form):
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class AllocateChnToRootForm(forms.Form):
    root_id = forms.IntegerField()
    platform_id = forms.IntegerField()
    chn_type = forms.CharField()
    allocate_amount = forms.IntegerField()


class CreateChnResourceForm(forms.Form):
    platform_id = forms.IntegerField()
    chn_type = forms.CharField()
    qr_link = forms.CharField()
    android_link = forms.CharField()
    ios_link = forms.CharField()
    range = JsonArrayField(element_is_integer=True)


class DeleteChnResourceForm(forms.Form):
    ids = JsonArrayField()


class RecreateChnResourceForm(forms.Form):
    ids = JsonArrayField()
    to_create_poster = forms.IntegerField(required=False)


class CreateOrUpdateChnTypeForm(forms.Form):
    project_name = forms.CharField()
    chn_type = forms.CharField()
    chn_type_name = forms.CharField()
    android_link = forms.CharField()
    ios_link = forms.CharField()
    qr_link = forms.CharField()
    to_update = forms.IntegerField()


class GetChnTypeDetailForm(forms.Form):
    platform_id = forms.IntegerField()
    chn_type = forms.CharField()

    def clean(self):
        form_data = self.cleaned_data
        platform_config = get_platform_chn()
        if form_data['platform_id'] not in platform_config:
            raise err.ParamError(u'平台号参数错误')
        return form_data
