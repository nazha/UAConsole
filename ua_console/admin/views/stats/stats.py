# -*- coding: utf-8 -*-
from django.views.decorators.http import require_GET
from common.utils.decorator import response_wrapper, validate_form
from common.utils.api import token_required
from common.stats import handler
from admin.views.stats import stats_form


@require_GET
@response_wrapper
@token_required
@validate_form(stats_form.ListFinancialStatsForm)
def list_financial_stats(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    agent_level = cleaned_data['agent_level']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    start_time = cleaned_data['start_time']
    end_time = cleaned_data['end_time']
    order_by = cleaned_data['order_by'] or '-day'
    to_export_file = cleaned_data['to_export_file']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_financial_stats(agent_id=agent_id,
                                        agent_level=agent_level,
                                        platform_id=platform_id,
                                        chn_type=chn_type,
                                        start_time=start_time,
                                        end_time=end_time,
                                        order_by=order_by,
                                        to_export_file=to_export_file,
                                        page=page,
                                        size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(stats_form.ListPlatformStatsForm)
def list_platform_stats(req, cleaned_data):
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    start_time = cleaned_data['start_time']
    end_time = cleaned_data['end_time']
    order_by = cleaned_data['order_by'] or '-day'
    to_export_file = cleaned_data['to_export_file']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_platform_stats(platform_id=platform_id,
                                       chn_type=chn_type,
                                       start_time=start_time,
                                       end_time=end_time,
                                       order_by=order_by,
                                       to_export_file=to_export_file,
                                       page=page,
                                       size=size)


@require_GET
@response_wrapper
@token_required
@validate_form(stats_form.ListAgentStatsForm)
def list_agent_stats(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    parent_id = cleaned_data['parent_id']
    agent_level = cleaned_data['agent_level']
    chn = cleaned_data['chn']
    register_start_time = cleaned_data['register_start_time']
    register_end_time = cleaned_data['register_end_time']
    order_by = cleaned_data['order_by'] or '-day'
    to_export_file = cleaned_data['to_export_file']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_agent_stats(agent_id=agent_id,
                                    parent_id=parent_id,
                                    agent_level=agent_level,
                                    chn=chn,
                                    register_start_time=register_start_time,
                                    register_end_time=register_end_time,
                                    page=page,
                                    size=size,
                                    order_by=order_by,
                                    to_export_file=to_export_file)


@require_GET
@response_wrapper
@token_required
@validate_form(stats_form.ListCustomServiceRewardStatsForm)
def list_custom_service_reward_stats(req, cleaned_data):
    agent_id = cleaned_data['agent_id']
    platform_id = cleaned_data['platform_id']
    chn_type = cleaned_data['chn_type']
    parent_id = cleaned_data['parent_id']
    start_time = cleaned_data['start_time']
    end_time = cleaned_data['end_time']
    to_export_file = cleaned_data['to_export_file']
    page = cleaned_data['page']
    size = cleaned_data['size']
    return handler.list_custom_service_reward_stats(agent_id=agent_id,
                                                    parent_id=parent_id,
                                                    platform_id=platform_id,
                                                    chn_type=chn_type,
                                                    start_time=start_time,
                                                    end_time=end_time,
                                                    to_export_file=to_export_file,
                                                    page=page,
                                                    size=size)
