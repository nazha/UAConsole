import logging
from django import forms
from common.utils.custom_form_field import DateTimeStringField, JsonArrayField

_LOGGER = logging.getLogger('agent')


class ListFinancialStatsForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    agent_level = forms.IntegerField(required=False)
    platform_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    start_time = DateTimeStringField(required=False, to_date=True)
    end_time = DateTimeStringField(required=False, to_date=True)
    order_by = forms.CharField(required=False)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class ListPlatformStatsForm(forms.Form):
    platform_id = forms.IntegerField(required=False)
    chn_type = JsonArrayField(required=False)
    start_time = DateTimeStringField(required=False, to_date=True)
    end_time = DateTimeStringField(required=False, to_date=True)
    order_by = forms.CharField(required=False)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class ListAgentStatsForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    agent_level = forms.IntegerField(required=False)
    chn = forms.CharField(required=False)
    register_start_time = DateTimeStringField(required=False, to_date=True)
    register_end_time = DateTimeStringField(required=False, to_date=True)
    order_by = forms.CharField(required=False)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)


class ListCustomServiceRewardStatsForm(forms.Form):
    agent_id = forms.IntegerField(required=False)
    platform_id = forms.IntegerField(required=False)
    parent_id = forms.IntegerField(required=False)
    chn_type = forms.CharField(required=False)
    start_time = DateTimeStringField(required=False, to_date=True)
    end_time = DateTimeStringField(required=False, to_date=True)
    to_export_file = forms.IntegerField(required=False)
    page = forms.IntegerField(required=False)
    size = forms.IntegerField(required=False)
