# -*- coding: utf-8 -*-

import os
import sys
from time import sleep

import arrow
from fabric.api import local, sudo, put, env, run
from fabric.colors import red, green, cyan, blue, yellow
from fabric.context_managers import settings, cd, lcd
from fabric.contrib.files import exists, append
from fabric.contrib.project import rsync_project


def print_error(msg, stop=True):
    print(red(u" x   %s\n\n x   Task failed. Fix issues list above.\n" % msg))
    if stop:
        sys.exit()


def print_warning(msg):
    print(yellow(" ->   %s ..." % msg))


def print_start(msg):
    print(cyan("==>> %s ..." % msg))


def print_done(msg):
    print(green(u" ->  %s" % msg))


def print_info(msg):
    print(blue(u" ->  %s" % msg))


PROJECT_DIRS = {
    'ua_console': '../',
}

SETTING_FILES_PATH = {
    'ua_console': 'ua_console/base',
}

STAGES = {
    "test": {
        "roledefs": {
            'ua_console': ['3.0.191.136'],
        },
        "user": "max",
        "key_filename": "~/.ssh/id_rsa",
    }
}

if "stage" not in env:
    print(red(u" ✘   Stage not set, use --set stage=<STAGE>.\n"))
    sys.exit()

if env.stage not in STAGES:
    print(red(u" ✘   Stage invalid, check config.py.\n"))
    sys.exit()

env.roledefs = STAGES[env.stage]["roledefs"]
env.setting_file_path = SETTING_FILES_PATH
env.user = STAGES[env.stage]["user"]
env.key_filename = STAGES[env.stage]["key_filename"]

_CONFIG_FILE_PATH = os.path.dirname(os.path.abspath(__file__)) + '/config_files/'
_CWD = os.getcwd()

_ROLE = env.roles[0]
_REMOTE_PROJECT_DIR = "/opt/%s" % _ROLE
_AVAILABLE_DIR = "%s/available/" % _REMOTE_PROJECT_DIR
_ENABLED_DIR = "%s/enabled/" % _REMOTE_PROJECT_DIR
_ENABLED_APP_DIR = "%s/enabled/app" % _REMOTE_PROJECT_DIR
_SETTING_DIR = "%s/enabled/app/%s" % (_REMOTE_PROJECT_DIR, env.setting_file_path.get(_ROLE))
_LOG_DIR = "/var/log/%s" % _ROLE

_SOURCE_CODE_DIR = PROJECT_DIRS.get(_ROLE)


def setup_server():
    print_start("Setting up server")
    sudo("apt-get install -y zsh curl git libgeoip1 libgeoip-dev geoip-bin")
    sudo("chsh ubuntu -s /usr/bin/zsh")
    if exists("~/.oh-my-zsh"):
        print_info("Oh-my-zsh exists, skip installing Oh-my-zsh.")
    else:
        run("git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh")
    put(_CONFIG_FILE_PATH + "zshrc", "/home/ubuntu/.zshrc")

    sudo("mkdir -p %s" % _AVAILABLE_DIR)
    sudo("mkdir -p %s" % _ENABLED_DIR)
    sudo("mkdir -p %s" % _LOG_DIR)
    sudo("mkdir -p /var/log/bigbang")
    sudo("chown ubuntu:ubuntu -R %s" % _REMOTE_PROJECT_DIR)
    sudo("chown ubuntu:ubuntu -R %s" % _LOG_DIR)
    # add uwsgi required system config
    sudo("sysctl -w net.core.somaxconn=4096")
    append("/etc/sysctl.conf", "net.core.somaxconn=4096", use_sudo=True)
    print_done("Setting up server done.")


def upload_src():
    print_start("Uploading source code")
    now_str = arrow.now().format("YYYYMMDDHHmmss")
    available_app_dir = "%s/available/app_%s/" % (_REMOTE_PROJECT_DIR, now_str)
    run("mkdir -p %s" % available_app_dir)
    rsync_project(available_app_dir, "%s/" % _SOURCE_CODE_DIR,
                  exclude=['.git', '*.tar.bz2', '.idea', '*.deb', '.gitignore', '.python-version', '*.pyc'],
                  extra_opts="--filter='dir-merge,- .gitignore'")
    run("rm -fr %s" % _ENABLED_APP_DIR)
    run("ln -s %s %s" % (available_app_dir, _ENABLED_APP_DIR))
    if _ROLE in SETTING_FILES_PATH:
        run("mv %s/settings_%s.py %s/env_settings.py" % (_SETTING_DIR, env.stage, _SETTING_DIR))
    print_done("Uploading source code done.")

    if '-web' in _ROLE:
        return
    print_start("Installing requirements")
    run(
        "/home/ubuntu/.pyenv/versions/casino/bin/pip install -r /opt/%s/enabled/app/requirements.txt" % _ROLE)
    print_done("Installing requirements done.")

def restart_service():
    """
    Sometime program is running, but supervisor lose track of its pid. Then the program go wildly.
    You have to kill the program, then start the program using supervisorctl start <NAME>.
    Really sucks!
    """
    print_start("restarting %s" % _ROLE)
    sudo("supervisorctl update")
    if 'ua_console' == _ROLE:
        sudo("supervisorctl stop ua-console-uwsgi")
        sleep(5)
        sudo("chown max:max -R %s" % _LOG_DIR)
        sudo("supervisorctl start ua-console-uwsgi")
    print_done("restarting %s done." % _ROLE)


def update_docs():
    print_start("Updating docs")
    sudo("mkdir -p %s" % _REMOTE_PROJECT_DIR)
    sudo("chown ubuntu:ubuntu -R %s" % _REMOTE_PROJECT_DIR)
    with lcd('%s/docs' % _CWD):
        print_info('Building docs ...')
        local('bundle exec middleman build --clean')
    print_info('Uploading docs ...')
    rsync_project(_REMOTE_PROJECT_DIR, '%s/docs/build/' % _CWD)
    install_nginx()
    print_done("Done.\n")


def run_worker():
    run("")


def check_system():
    run("")


def deploy():
    #install_python()
    upload_src()
    restart_service()
    check_system()