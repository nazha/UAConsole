# -*- coding: utf-8 -*-
import os
import sys
from time import sleep
import arrow
from fabric.api import sudo, env, run
from fabric.contrib.files import append
from fabric.contrib.project import rsync_project
from fabric.colors import red, green, cyan, blue, yellow


def print_error(msg, stop=True):
    print(red(u" ✘   %s\n\n ✘   Task failed. Fix issues list above.\n" % msg))
    if stop:
        sys.exit()


def print_warning(msg):
    print(yellow(" !   %s ..." % msg))


def print_start(msg):
    print(cyan("==>> %s ..." % msg))


def print_done(msg):
    print(green(u" ✔   %s" % msg))


def print_info(msg):
    print(blue(u" ☞   %s" % msg))


PROJECT_DIRS = {
    'ua_console': '/Users/tsungjen/Projects/develop/dark/UAConsole',
}

SETTING_FILES_PATH = {
    'ua_console': '',
}

STAGES = {
    "prod": {
        "roledefs": {
            'ua_console': ['103.230.240.16']
        },
        "user": "ubuntu",
        "key_filename": "~/.ssh/id_rsa",
    },
}

if "stage" not in env:
    print(red(u" ✘   Stage not set, use `--set stage=<STAGE>`.\n"))
    sys.exit()

if env.stage not in STAGES:
    print(red(u" ✘   Stage invalid, check `config.py`.\n"))
    sys.exit()

env.roledefs = STAGES[env.stage]["roledefs"]
env.setting_file_path = SETTING_FILES_PATH
env.user = STAGES[env.stage]["user"]
env.key_filename = STAGES[env.stage]["key_filename"]

_CONFIG_FILE_PATH = os.path.dirname(os.path.abspath(__file__)) + '/config_files/'
_CWD = os.getcwd()

_ROLE = env.roles[0]
_REMOTE_PROJECT_DIR = "/opt/%s" % _ROLE
_AVAILABLE_DIR = "%s/available/" % _REMOTE_PROJECT_DIR
_ENABLED_DIR = "%s/enabled/" % _REMOTE_PROJECT_DIR
_ENABLED_APP_DIR = "%s/enabled/app" % _REMOTE_PROJECT_DIR
_SETTING_DIR = "%s/enabled/app/%s/" % (_REMOTE_PROJECT_DIR, env.setting_file_path.get(_ROLE))
_LOG_DIR = "/var/log/%s" % _ROLE

_SOURCE_CODE_DIR = PROJECT_DIRS.get(_ROLE)


def check_variables():
    print_info("project dir: %s" % PROJECT_DIRS.get(_ROLE))


def setup_server():
    print_start("Setting up server")
    sudo("apt-get install -y zsh curl git libgeoip1 libgeoip-dev geoip-bin")
    sudo("mkdir -p %s" % _AVAILABLE_DIR)
    sudo("mkdir -p %s" % _ENABLED_DIR)
    sudo("mkdir -p %s" % _LOG_DIR)
    sudo("mkdir -p /var/log/agent")
    sudo("chown ubuntu:ubuntu -R %s" % _REMOTE_PROJECT_DIR)
    sudo("chown ubuntu:ubuntu -R %s" % _LOG_DIR)
    sudo("sysctl -w net.core.somaxconn=4096")
    append("/etc/sysctl.conf", "net.core.somaxconn=4096", use_sudo=True)
    print_done("Setting up server done.")


def upload_src():
    print_start("Uploading source code")
    now_str = arrow.now().format("YYYYMMDDHHmmss")
    available_app_dir = "%s/available/app_%s/" % (_REMOTE_PROJECT_DIR, now_str)
    run("mkdir -p %s" % available_app_dir)
    rsync_project(available_app_dir, "%s/" % _SOURCE_CODE_DIR,
                  exclude=['.git', '*.tar.bz2', '.idea', '*.deb', '.gitignore', '.python-version', '*.pyc'],
                  extra_opts="--filter='dir-merge,- .gitignore'")
    run("rm -fr %s" % _ENABLED_APP_DIR)
    run("ln -s %s %s" % (available_app_dir, _ENABLED_APP_DIR))
    if _ROLE in SETTING_FILES_PATH:
        if _ROLE == 'ua_console':
            run("mv /opt/ua_console/enabled/app/ua_console/base/settings_%s.py "
                "/opt/ua_console/enabled/app/ua_console/base/env_settings.py"
                % env.stage)
    print_done("Installing requirements done.")


def restart_service():
    print_start("restarting %s" % _ROLE)
    sudo("supervisorctl update")
    if 'maestro' == _ROLE:
        sudo("supervisorctl stop ua-console-uwsgi")
        sleep(2)
        sudo("chown ubuntu:ubuntu -R %s" % _LOG_DIR)
        sudo("supervisorctl start ua-console-uwsgi")
    print_done("restarting %s done." % _ROLE)


def deploy():
    #setup_server()
    upload_src()
    restart_service()
    print_done("Deploy done.\n")
